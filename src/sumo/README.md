{\rtf1\ansi\ansicpg932\cocoartf1671\cocoasubrtf600
{\fonttbl\f0\fswiss\fcharset0 Helvetica;\f1\fnil\fcharset0 HelveticaNeue;}
{\colortbl;\red255\green255\blue255;}
{\*\expandedcolortbl;;}
\paperw11900\paperh16840\margl1440\margr1440\vieww10800\viewh8400\viewkind0
\pard\tx566\tx1133\tx1700\tx2267\tx2834\tx3401\tx3968\tx4535\tx5102\tx5669\tx6236\tx6803\pardirnatural\partightenfactor0

\f0\fs24 \cf0 Create mobility file
\f1 \
\pard\pardeftab560\slleading20\pardirnatural\partightenfactor0
\cf0 step1\
netconvert --osm-files xx.osm -o xx.net.xml --geometry.remove true --ramps.guess --roundabouts.guess --junctions.join true --tls.guess-signals true --tls.discard-simple true --tls.join\
\
=>xx is file name\
\
\
step2\
\pard\pardeftab560\slleading20\partightenfactor0
\cf0 python randomTrips.py -n xx.net.xml -b 0 -e 300 -p 0.1 -o xx.trip.xml\
\
=>b is simulation start time\
=>e is simulation end time\
=>p is interval of spawning a car\
\
\
step3\
duarouter -n xx.net.xml -t xx.trip.xml -o xx.rou.xml\
\
\
step4\
sumo -c xx.sumocfg --netstate-dump xx.dump\
\
\
Running mobility file\
sumo-gui -c xx.sumocfg\
\
\
Convert mobility file to tcl file\
step1\
sumo -c xx.sumocfg --fcd-output xx.xml\
\
\
step2\
python traceExporter.py --fcd-input xx.xml --ns2config-output xx.tcl --ns2activity-output xx_activity.tcl --ns2mobility-output xx_mobility.tcl\
\
\
Edit xx.net.xml \
netedit\
}