#####################################################################
#   Copyright (C) 2004  Toilers Research Group -- Colorado School of Mines
#
#   Please see COPYRIGHT.TXT and LICENSE.TXT for copyright and license
#   details.
#####################################################################
set debug 0         ;# 0 is off, 1 is on

set val(chan)       Channel/WirelessChannel
set val(prop)       Propagation/TwoRayGround
set val(ant)        Antenna/OmniAntenna
set val(ll)         LL
set val(ifq)        Queue/DropTail/PriQueue
set val(ifqlen)     20
set val(netif)      Phy/WirelessPhy
set val(mac)        Mac/NULL
set val(rp)         AODV
set val(nn)         [lindex $argv 0]
set val(x)          300
set val(y)          600
set val(txPower)    0.0075   ;# 100 meter range
set val(rxPower)    1
set val(mf)         [lindex $argv 1]

# =====================================================================
# Other default settings

puts "setting other default settings..."

LL set mindelay_                50us
LL set delay_                   25us
LL set bandwidth_               0       ;# not used

Agent/Null set sport_           0
Agent/Null set dport_           0

Agent/CBR set sport_            0
Agent/CBR set dport_            0

Agent/TCPSink set sport_        0
Agent/TCPSink set dport_        0

Agent/TCP set sport_            0
Agent/TCP set dport_            0
Agent/TCP set packetSize_       512

Queue/DropTail/PriQueue set Prefer_Routing_Protocols    1

# unity gain, omni-directional antennas
# set up the antennas to be centered in the node and 1.5 meters above it
Antenna/OmniAntenna set X_ 0
Antenna/OmniAntenna set Y_ 0
Antenna/OmniAntenna set Z_ 1.5
Antenna/OmniAntenna set Gt_ 1.0
Antenna/OmniAntenna set Gr_ 1.0

# Initialize the SharedMedia interface with parameters to make
# it work like the 914MHz Lucent WaveLAN DSSS radio interface
Phy/WirelessPhy set CPThresh_ 10.0
Phy/WirelessPhy set CSThresh_ 1.559e-11
Phy/WirelessPhy set RXThresh_ 3.652e-10
Phy/WirelessPhy set Rb_ 2*1e6
#this was the default
#Phy/WirelessPhy set Pt_ 0.2818
# This is for 100m
Phy/WirelessPhy set Pt_ 7.214e-3
# This is for 40m
#Phy/WirelessPhy set Pt_ 8.5872e-4
# This is for 250m
#Phy/WirelessPhy set Pt_ 0.2818
Phy/WirelessPhy set freq_ 914e+6
Phy/WirelessPhy set L_ 1.0

# =====================================================================
# This puts in only the headers that we need.
# =====================================================================
puts "removing unecessary packet headers..."
remove-all-packet-headers
add-packet-header IP
add-packet-header Common
add-packet-header LBM
add-packet-header LL
add-packet-header Mac

#Create a simulator object
set ns_ [new Simulator]

#Open a trace file
#set nf [open outTR.nam w]
#$ns_ namtrace-all-wireless $nf $val(x) $val(y)

#set nt [open "|awk -f script.awk > try25b.tr" w]
#set nt [open "|awk -f trial2w.awk > ldb10-19.tr" w]
set nt [open wstry.tr w]
#set nt [open "|awk -f geocast.awk >> geo_corridor.tr" w]
$ns_ use-newtrace
$ns_ trace-all $nt

set topo [new Topography]
$topo load_flatgrid $val(x) $val(y)

create-god $val(nn)

# Create channel #1
puts "creating channel..."
set chan_1_ [new $val(chan)]

# configure node

$ns_ node-config -adhocRouting $val(rp) \
                -llType $val(ll) \
                -macType $val(mac) \
                -ifqType $val(ifq) \
                -ifqLen $val(ifqlen) \
                -antType $val(ant) \
                -propType $val(prop) \
                -phyType $val(netif) \
                #-channelType $val(chan) \
		-channel $chan_1_ \
                -topoInstance $topo \
                -agentTrace OFF \
                -routerTrace OFF \
                -macTrace OFF\
                -movementTrace OFF \
                -txPower $val(txPower) \
                -rxPower $val(rxPower)            


# create the nodes			 
for {set i 0} {$i <= $val(nn) } {incr i} {
  set node_($i) [$ns_ node]	
  $node_($i) random-motion 0           ;# disable random motion
}

puts "Loading movement scenario file"
source $val(mf)

$node_($val(nn)) set X_ 225.0
$node_($val(nn)) set Y_ 525.0
$node_($val(nn)) set Z_ 0.0 

#
#Define a 'finish' procedure
proc finish {} {
        global ns_ nt
        $ns_ flush-trace
#        close $nf
         close $nt
#        exec nam out.nam &
        exit 0
}

for {set i 0} {$i <= $val(nn)} {incr i} {
  $ns_ initial_node_pos $node_($i) 20
}

#Create geocast agents and attach them to the nodes 
for {set i 0} {$i <= $val(nn)} {incr i} {
  set g($i) [new Agent/LBM]
  $node_($i) attach $g($i) 220

  # need to tell the geocast agents about their link layers
  set ll($i) [$node_($i) set ll_(0)]
  $ns_ at 0.0 "$g($i) set-ll $ll($i)"

  # need to tell the geocast agents which nodes they're on also
  $ns_ at 0.0 "$g($i) set-node $node_($i)"
  # $ns_ at 0.0 "$g($i) setVerbosity 0"
}

puts "Scheduling the send events"
set v 1.0
while {$v < 1001.0} {
  $ns_ at $v "$g(0) sendMulticastData 150 450 300 600 64 S"
  set v [expr $v+0.025]
}

for {set i 0} {$i <= $val(nn) } {incr i} {
  $ns_ at 1003.0 "$g($i) lbmDone"
}

$ns_ at 1004.0 "finish"
$ns_ at 1005.0 "puts \"NS Exiting...\" ; $ns_ halt"

#Run the simulation
puts ""
puts ""
puts "***********************************************"
puts "***********************************************"
puts "***********************************************"
puts ""
puts "Running the simulation"
puts ""
puts "***********************************************"
puts "***********************************************"
puts "***********************************************"
puts ""
puts ""
$ns_ run
