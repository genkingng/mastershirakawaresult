/******************************************************************************
*   Copyright (C) 2004  Toilers Research Group -- Colorado School of Mines
*
*   Please see COPYRIGHT.TXT and LICENSE.TXT for copyright and license
*   details.
*******************************************************************************/
#ifndef ns_lbm_h
#define ns_lbm_h

#include "object.h"
#include "agent.h"
#include "tclcl.h"
#include "packet.h"
#include "address.h"
#include "ip.h"
#include "mac.h"
#include "mobilenode.h"
#include "timer-handler.h"
#include "math.h"

#include <set>
#include <map>
#include <list>
#include <iostream>

using namespace std;

// This means that the jitter ranges from 0.0 to 0.0001 with
// a mean of 0.00005, so the average number of bits that could
// be transmitted without jitter is 50 per transmission.  This seems
// acceptable.
// Note:  this is just the divisor in the random number range calculation.
#define  lbmJitter 10000.0

// size of the fixed portion of the simulated header
#define LBM_HDR_SIZE_FIXED sizeof(hdr_lbm)

// Protocol parameters
// This is the lbm fudge factor.  It is used to make the box or
// step methods slightly bigger or smaller.  It is measured in units or meters
#define LBMboxDelta 0.0
#define LBMstepDelta 0.0

#define lbm_min(x,y) (((x) < (y)) ? (x) : (y))
#define lbm_max(x,y) (((x) > (y)) ? (x) : (y))

inline double DIST(double x1,double y1,double x2,double y2)
{
  double dist = (x2-x1)*(x2-x1)+(y2-y1)*(y2-y1);
  dist = sqrt(dist);
  return dist;
};

// prototypes for utilitiy functions
void LBMprintHeaders(Packet *p);

struct hdr_lbm {

  // 'D' = Data Packet
  // 'U' = unset
  char lbmCode_;

  // 'B' = box
  // 'S' = step
  // 'F' = flood
  // 'U' = unset
  char forwardCode_;

  // fields always needed
  double      sourceX_;
  double      sourceY_;
  double      lastHopX_;
  double      lastHopY_;
  double      lowerLeftX_;
  double      lowerLeftY_;
  double      upperRightX_;
  double      upperRightY_;
  double      sendTime_;

  // fields needed by data packets
  int       dataLength_;   // length of the data in the packet in bytes
                          // does not include the header length

  // additional field for calculate 
  // approx. number of hops from source to geocast region
  int numHops;

  // things I get for "free" from hdr_ip in packet.h
  // int saddr();      // IP addr of source sender
  // int sport();      // port number of the source sender
  // int daddr();      // IP addr of the destination
  // int dport();      // port of the destination

  // things I get for "free" from hdr_cmn in packet.h
  // double   ts_;            // timestamp
  // int      size_;          // simulated packet size
  // int      uid_;           // unique packet id
  // nsaddr_t prev_hop_;      // IP addr of forwarding hop
  // int      num_forwards_;  // number of forwards

  // Now I need to provide access functions for my structure members
  inline char     &lbmCode() { return lbmCode_; }
  inline char     &forwardCode() { return forwardCode_; }
  inline double   &sourceX() { return sourceX_; }
  inline double   &sourceY() { return sourceY_; }
  inline double   &lastHopX() { return lastHopX_; }
  inline double   &lastHopY() { return lastHopY_; }
  inline double   &lowerLeftX() { return lowerLeftX_; }
  inline double   &lowerLeftY() { return lowerLeftY_; }
  inline double   &upperRightX() { return upperRightX_; }
  inline double   &upperRightY() { return upperRightY_; }
  inline double   &sendTime() { return sendTime_; }
  inline int      &dataLength() { return dataLength_; }

  inline int size()
  { 
    return LBM_HDR_SIZE_FIXED + dataLength_;
  }

  static int offset_;
  inline static int& offset() {return offset_;}
  inline static hdr_lbm* access(const Packet* p) 
  {
    return (hdr_lbm*) p->access(offset_);
  }
};

class LBMAgent;

// Agent inheritance is because this is a specialized Agent
class LBMAgent : public Agent
{
  public:
    LBMAgent();
    ~LBMAgent();
    int command(int argc, const char*const* argv);
    void recv(Packet*, Handler*);
  protected:
    int off_lbm_;

    bool dupRcvdPacket(int id);
    bool dupSentPacket(int id);
    void recordSentPacket(int id);
    void recordRcvdPacket(int id);
    bool inMulticastRegion(double llx, double lly, double urx, double ury);
    bool inForwardingRegion(char fwdCode, double srcX, double srcY,
			    double llx, double lly, double urx, double ury);
    void broadcastPacket(Packet *p);
    void rebroadcastPacket(Packet *p);

    // this is just a set of packets I've sent
    set<int, less<int> > sentPackets;
    // this is just a set of packets I've received
    set<int, less<int> > rcvdPackets;

    NsObject *ll;  // link layer output in order to avoid using a routing agent
    MobileNode *node;  // the node that this agent is attached to

    /*    
    // all these variables are for statistics keeping...
    int lbmPkts; // the number of lbm packets sent in the simulation
    int totalTransmitPkts; // the total number of packets transmitted in the network 
			   // by adding one for each hop in the network a packet takes
    int deliveredPktID[30000]; // deliveredPktID[i] indicate how many nodes in the multicast region receive the packet with packet ID i
    */
};

#endif
