/******************************************************************************
*   Copyright (C) 2004  Toilers Research Group -- Colorado School of Mines
*
*   Please see COPYRIGHT.TXT and LICENSE.TXT for copyright and license
*   details.
*******************************************************************************/
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>
#include "gamer.h"

using namespace std;

int hdr_gamer::offset_;

// SWITCH TIMER
#define GEO_TIMER_FORWARD 0.1

// default value
// If a join table hasn't been received in meshMemberTimeout seconds
// then the node will stop automatically forwarding data packets
double meshMemberTimeout = 7;

// variables for statistics
long sendpkts = 0; // the number of data packets in the area
long long totalTransmitPkts = 0; // the total number of packets transmitted in the sim area, add one at broadcast and rebroadcast
long long totalTransmitBytes = 0; // the total bytes transmitted in the sim area, add at broadcast and rebroadcast
long long totalTransmitCtrlPkts = 0; // the total number of control packets transmitted in the sim area, add one at broadcast and rebroadcast of the control packets
long long totalTransmitCtrlBytes = 0; // the total control bytes transmitted in the sim area, add at broadcast and rebroadcast of the control packets
long long totalTransmitDataPkts = 0; // the total number of data packets transmitted in the sim area, add one at broadcast and rebroadcast of the data packets
long long totalTransmitDataBytes = 0; // the total data bytes transmitted in the sim area, add at broadcast and rebroadcast of the data packets
long oneSuccess = 0; // the number of one success packets
double totalEndToEndDelay = 0; // The end_to_end delay is the delay from the time when data packet sent to the time when the data packet reached the geocast region (the first node received)
vector<int> sendPktID; // store the PIDs of the data packets
vector<int> receivePktID; // =0, not received by any node in geocast region 
		       // >0, received by at least one node in geocast region

// to calculate approx. number of hops from source to geocast region
int totalnumHops = 0;
		       
// I reused some routines from a previous project, and they need a class
// Pointxy that looks something like this.  For now, it has file scope in
// this source (non-header) file, and should not cause any naming conflicts.
class Pointxy
{
  public:
    // constructor
    Pointxy(const double &x=0.0, const double &y=0.0):  
      xVal(x), yVal(y)
    { /* no code needed */ };

    ~Pointxy() { return; };

    // inspectors
    double getX() const {return xVal;};
    double getY() const {return yVal;};

    // mutators
    double setX(const double &newx) { return (xVal = newx); };
    double setY(const double &newy) { return (yVal = newy); };

// don't un-comment this unless you define PtDelta somewhere
//    bool operator==(const Pointxy &right) const
//    {
//      return ((fabs(xVal-right.getX()) < PtDelta) &&
//              (fabs(yVal-right.getY()) < PtDelta));
//    };

  protected:
    double   xVal, yVal;
};

//class to represent a line in the format y=a*x+b
class GamerLine
{
 public:
   //constructor
   GamerLine( const double &a = 0.0, const double &b=0.0):
                  aVal(a),bVal(b){};

   ~GamerLine(){return;};

   //inspectors
   double getA() const {return aVal;};
   double getB() const {return bVal;};

   //mutators
   double setA(const double &newA) {return  (aVal = newA);};
   double setB(const double &newB) {return  (bVal = newB);};

 protected:
   double aVal, bVal;

};
// just a prototype here for a function that is needed later
bool inHull(const Pointxy &p, const vector<Pointxy> &hull);


//utility function to find the line function across two points
GamerLine getLineTwoP(const Pointxy &p1, const Pointxy &p2);

//utility function to find the cross point of two line
Pointxy crossPoint(const GamerLine &line1, const GamerLine &line2);

//utility function to find the line function with one point and rate
GamerLine getLineOneP(const Pointxy &p, const double &cut_rate);

// if true, this will add jitter to unicast packets being sent
bool useJitteronSend = true;

// used for reporting status, higher numbers imply more verbose output
int verbosity = 1;

//Implementation of virtual function "expire" for timer class
void GeoRtxTimer::expire(Event *e)
{
 a_-> timeout();
}

// The next two class declarations bind our C++ classes to the OTcl
// class.
static class GamerHeaderClass : public PacketHeaderClass {
public:
  GamerHeaderClass() : PacketHeaderClass("PacketHeader/Gamer", 
					sizeof(hdr_gamer)) {}
} class_gamerhdr;


static class GamerClass : public TclClass {
public:
  GamerClass() : TclClass("Agent/Gamer") {}
  TclObject* create(int, const char*const*) {
    return (new GamerAgent());
  }
} class_gamer;


// ***********************************************************************
//                            Creator
// ***********************************************************************
GamerAgent::GamerAgent() : Agent(PT_GAMER),rtx_timer_(this)
{
  // off_gamer_ = hdr_gamer::offset();
  // bind("packetSize_", &size_);
  // bind("off_gamer_", &off_gamer_);
  // This binds the C++ and OTcl variables.  I may need to do some
  // more, but I don't know right now.

  // initially, no nodes are in the mesh
  meshMember = false;

  // defaults to true to limit the fan out of the mesh on the last hop
  sparseMeshMode = true;

  // initialize this to the negative of two times the timeout
  timeOfLastJoinTable = -2.0*meshMemberTimeout;

  ll = NULL;
  node = NULL;
 
   //srand(clock());

  // 
  return;
}

// ***********************************************************************
//                            Destructor
// ***********************************************************************
GamerAgent::~GamerAgent()
{
  rcvdPackets.erase(rcvdPackets.begin(), rcvdPackets.end());
  sentPackets.erase(sentPackets.begin(), sentPackets.end());
  geocastRegions.erase(geocastRegions.begin(), geocastRegions.end());
  demandsToRegions.erase(demandsToRegions.begin(), demandsToRegions.end());
  return;
}


// ***********************************************************************
// Set retry timer using average round-trip-time
// ***********************************************************************
void GamerAgent::set_rtx_timer()
 {
    rtx_timer_.resched(GEO_TIMER_FORWARD);
 }


// ************************************************************************
// Update the current and try flag according to the result of JOIN_DEMAND
// ************************************************************************
 void GamerAgent::updateFwdCode(bool success)
 {
  if(success == true)
   {
     current = next;
     if(next == 'D')
       {
        next = 'C';
       }
     else if(next == 'F')
       {
        next = 'D';
       }
   } 
  else
   {
    if(next == 'C')
     {
       next = 'D';
       if (verbosity > 0)
       {
       		cout << "Try Cone failed, ";
       		cout << "Now try Corridor to set mesh.....\n";
       }
     }
    else if(next == 'D')
     {
       next = 'F';
       if (verbosity > 0)
       {
       		cout << "Try Cone failed, ";
       		cout << "Now try Corridor to set mesh.....\n";
       }	
     }
    else if(next == 'F')
     {
     	if (verbosity > 0)
       		cout<<"There is no way to establish mesh right now,please try later"<<endl;
       //temporarily stop send data and try flood for next JOIN_DEMAND
       current = 'U';
      
     }
   }
 }

// ************************************************************************
// timeout() function called by timer's expire()
// ************************************************************************
 void GamerAgent::timeout()
 {
  /* ------------------------------------------------------------
   * Need to call the updateFwd() function for the geocast agent 
   * -----------------------------------------------------------*/
   if (verbosity > 0)
   	cout << "It is timeout, but there is no JOIN_TABLE came back\n";
   updateFwdCode(false);
   sendJoinDemand();
 }

// ******************************************************************************
// method to automatically send a join demand with new forwarding code if timeout
// ******************************************************************************
 int GamerAgent::sendJoinDemand()
 {
    if ((ll == NULL) || (node == NULL))
      {
        fprintf(stderr, "Link layer and node must be set on a geocast/gamer agent\n");
        fprintf(stderr, "before anything can be sent.\n");
        return (TCL_ERROR);
      }

      /*BW*/  node->update_position();
      //int regionID = atoi(argv[2]);
      int regionID = 0;
      map<int, geocastRegionType, less<int> >::iterator  regionPtr;
      regionPtr = geocastRegions.find(regionID);
      if (regionPtr == geocastRegions.end())
      {
        // the ID passed to us is invalid
        fprintf(stderr, "Invalid geocast region ID passed to sendDemand.\n");
        return (TCL_ERROR);
      }

      // BW Added this next part to prevent join Demand if source in GR
      if (this->inGeocastRegion(regionPtr->second.llx,regionPtr->second.lly,regionPtr->second.urx,regionPtr->second.ury))
      {
      	if (verbosity > 0)
	{
           cout << "***************************************************" << endl;
           cout << "Not Sending Join Demand from: " << this->addr()
             << " at [" << node->X() << "," << node->Y() << "]" << endl;
           cout << "Time: " << Scheduler::instance().clock() << endl;
           cout << "I'm in the GeoRegion so no need to send Join Demands" << endl;
           cout << "***************************************************" << endl;
	}
        return (TCL_OK);
      }
      // Create a new packet
      Packet* pkt = allocpkt();
      // Access the Gamer header for the new packet:
      /*
      hdr_gamer* geohdr = (hdr_gamer*)pkt->access(off_gamer_);
      hdr_cmn* hdrcmn = (hdr_cmn*)pkt->access(off_cmn_);*/
      
      struct hdr_gamer* geohdr = hdr_gamer::access(pkt);
      struct hdr_cmn* hdrcmn = hdr_cmn::access(pkt);

/*~~~~~~~~~~~~~~~~~~change source routing to local routing~~~~~~~~~~~~~
      geohdr->hops() = 0;
      geohdr->route_[geohdr->hops()] = this->addr();
      geohdr->hops()++;
      
      // record that I sent this join demand so if I get it back I know to
      // take some action
      originator.insert(hdrcmn->uid_);
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/ 
      // Store the current time in the 'send_time' field
      geohdr->sendTime_ = Scheduler::instance().clock();

      geohdr->gamerCode_ = 'J';
      /* ------------------------------------------------------
       * use the "next" forwarding code for JOIN_DEMAND and
       * the "current" forwarding code for DATA packet
       * ------------------------------------------------------*/
      geohdr->forwardCode_ = next;
      geohdr->sourceX_ = node->X();
      geohdr->sourceY_ = node->Y();
      geohdr->lowerLeftX_ = regionPtr->second.llx;
      geohdr->lowerLeftY_ = regionPtr->second.lly;
      geohdr->upperRightX_ = regionPtr->second.urx;
      geohdr->upperRightY_ = regionPtr->second.ury;
      geohdr->repliedTo() = false;
 
      geohdr->demandID_ = hdrcmn->uid_;
      geohdr->demandSendTime_ = Scheduler::instance().clock();
      geohdr->dataLength_ = 0;

      // update the data structure that links demand ID's to regions ID's
      demandsToRegions[hdrcmn->uid_] = regionID;

/*~~~~~~~~~~~~~~~~~~change source routing to local routing~~~~~~~~~~~~~	*/

      // record that I sent this join demand so if I get it back I know to
      // take some action
      // since I am the originator, so the last hop is myself
      
      localRoute newLastHop;
      newLastHop.demandID_ = hdrcmn->uid_;
      newLastHop.lastHop_ = this->addr();
      lastHopOfJD[hdrcmn->uid_] = newLastHop;
      
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

      // send it out
      if (verbosity > 0)
      {
        cout << "****************************************************" << endl;
        cout <<  "SendJoinDemand::Start ........................."<<endl;
        cout << "Sending Join Demand from:  " << this->addr()
           << " at [" << node->X() << "," << node->Y() << "]" << endl;
        cout << "Time:  " << Scheduler::instance().clock() << endl;
	cout << "****************************************************" << endl;
        cout <<"The timer is started:  " << endl;
        cout << "****************************************************" << endl << endl;
      }
      
       this->broadcastPacket(pkt);

      // start the timer after the sending
      set_rtx_timer();

      // return TCL_OK, so the calling function knows that the
      // command has been processed
      return (TCL_OK);
 }


// ***********************************************************************
//                            command
// ***********************************************************************
int GamerAgent::command(int argc, const char*const* argv)
{
  TclObject *obj;  

  if (argc == 2)
  {
    if (strcmp(argv[1], "gamerDone") == 0)
    {
    	// print out the result at the first node, just print once
	if ( this->addr() == 0 )
	{
	      cout << "\t GEO_TIMER_FORWARD = " << GEO_TIMER_FORWARD << endl;
	      cout << "\t meshMemberTimeout = " << meshMemberTimeout << endl;
	      cout << "\t sendpkts = " << sendpkts << endl;
 	      cout << "\t oneSuccess = " << oneSuccess << endl;
	      cout << "\t totalTransmitPkts = " << totalTransmitPkts << endl;
	      cout << "\t totalTransmitBytes = " << totalTransmitBytes << endl;
	      cout << "\t totalTransmitCtrlPkts = " << totalTransmitCtrlPkts << endl;
	      cout << "\t totalTransmitCtrlBytes = " << totalTransmitCtrlBytes << endl;
	      cout << "\t totalTransmitDataPkts = " << totalTransmitDataPkts << endl;
	      cout << "\t totalTransmitDataBytes = " << totalTransmitDataBytes << endl;	         
	      cout << "\t oneSuccessRatio = " << (float)oneSuccess/sendpkts << endl;
	      cout << "\t totalTransmitPkts/oneSuccess = " << 
	      		totalTransmitPkts/oneSuccess << endl;
	      cout << "\t totalTransmitBytes/oneSuccess = " <<
	      		totalTransmitBytes/oneSuccess << endl;
	      cout << "\t totalTransmitCtrlPkts/oneSuccess = " << 
	      		totalTransmitCtrlPkts/oneSuccess << endl;
	      cout << "\t totalTransmitCtrlBytes/oneSuccess = " <<
	      		totalTransmitCtrlBytes/oneSuccess << endl;
	      cout << "\t totalTransmitDataPkts/oneSuccess = " << 
	      		totalTransmitDataPkts/oneSuccess << endl;
	      cout << "\t totalTransmitDataBytes/oneSuccess = " <<
	      		totalTransmitDataBytes/oneSuccess << endl;						
              cout << "\t totalEndToEndDelay/oneSuccess = " <<
                	totalEndToEndDelay/oneSuccess << endl;	      

	      cout << "---------" << endl;
	      // to calculate approx. number of hops 
	      // from source to gamer region
	      cout << "\t totalnumHops = " << totalnumHops << endl;
	      cout << "\t totalnumHops/oneSuccess = " << 
			((float)totalnumHops)/oneSuccess << endl;     

	      	      /*
	      for (int m = 0; m < sendpkts; m++)
	      	{
			cout << "sendpkt[" << m << "] = " << sendPktID[m] << endl;
			cout << "receivepkt[" << m << "] = " << receivePktID[m] << endl;
		}*/
	 } 
	    
      if(verbosity > 0)
      {
      	cout << "Gamer agent on node " << this->addr() << " done at "
           << Scheduler::instance().clock() << endl;
      }
      return (TCL_OK);
    } else if (strcmp(argv[1], "setSparseMeshMode") == 0) {
      sparseMeshMode = true;
      return (TCL_OK);
    } else if (strcmp(argv[1], "unsetSparseMeshMode") == 0) {
      sparseMeshMode = false;
      return (TCL_OK);
    }
  } else if (argc == 3) {
    if (strcmp(argv[1], "setVerbosity") == 0)
    {
      verbosity = atoi(argv[2]);
      if (verbosity < 1)
      {
        verbosity = 0;
      } else if (verbosity > 2) {
        verbosity = 2;
      }
      return (TCL_OK);
    } else if (strcmp(argv[1], "set-ll") == 0) {
      if( (obj = TclObject::lookup(argv[2])) == 0)
      {
        fprintf(stderr, "GamerAgent(set-ll): %s lookup of %s failed\n", 
                        argv[1], argv[2]);
        return (TCL_ERROR);
      }
      ll = (NsObject*) obj;
      return (TCL_OK);
    } else if (strcmp(argv[1], "set-node") == 0) {
      if( (obj = TclObject::lookup(argv[2])) == 0)
      {
        fprintf(stderr, "GamerAgent(set-node): %s lookup of %s failed\n", 
                        argv[1], argv[2]);
        return (TCL_ERROR);
      }
      node = dynamic_cast< MobileNode * >(obj);
      if (node)       // dynamic cast was successful and didn't return NULL
      {
        return (TCL_OK);
      } else {
        fprintf(stderr, "Unable to dynamically cast %s to a MobileNode\n",
                        argv[2]);
        return (TCL_ERROR);
      } 
    } else if (strcmp(argv[1], "setMeshMemberTimeout") == 0) {
      meshMemberTimeout = atoi(argv[2]);
      return (TCL_OK);
    } else if (strcmp(argv[1], "sendDemand") == 0) {
      if ((ll == NULL) || (node == NULL))
      {
        fprintf(stderr, "Link layer and node must be set on a gamer agent\n");
        fprintf(stderr, "before anything can be sent.\n");
        return (TCL_ERROR);
      }
      
      /*BW*/  node->update_position();
      int regionID = atoi(argv[2]);
      map<int, geocastRegionType, less<int> >::iterator  regionPtr;
      regionPtr = geocastRegions.find(regionID);
      if (regionPtr == geocastRegions.end())
      {
        // the ID passed to us is invalid
        fprintf(stderr, "Invalid geocast region ID passed to sendDemand.\n");
        return (TCL_ERROR);
      }

      // BW Added this next part to prevent join Demand if source in GR
      if (this->inGeocastRegion(regionPtr->second.llx,regionPtr->second.lly,regionPtr->second.urx,regionPtr->second.ury)) 
      { 
      	if (verbosity > 0)
	{
          cout << "***************************************************" << endl;
          cout << "Not Sending Join Demand from: " << this->addr()
             << " at [" << node->X() << "," << node->Y() << "]" << endl;
          cout << "Time: " << Scheduler::instance().clock() << endl;
          cout << "I'm in the GeoRegion so no need to send Join Demands" << endl;
          cout << "***************************************************" << endl;
        }
	return (TCL_OK);
      }
      // Create a new packet
      Packet* pkt = allocpkt();
      // Access the Gamer header for the new packet:
      /*
      hdr_gamer* geohdr = (hdr_gamer*)pkt->access(off_gamer_);
      hdr_cmn* hdrcmn = (hdr_cmn*)pkt->access(off_cmn_);*/
      
      struct hdr_gamer* geohdr = hdr_gamer::access(pkt);
      struct hdr_cmn* hdrcmn = hdr_cmn::access(pkt);
 
/*~~~~~~~~~~~~~~~~~~change source routing to local routing~~~~~~~~~~~~~
      geohdr->hops() = 0;
      geohdr->route_[geohdr->hops()] = this->addr();
      geohdr->hops()++;
      
      // record that I sent this join demand so if I get it back I know to
      // take some action
      originator.insert(hdrcmn->uid_);
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/           
      
      // Store the current time in the 'send_time' field
      geohdr->sendTime_ = Scheduler::instance().clock();

      geohdr->gamerCode_ = 'J';
      /* ------------------------------------------------------
       * Call the chooseCode() function to choose the current
       * use the try forwarding code for JOIN_DEMAND and 
       * the current forwarding code for DATA packet
       * ------------------------------------------------------*/
      geohdr->forwardCode_ = next;
      geohdr->sourceX_ = node->X();
      geohdr->sourceY_ = node->Y();
      geohdr->lowerLeftX_ = regionPtr->second.llx;
      geohdr->lowerLeftY_ = regionPtr->second.lly;
      geohdr->upperRightX_ = regionPtr->second.urx;
      geohdr->upperRightY_ = regionPtr->second.ury;
      geohdr->repliedTo() = false;

      geohdr->demandID_ = hdrcmn->uid_;
      geohdr->demandSendTime_ = Scheduler::instance().clock();
      geohdr->dataLength_ = 0;

      // update the data structure that links demand ID's to regions ID's
      demandsToRegions[hdrcmn->uid_] = regionID;

/*~~~~~~~~~~~~~~~~~~change source routing to local routing~~~~~~~~~~~~~	*/

      // record that I sent this join demand so if I get it back I know to
      // take some action
      // since I am the originator, so the last hop is myself
      
      localRoute newLastHop;
      newLastHop.demandID_ = hdrcmn->uid_;
      newLastHop.lastHop_ = this->addr();
      lastHopOfJD[hdrcmn->uid_] = newLastHop;
      
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

      // send it out
      if(verbosity > 0)
      {
	cout << "****************************************************" << endl;
	cout << "Sending Join Demand from:  " << this->addr()
     	  << " at [" << node->X() << "," << node->Y() << "]" << endl;
	cout << "Time:  " << Scheduler::instance().clock() << endl;
	cout << "****************************************************" << endl;
	cout <<"The timer is started:  " << endl;
	cout << "****************************************************" << endl << endl;
      }
      
      this->broadcastPacket(pkt);

      // start the timer after the sending
      set_rtx_timer();

      // return TCL_OK, so the calling function knows that the
      // command has been processed
      return (TCL_OK);
    }
  } else if (argc == 4) {
    if (strcmp(argv[1], "sendData") == 0)
    {
// NOTE:  I need to use the data size value later, and I'm not using it
//        right now.
      if ((ll == NULL) || (node == NULL))
      {
        fprintf(stderr, "Link layer and node must be set on a gamer agent\n");
        fprintf(stderr, "before anything can be sent.\n");
        return (TCL_ERROR);
      }
      //check existence of mesh before sending out data
      //if (current == 'U')
      // {
      // fprintf(stderr,"There is no mesh existing from source node to region\n");
       //return (TCL_OK);
       //}
      //else
      //{//if there is mesh exists, start send the data packet
      /*BW*/ node->update_position();
      int regionID = atoi(argv[2]);
      map<int, geocastRegionType, less<int> >::iterator  regionPtr;
      regionPtr = geocastRegions.find(regionID);
      if (regionPtr == geocastRegions.end())
      {
        // the ID passed to us is invalid
        fprintf(stderr, "Invalid geocast region ID passed to sendData.\n");
        return (TCL_ERROR);
      }
      // Create a new packet
      Packet* pkt = allocpkt();
      // Access the Geocast header for the new packet:
      /*
      hdr_gamer* geohdr = (hdr_gamer*)pkt->access(off_gamer_);
      hdr_cmn* hdrcmn = (hdr_cmn*)pkt->access(off_cmn_);*/
      
      struct hdr_gamer* geohdr = hdr_gamer::access(pkt);
      struct hdr_cmn* hdrcmn = hdr_cmn::access(pkt);      

/*~~~~~~~~~~~~~~~~~~change source routing to local routing~~~~~~~~~~~~~
      geohdr->hops() = 0;
      geohdr->route_[geohdr->hops()] = this->addr();
      geohdr->hops()++;
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/ 

      // Store the current time in the 'send_time' field
      geohdr->sendTime_ = Scheduler::instance().clock();

      geohdr->gamerCode_ = 'D';
      geohdr->forwardCode_ = current;
      geohdr->sourceX_ = node->X();
      geohdr->sourceY_ = node->Y();
      geohdr->lowerLeftX_ = regionPtr->second.llx;
      geohdr->lowerLeftY_ = regionPtr->second.lly;
      geohdr->upperRightX_ = regionPtr->second.urx;
      geohdr->upperRightY_ = regionPtr->second.ury;
      geohdr->repliedTo() = false;

      geohdr->demandID_ = hdrcmn->uid_;
      geohdr->demandSendTime_ = Scheduler::instance().clock();
      geohdr->dataLength_ = atoi(argv[3]);

      // to calculate approx. number of hops from source to gamer region
      geohdr->numHops = 1;
      
      // send it out
      if (verbosity > 0)
      {
	cout << "****************************************************" << endl;
	cout << "Sending gamer data from:  " << this->addr()
     	  << " at [" << node->X() << "," << node->Y() << "]" << endl;
	cout << "Time:        " << Scheduler::instance().clock() << endl;
	cout << "Data size:   " << geohdr->dataLength() << endl;
	cout << "Total size:  " << geohdr->size() << endl;
	cout << "****************************************************" << endl << endl;
      }

      this->broadcastPacket(pkt);

      // ############# statistic ##############
      // add 1 in the sendpkts by sending one data packet
      sendpkts++;
      // add an entry for the vectors    
      sendPktID.push_back(hdrcmn->uid_);
      receivePktID.push_back(0);
 
	      
      // for statistics convenience, if the source which sends the multicast packet is in the multicast region, then we think it receives its owm broadcasted packet, add 1 on corresponding deliveredPktID
      if ( this->inGeocastRegion(geohdr->lowerLeftX_, geohdr->lowerLeftY_, geohdr->upperRightX_, geohdr->upperRightY_))
      {
 		receivePktID[sendpkts-1]++;
		if (receivePktID[sendpkts-1] == 1) // first reach the geocast region
		{
			oneSuccess++;
			totalEndToEndDelay += Scheduler::instance().clock()-geohdr->sendTime(); 
		
		// to calculate approx. number of hops from source to geocast region
	       // no change to totalnumHops 	
			
		}
      }     
      
      // return TCL_OK, so the calling function knows that the
      // command has been processed
      return (TCL_OK);
     //}
    }
  } else if (argc == 8) {
    if (strcmp(argv[1], "setGeocastRegion") == 0)
    {
      geocastRegionType   newRegion;
      newRegion.llx = atoi(argv[2]);
      newRegion.lly = atoi(argv[3]);
      newRegion.urx = atoi(argv[4]);
      newRegion.ury = atoi(argv[5]);
      newRegion.method = (char)argv[6][0]; //right now this one is not used
      /* ---------------------------------------------
       * Initialize the forwarding code
       * may need to modify according to different
       * order of try
       * ---------------------------------------------*/
      current = 'U';
      next = 'C';
      int regionID = 0;
      regionID = atoi(argv[7]);

      geocastRegions[regionID] = newRegion;

      // return TCL_OK, so the calling function knows that the
      // command has been processed
      return (TCL_OK);
    }
  }
  // If the command hasn't been processed by GamerAgent()::command,
  // call the command() function for the base class
  return (Agent::command(argc, argv));
}


// ***********************************************************************
//                            recv
// ***********************************************************************
void GamerAgent::recv(Packet* pkt, Handler*)
{
  // Access the common header for the received packet:
  /*
  hdr_cmn* hdrcmn = (hdr_cmn*)pkt->access(off_cmn_);

  // Access the IP header for the received packet:
  hdr_ip* hdrip = (hdr_ip*)pkt->access(off_ip_);

  // Access the Gamer header for the received packet:
  hdr_gamer* hdr = (hdr_gamer*)pkt->access(off_gamer_);*/
  
  // Access the headers for the received packet:
  struct hdr_gamer* hdr = hdr_gamer::access(pkt);
  struct hdr_cmn* hdrcmn = hdr_cmn::access(pkt);
  struct hdr_ip* hdrip = hdr_ip::access(pkt);  

  node->update_position();

  // Is it a duplicate packet?
  if (this->dupRcvdPacket(hdrcmn->uid_) || this->dupSentPacket(hdrcmn->uid_))
  {
    // already seen it, or I sent it, just free it and drop it
    if (verbosity == 2)
    {
      cout << "****************************************************" << endl;
      cout << "In Gamer recv at node:  " << this->addr()
           << " at [" << node->X() << "," << node->Y() << "]" << endl;
      cout << "Time:  " << Scheduler::instance().clock() << endl;
      cout << "Route thus far:  ";
/*~~~~~~~~~~~~~~~~~~change source routing to local routing~~~~~~~~~~~~~
      for (int i = 0; i < hdr->hops(); i++)
      {
        cout << hdr->route_[i] << "  ";
      }
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
      cout << endl;
      cout << "Packet ID:  " << hdrcmn->uid_ 
           << "     Type:  " << hdr->gamerCode() << endl;
      cout << "Duplicate packet seen from " << hdrcmn->prev_hop_ 
           << ", freeing it." << endl;
      cout << "Returning from recv early" << endl;
      cout << "Time:  " << Scheduler::instance().clock() << endl;
      cout << "Packet Size: " << hdr->size() << endl; 
      cout << "****************************************************" << endl << endl;
    }
    Packet::free(pkt);
    return;
  } else {
    // This is redundant for now because dupRcvdPacket records the packet
    // as recieved, but I don't like that side effect and may change it
    // later.
    recordRcvdPacket(hdrcmn->uid_);
  }
  if (verbosity > 0)
  {
	cout << "****************************************************" << endl;
	cout << "In Gamer recv at node:  " << this->addr()
     		<< " at [" << node->X() << "," << node->Y() << "]" << endl;
	cout << "Time:  " << Scheduler::instance().clock() << endl;

/*~~~~~~~~~~~~~~~~~~change source routing to local routing~~~~~~~~~~~~~
	cout << "Route thus far:  ";

	for (int i=0; i<hdr->hops(); i++)
	{
  	cout << hdr->route_[i] << "  ";
	}
	cout << endl;
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/	
        cout << "hdrcmn->size() = " << hdrcmn->size() << endl;
        cout << "hdr->size() = " << hdr->size() << endl;	
	cout << "Packet Size: " << hdr->size() << endl;
  }

  // What kind of packet is it?
  // ***********************************************************************
  //                            Join Demand
  // ***********************************************************************
  if (hdr->gamerCode() == 'J')
  {
    if (verbosity > 0)
    {
	cout << "Join Demand packet seen" << endl;
	cout << "From:       " << hdrip->src_.addr_ 
     		<< " at [" << hdr->sourceX() << "," << hdr->sourceY() << "]" << endl;
	cout << "Last hop:   " << hdrcmn->prev_hop_ << endl;
	cout << "Packet ID:  " << hdrcmn->uid_ << endl;
    }
    
    //printHeaders(pkt);
    
    if (this->inGeocastRegion(hdr->lowerLeftX(), hdr->lowerLeftY(),
                       hdr->upperRightX(), hdr->upperRightY()))
    {
      if (sparseMeshMode)
      {
        if (hdr->repliedTo())
        {
	  if (verbosity > 0)
	  {
		cout << "Replied To flag is set, recording packet ID and ignoring." << endl;
		cout << "****************************************************" << endl << endl;
          }
	  repliedToDemands.insert(hdrcmn->uid_);
          Packet::free(pkt);
          return;
        } else if (alreadyRepliedTo(hdrcmn->uid_)) {
	  if (verbosity > 0)
	  {
		cout << "This demand has already been replied to by an immediate neighbor." << endl;
		cout << "Ignoring this demand to keep the mesh sparse." << endl;
		cout << "****************************************************" << endl << endl;
          }
	  Packet::free(pkt);
          return;
        } else {
          // set the replied to flag and rebroadcast the packet to all the
          // neighbors
	  if (verbosity > 0)
		cout << "In sparse mesh mode, so rebroadcasting with replied to flag set." << endl;
          hdr->repliedTo() = true;
/*~~~~~~~~~~~~~~~~~~change source routing to local routing~~~~~~~~~~~~~	  
          hdr->route_[hdr->hops()] = this->addr();
          hdr->hops()++;
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
          this->rebroadcastPacket(pkt);
        }
      }
      // I need to reply with a join table
      // Create a new packet
      Packet* newpkt = allocpkt();
      // Access the headers for the new packet:
      /*
      hdr_gamer* newhdr = (hdr_gamer*)newpkt->access(off_gamer_);
      hdr_cmn* newhdrcmn = (hdr_cmn*)newpkt->access(off_cmn_);*/
  
      struct hdr_gamer* newhdr = hdr_gamer::access(newpkt);
      struct hdr_cmn* newhdrcmn = hdr_cmn::access(newpkt);

/*~~~~~~~~~~~~~~~~~~change source routing to local routing~~~~~~~~~~~~~	 
      newhdr->hops() = 0;
      // puts the route that the join demand took in the join table
      for (int i=0; i<hdr->hops(); i++)
      {
        newhdr->route_[i] = hdr->route_[i];
      }
      newhdr->hops() = hdr->hops();
      / adds the final destination to the route
      newhdr->route_[newhdr->hops()] = this->addr();
      newhdr->hops()++;
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

      // Store the current time in the 'send_time' field
      newhdr->sendTime_ = Scheduler::instance().clock();
      newhdr->sourceX_ = node->X();
      newhdr->sourceY_ = node->Y();
      newhdr->lowerLeftX_ = hdr->lowerLeftX();
      newhdr->lowerLeftY_ = hdr->lowerLeftY();
      newhdr->upperRightX_ = hdr->upperRightX();
      newhdr->upperRightY_ = hdr->upperRightY();
      newhdr->gamerCode_ = 'T';
      newhdr->forwardCode_ = hdr->forwardCode();
      newhdr->repliedTo() = false;
      
      // the demandID is the packet ID of recieved JD
      newhdr->demandID_ = hdrcmn->uid_;
      newhdr->demandSendTime_ = hdr->demandSendTime();
      newhdr->dataLength_ = 0;

      if (verbosity > 0)
      {
	cout << "Sending a Join Table, ID:  " << newhdrcmn->uid_ 
     		<< " to " << hdrcmn->prev_hop_ << endl;
      }
      //this->broadcastPacket(newpkt);
      // Don't want to rebroadcast, want send it to my "upstream" neighbor

      this->sendPacket(newpkt, hdrcmn->prev_hop_);

      if (!sparseMeshMode)
      {
        if (verbosity > 0)
		cout << "Freeing join demand packet" << endl;

        Packet::free(pkt);
      }
    } else if (this->inForwardingRegion(hdr->forwardCode(), 
                                        hdr->sourceX(), hdr->sourceY(),
                                        hdr->lowerLeftX(), hdr->lowerLeftY(),
                                        hdr->upperRightX(), hdr->upperRightY())) {
      if (hdr->repliedTo())
      {
        // this join demand has been replied to, which means I received a 
        // rebroadcast from a node in the gamer region already, so I don't
        // need to rebroadcast it
	if (verbosity > 0)
	{
		cout << "In the forwarding region, but..." << endl;
		cout << "Received a join demand that has already been replied to." << endl;
		cout << "Must have been rebroadcast by a node in the gamer region." << endl;
		cout << "Freeing join demand packet" << endl;
	}	
        Packet::free(pkt);
      } else {
        // re-broadcast the packet, because I'm in the forwarding region,
        // and join demands flood in this region
	if (verbosity > 0)
		cout << "Rebroadcasting, forwarding node, but not in region..." << endl;
/*~~~~~~~~~~~~~~~~~~change source routing to local routing~~~~~~~~~~~~~
        hdr->route_[hdr->hops()] = this->addr();
        hdr->hops()++;
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	
/*~~~~~~~~~~~~~~~~~~change source routing to local routing~~~~~~~~~~~~~	*/
      // record that I forward the join demand
      // and the hop I received the join demand from
      
      localRoute newLastHop;
      newLastHop.demandID_ = hdrcmn->uid_;
      newLastHop.lastHop_ = hdrcmn->prev_hop_;
      lastHopOfJD[hdrcmn->uid_] = newLastHop;
      if (verbosity > 0)
      {
      	cout << "In node " << this-> addr() << " receive a JD" << endl;
	cout << "Agent of this node record:" << endl;
	cout << "JD UID = " << newLastHop.demandID_ << endl;
	cout << "Last hop = " << newLastHop.lastHop_ << endl;
      }            
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	
        this->rebroadcastPacket(pkt);
      }
    } else {
      if (verbosity > 0)
	cout << "Not in geocast/gamer region or forwarding region.  Ignoring." << endl;
      // don't really need an else here because I take care of duplicate
      // packets up above
    }
  // ***********************************************************************
  //                            Join Table
  // ***********************************************************************
  } else if (hdr->gamerCode() == 'T') {
    if (verbosity > 0)
    {
	cout << "Join Table packet seen" << endl;
	cout << "From:       " << hdrip->src_.addr_ 
     		<< " at [" << hdr->sourceX() << "," << hdr->sourceY() << "]" << endl;
	cout << "Last hop:   " << hdrcmn->prev_hop_ << endl;
	cout << "Packet ID:  " << hdrcmn->uid_ << endl;
	cout << "Demand ID:  " << hdr->demandID_ << endl;
    }	
    // record the time of this join table
    timeOfLastJoinTable = Scheduler::instance().clock();

/*~~~~~~~~~~~~~~~~~~change source routing to local routing~~~~~~~~~~~~~
    if (originator.find(hdr->demandID_) != originator.end())
    {
      // I sent the corresponding join demand, and the table just got back
      // to me.
      if (verbosity > 0)
      {
	cout << "Back at originating node!  Ignoring." << endl;
	cout << "Join Demand sent at " << hdr->demandSendTime_
     		<< " received at " << Scheduler::instance().clock() << endl;
	cout << "Round trip delay = " 
     		<< Scheduler::instance().clock() - hdr->demandSendTime_ << endl;
      }
      Packet::free(pkt);
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

/*~~~~~~~~~~~~~~~~~~change source routing to local routing~~~~~~~~~~~~~	*/
map<int, localRoute, less<int> >::iterator  localPtr;
localPtr = lastHopOfJD.find(hdr->demandID_);
if (localPtr == lastHopOfJD.end())
{
        // I haven't received the relative JD
        fprintf(stderr, "No JD seen before JT\n");
	fprintf(stderr, "Can't find the relative JD!!");
        return;
}

nsaddr_t preHopInJDRoute = localPtr->second.lastHop_;

if ( verbosity > 0 )
{
	cout << "In node " << this->addr() << " receive a JT" << endl;
	cout << "Agent of this node find local route:" << endl;
	cout << "Corresponding JD UID = " << hdr->demandID_ << endl;
	cout << "Found the last hop in the route = " << preHopInJDRoute << endl;
}

if ( preHopInJDRoute == this->addr())
 {
      // I sent the corresponding join demand, and the table just got back
      // to me.
      // I am the sender of the relative JD
      if (verbosity > 0)
      {
	cout << "Back at originating node!  Ignoring." << endl;
	cout << "Join Demand sent at " << hdr->demandSendTime_
     		<< " received at " << Scheduler::instance().clock() << endl;
	cout << "Round trip delay = " 
     		<< Scheduler::instance().clock() - hdr->demandSendTime_ << endl;
      }
      Packet::free(pkt);
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/ 
   
      /*-----------------------------------------------------------
       *Need to cancel the rtx_timer after the JOIN_TABLE come back
       *-----------------------------------------------------------*/
      if(rtx_timer_.status() == TIMER_PENDING)
         { rtx_timer_.cancel();
	   if (verbosity > 0)
	   {
           	cout<<" ***********************************************\n";
           	cout<<" Timer is canceld after the JOIN_TABLE gets back \n";
           }
	  }
      /* -----------------------------------------------------------------------
       * Also need to update the current fowarding code and try forwarding code
       * ----------------------------------------------------------------------*/
       updateFwdCode(true); 
      
    } else if (alreadyForwardedTable(hdr->demandID_)) {
      if (verbosity > 0)
      {
	cout << "I've already forwarded a table back upstream for this demand." << endl;
	cout << "Ignoring..." << endl;
	cout << "****************************************************" << endl << endl;
      }
      Packet::free(pkt);
      return;
    } else {
      // if I get a join table and am not the sender, then I must be
      // the addressee of the table, and I should be sure to become a
      // mesh member
      meshMember = true;
      // record that I've forwarded a table for this demandID already.
      tablesForwardedByDemand.insert(hdr->demandID_);
      // I then send it to the next "upstream" neighbor to build the mesh
      nsaddr_t dest;

/*~~~~~~~~~~~~~~~~~~change source routing to local routing~~~~~~~~~~~~~
      dest = hdr->route_[0];
      for (int i=0; i<hdr->hops(); i++)
      {
        dest = hdr->route_[i];
        if (dest == this->addr())
        {
          // after the find, dest is pointing to my address, so decrement it to 
          // the previous one in the route
          dest = hdr->route_[i-1];
          break;
        }
      }
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

/*~~~~~~~~~~~~~~~~~~change source routing to local routing~~~~~~~~~~~~~	*/
dest = preHopInJDRoute;
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/     
      
      if (verbosity > 0)
	cout << "Re-sending a Join Table, ID:  " << hdrcmn->uid_ 
     		<< " to " << dest << endl;
      this->sendPacket(pkt, dest);

    }
  // ***********************************************************************
  //                            Data Packet
  // ***********************************************************************
  } else if (hdr->gamerCode() == 'D') {
    if (verbosity > 0)
    {
	cout << "Gamer data packet seen" << endl;
	cout << "From:       " << hdrip->src_.addr_ 
     		<< " at [" << hdr->sourceX() << "," << hdr->sourceY() << "]" << endl;
	cout << "Last hop:   " << hdrcmn->prev_hop_ << endl;
	cout << "Packet ID:  " << hdrcmn->uid_ << endl;
	cout << "Demand ID:  " << hdr->demandID_ << endl;
    }
    if (this->inGeocastRegion(hdr->lowerLeftX(), hdr->lowerLeftY(),
                       hdr->upperRightX(), hdr->upperRightY()))
    {
      if (verbosity > 0)
      {
      	// print some nice informational message
      	cout << "Recieved a data packet sucessfully!!!" << endl;
      	cout << "Join demand sent at:  " << hdr->demandSendTime() << endl;
      	cout << "Received at:          " << Scheduler::instance().clock() << endl;
      	cout << "Total Delay:          " 
           << Scheduler::instance().clock()-hdr->demandSendTime() << endl;
	cout << "Rebroadcasting data packet to flood in geocast region..." << endl;
      }
      
/*~~~~~~~~~~~~~~~~~~change source routing to local routing~~~~~~~~~~~~~      
      hdr->route_[hdr->hops()] = this->addr();
      hdr->hops()++;
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/      
      
        // ############# statistic ##############
	// I receive the data packet successfully
	
	// if the data packet is first arrival in the geocast region,
	// count it to the received data packets, oneSuccess++
	// if no, other node has already count it
	for (int m = 0; m < sendpkts; m++)
	{
		if (sendPktID[m] == hdrcmn->uid_)
		{
			receivePktID[m]++;
			if (receivePktID[m] == 1) // first reach the geocast region
			{
				oneSuccess++;
				totalEndToEndDelay += Scheduler::instance().clock()-hdr->sendTime(); 
			
				// to calculate approx. number of hops from source to geocast region
	    			totalnumHops += hdr->numHops;	
				
			}
		}
	}
	
      this->rebroadcastPacket(pkt);

	if (verbosity > 0)	
		cout << "****************************************************" << endl << endl;
      	// Discard the packet
  	// can't free the packet if it is being rebroadcast.  This would create a segmentation
  	// fault
  	//      Packet::free(pkt);
      return;
    } else if (meshMember) {
      if (Scheduler::instance().clock() - timeOfLastJoinTable 
                      <= meshMemberTimeout)
      {
        // re-broadcast the packet
        // Note, mesh members rebroadcast data packets, not like join tables,
        // where they only send to the "upstream" neighbor
	if (verbosity > 0)
		cout << "Rebroadcasting, forwarding node, but not in region..." << endl;

/*~~~~~~~~~~~~~~~~~~change source routing to local routing~~~~~~~~~~~~~
        hdr->route_[hdr->hops()] = this->addr();
        hdr->hops()++;
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/	
	
        this->rebroadcastPacket(pkt);
      } else {
        // I've been a mesh member too long without receiving a join table
	if (verbosity > 0)
		cout << "Mesh membership timed out." << endl;
        meshMember = false;
      }
    }

    // again, don't really need an else here because I take care of duplicate
    // packets up above

  } else if (hdr->gamerCode() == 'U') {
    // there is an error if this happens, but I'm not sure how to
    // respond, so it is commented out for now
    cerr << "The gamer code is unset" << endl;
  }
  
  if (verbosity > 0)
  {
	cout << "Returning from recv at the bottom" << endl;
	cout << "Time:  " << Scheduler::instance().clock() << endl;
	cout << "****************************************************" << endl << endl;
  }
  return;
}

// ***********************************************************************
//                            alreadyForwardedTable
// ***********************************************************************
bool GamerAgent::alreadyForwardedTable(int id)
{
  // I may need to limit the size of this in the future.
  if (tablesForwardedByDemand.find(id) != tablesForwardedByDemand.end())
  {
    return true;
  } else {
    return false;
  }
}

// ***********************************************************************
//                            alreadyRepliedTo
// ***********************************************************************
bool GamerAgent::alreadyRepliedTo(int id)
{
  // I may need to limit the size of this in the future.
  if (repliedToDemands.find(id) != repliedToDemands.end())
  {
    return true;
  } else {
    //repliedToDemands.insert(id);
    return false;
  }
}

// ***********************************************************************
//                            dupRcvdPacket
// ***********************************************************************
bool GamerAgent::dupRcvdPacket(int id)
{
  // I may need to limit the size of this in the future.
  if (rcvdPackets.find(id) != rcvdPackets.end())
  {
    return true;
  } else {
    rcvdPackets.insert(id);
    return false;
  }
}

// ***********************************************************************
//                            dupSentPacket
// ***********************************************************************
bool GamerAgent::dupSentPacket(int id)
{
  // I may need to limit the size of this in the future.
  if (sentPackets.find(id) != sentPackets.end())
  {
    return true;
  } else {
    sentPackets.insert(id);
    return false;
  }
}

// ***********************************************************************
//                            inForwardingRegion
// ***********************************************************************
bool GamerAgent::inForwardingRegion(char fwdCode,
                                      double X, double Y,
                                      double llx, double lly, 
                                      double urx, double ury)
{
  node->update_position();
  if (fwdCode == 'F')
  {
    if (verbosity > 0)
	cout << "fwdCode=flood" << endl;
    return true;
  } else if (fwdCode == 'B') {
    if (verbosity > 0)
	cout << "fwdCode=box" << endl;
    if ((node->X() >= Gamer_min(X,llx)) && (node->X() <= Gamer_max(X,urx)) &&
        (node->Y() >= Gamer_min(Y,lly)) && (node->Y() <= Gamer_max(Y,ury)))
    {
      return true;
    } else {
      return false;
    }
  } else if (fwdCode == 'C') {
    if (verbosity > 0)
	cout << "fwdCode=cone" << flush;
    Pointxy p(node->X(),node->Y());
    vector<Pointxy> hull;
    hull.push_back(Pointxy(X, Y));

    // First of all, determine where the point lies in relation to the the
    // geocast region.  There are eight possibilities.
    // Read the comments in the inHull routine.  I think the points must be
    // counter clock wise in the hull vector.
    if ((X < llx) && (Y < lly))
    {
      if (verbosity > 0)
	cout << "\tDirection=sw " << endl;
      // it is in the south west quadrant
      hull.push_back(Pointxy(urx,lly));
      hull.push_back(Pointxy(urx,ury));
      hull.push_back(Pointxy(llx,ury));
    } else if ((X > urx) && (Y > ury)) {
      if (verbosity > 0)    
	cout << "\tDirection=ne " << endl;
      // it is in the north east quadrant
      hull.push_back(Pointxy(llx,ury));
      hull.push_back(Pointxy(llx,lly));
      hull.push_back(Pointxy(urx,lly));
    } else if ((X < llx) && (Y > ury)) {
      if (verbosity > 0)     
	cout << "\tDirection=nw " << endl;
      // it is in the north west quadrant
      hull.push_back(Pointxy(llx,lly));
      hull.push_back(Pointxy(urx,lly));
      hull.push_back(Pointxy(urx,ury));
    } else if ((X > urx) && (Y < lly)) {
      if (verbosity > 0)    
	cout << "\tDirection=se " << endl;
      // it is in the south east quadrant
      hull.push_back(Pointxy(urx,ury));
      hull.push_back(Pointxy(llx,ury));
      hull.push_back(Pointxy(llx,lly));
    } else if ((X >= llx) && (X <= urx) && (Y <= lly)) {
      if (verbosity > 0)     
	cout << "\tDirection=s " << endl;
      // it is due south
      hull.push_back(Pointxy(urx,lly));
      hull.push_back(Pointxy(llx,lly));
    } else if ((X >= llx) && (X <= urx) && (Y >= ury)) {
      if (verbosity > 0)     
	cout << "\tDirection=n " << endl;
      // it is due north
      hull.push_back(Pointxy(llx,ury));
      hull.push_back(Pointxy(urx,ury));
    } else if ((Y >= lly) && (Y <= ury) && (X <= llx)) {
      if (verbosity > 0)
	cout << "\tDirection=w " << endl;
      // it is due west
      hull.push_back(Pointxy(llx,lly));
      hull.push_back(Pointxy(llx,ury));
    } else if ((Y >= lly) && (Y <= ury) && (X >= urx)) {
      if (verbosity > 0)     
	cout << "\tDirection=e " << endl;
      // it is due east
      hull.push_back(Pointxy(urx,ury));
      hull.push_back(Pointxy(urx,lly));
    }

    // By the way, this will return true if the point (X,Y) is in the geocast
    // region also, not just the forwarding zone.  This is because the inHull
    // routine expects a convex hull.  In order to work it out in all cases to
    // be in the forwarding region and not the geocast region, inHull would have
    // to work with non-convex hulls, which is a much harder problem.
    return inHull(p, hull);
  } else if (fwdCode == 'D')
  {
    if (verbosity > 0)   
    	cout<<"fwdCode=Corridor" << flush;

    Pointxy p (node->X(), node->Y());
    Pointxy sp (X, Y);
    Pointxy g_ll(llx,lly);
    Pointxy g_rl(urx,lly);
    Pointxy g_lu(llx,ury);
    Pointxy g_ru(urx, ury);
    vector< Pointxy > hull;
    //get the center point of geocast region
    double cx = llx + (urx - llx)/2;
    double cy = lly + (ury - lly)/2;
    Pointxy cp (cx, cy);
  
   //There are 8 cases for the relation of the point to the geocast region
     //(1) south west - sw
    if ((X < cx) && (Y<cy))
     {
      if (verbosity > 0) 
      	cout<<"\tDirection=sw"<<endl;
      //it is in the south west quadrant
      //get the center line the lines parallel and perpendicular to it
      GamerLine cline = getLineTwoP(sp, cp);
      GamerLine lline = getLineOneP(g_rl, cline.getA());
      GamerLine uline = getLineOneP(g_lu, cline.getA());
      GamerLine pline = getLineOneP(sp, (-1/cline.getA()));
      //get the cross point of those lines
      Pointxy p1 = crossPoint(pline, lline);
      Pointxy p5 = crossPoint(pline, uline);
      hull.push_back(p1);
      hull.push_back(g_rl);
      hull.push_back(g_ru);
      hull.push_back(g_lu);
      hull.push_back(p5);
     }else if ((X > cx) && (Y < cy))
     {// (2) south east - se
      if (verbosity > 0)      
      	cout<<"\tDirection=se"<<endl;
      //get the center line the lines parallel and perpendicular to it
      GamerLine cline = getLineTwoP(sp, cp);
      GamerLine uline = getLineOneP(g_ru, cline.getA());
      GamerLine lline = getLineOneP(g_ll, cline.getA());
      GamerLine pline = getLineOneP(sp, (-1/cline.getA()));
      //get the cross point of those lines
      Pointxy p1 = crossPoint(pline, lline);
      Pointxy p2 = crossPoint(pline, uline);
      hull.push_back(p1);
      hull.push_back(p2);
      hull.push_back(g_ru);
      hull.push_back(g_lu);
      hull.push_back(g_ll);
     } else    if(( X< cx) && ( Y > cy))
     { // (3) north west - nw
      if (verbosity > 0) 
      	cout<<"\tDirection=nw"<<endl;
      //get the center line the lines parallel and perpendicular to it
      GamerLine cline = getLineTwoP(sp, cp);
      GamerLine uline = getLineOneP(g_ru, cline.getA());
      GamerLine lline = getLineOneP(g_ll, cline.getA());
      GamerLine pline = getLineOneP(sp, (-1/cline.getA()));
      //get the cross point of those lines
      Pointxy p3 = crossPoint(pline, lline);
      Pointxy p4 = crossPoint(pline, uline);
      hull.push_back(g_rl);
      hull.push_back(g_ru);
      hull.push_back(p3);
      hull.push_back(p4);
      hull.push_back(g_ll);
     }else   if(( X> cx) && ( Y > cy))
     {//(4) north east - ne 
      if (verbosity > 0)      
      	cout<<"\tDirection=ne"<<endl;
      //get the center line the lines parallel and perpendicular to it
      GamerLine cline = getLineTwoP(sp, cp);
      GamerLine uline = getLineOneP(g_lu, cline.getA());
      GamerLine lline = getLineOneP(g_rl, cline.getA());
      GamerLine pline = getLineOneP(sp, (-1/cline.getA()));
      //get the cross point of those lines
      Pointxy p2 = crossPoint(pline, lline);
      Pointxy p3 = crossPoint(pline, uline);
      hull.push_back(g_rl);
      hull.push_back(p2);
      hull.push_back(p3);
      hull.push_back(g_lu);
      hull.push_back(g_ll);
     } else  if ((X == cx) && (Y < cy))
      { // (5) south
       if (verbosity > 0)       
       	cout<<"\tDirection=s"<<endl;
       //get the lines
       GamerLine lline = getLineTwoP (g_ll, g_lu);
       GamerLine rline = getLineTwoP (g_rl, g_ru);
       GamerLine pline = getLineOneP ( sp, (-1/rline.getA()));
       Pointxy p1 = crossPoint(pline, rline);
       Pointxy p4 = crossPoint(pline, lline);
       hull.push_back(p1);
       hull.push_back(g_ru);
       hull.push_back(g_lu);
       hull.push_back(p4);
      }else if ((X < cx) && (Y == cy))
      { //(7) west
       if (verbosity > 0)       
       	cout<<"\tDirection=w"<<endl;
       //get the lines
       GamerLine lline = getLineTwoP (g_ll, g_rl);
       GamerLine uline = getLineTwoP (g_lu, g_ru);
       GamerLine hline = getLineOneP (sp, (-1/uline.getA()));
       Pointxy p3 = crossPoint  (uline, hline); 
       Pointxy p4 = crossPoint  (lline, hline); 
       hull.push_back(g_rl);
       hull.push_back(g_ru);
       hull.push_back(p3);
       hull.push_back(p4);
      } else if ((X > cx) && (Y == cy))
      { //   (8) east
       if (verbosity > 0)       
       	cout<<"\tDirection=e"<<endl;
       //get the lines
       GamerLine lline = getLineTwoP (g_ll, g_rl);
       GamerLine uline = getLineTwoP (g_lu, g_ru);
       GamerLine hline = getLineOneP (sp, (-1/lline.getA()));
       Pointxy p2 = crossPoint  (lline, hline);
       Pointxy p3 = crossPoint  (uline, hline);
       hull.push_back(g_ll);
       hull.push_back(p2);
       hull.push_back(p3);
       hull.push_back(g_lu);
      }
  
     return inHull(p,hull);
  }
  return true;
}

// ***********************************************************************
//                            inGeocastRegion
// ***********************************************************************
bool GamerAgent::inGeocastRegion(double llx, double lly, double urx, double ury)
{
  node->update_position();
  if ((node->X() > llx) && (node->X() < urx) &&
      (node->Y() > lly) && (node->Y() < ury))
  {
    return true;
  } else {
    return false;
  }
}

// ***********************************************************************
//                            recordSentPacket
// ***********************************************************************
void GamerAgent::recordSentPacket(int id)
{
  sentPackets.insert(id);
  return;
}
  
// ***********************************************************************
//                            recordRcvdPacket
// ***********************************************************************
void GamerAgent::recordRcvdPacket(int id)
{
  rcvdPackets.insert(id);
  return;
}

void printHeaders(Packet *p)
{
  // Access the Gamer header for the new packet:
  //hdr_gamer* geohdr = (hdr_gamer*)p->access(off_gamer_);

  // Access the common header for the new packet:
  hdr_cmn* cmnhdr = (hdr_cmn*)p->access(hdr_cmn::offset());

  // Access the IP header for the new packet:
  hdr_ip* iphdr = (hdr_ip*)p->access(hdr_ip::offset());

  // Access the mac header for the new packet:
  hdr_mac* machdr = (hdr_mac*)p->access(hdr_mac::offset());
  cout << "IP Header Information:" << endl;
  cout << "\tSrc Address:   " << iphdr->saddr() << endl;
  cout << "\tSrc Port:      " << iphdr->sport() << endl;
  cout << "\tDest Address:  " << iphdr->daddr() << endl;
  cout << "\tDest Port:     " << iphdr->dport() << endl;
  cout << "\tTTL:           " << iphdr->ttl() << endl;

  cout << endl << "Common Header Information:" << endl;
  cout << "\tPacket Type:   " << p_info().name(cmnhdr->ptype()) << endl;
//  cout << "\tPacket Type:   " << cmnhdr->ptype() << endl;
  cout << "\tSize:          " << cmnhdr->size() << endl;
  cout << "\tUID:           " << cmnhdr->uid() << endl;
  cout << "\terror:         " << cmnhdr->error() << endl;
  cout << "\ttimestamp:     " << cmnhdr->timestamp() << endl;
  cout << "\tinterface:     " << cmnhdr->iface() << endl;
  cout << "\tDirection:     " << cmnhdr->direction() << endl;
  cout << "\tref count:     " << cmnhdr->ref_count() << endl;
  cout << "\tprev hop:      " << cmnhdr->prev_hop_ << endl;
  cout << "\tnext hop:      " << cmnhdr->next_hop() << endl;
  cout << "\taddress type:  " << cmnhdr->addr_type() << endl;
  cout << "\tlast hop:      " << cmnhdr->last_hop_ << endl;
  cout << "\tnum forwards:  " << cmnhdr->num_forwards() << endl;
  cout << "\topt forwards:  " << cmnhdr->opt_num_forwards() << endl;

  cout << endl << "MAC Header Information:" << endl;
  cout << "\tSrc Address:   " << machdr->macSA() << endl;
  cout << "\tDest Address:  " << machdr->macDA() << endl;

  return;
}

// ***********************************************************************
//                            sendPacket
// ***********************************************************************
void GamerAgent::sendPacket(Packet *p, nsaddr_t to)
{
  /*
  // Access the common header for the new packet:
  hdr_cmn* cmnhdr = (hdr_cmn*)p->access(off_cmn_);
  // Access the IP header for the new packet:
  hdr_ip* iphdr = (hdr_ip*)p->access(off_ip_);
  // Access the gamer header for the new packet:
  hdr_gamer* geohdr = (hdr_gamer*)p->access(off_gamer_);*/
  
  // Access the headers for the received packet:

  struct hdr_gamer* geohdr = hdr_gamer::access(p);
  struct hdr_cmn* cmnhdr = hdr_cmn::access(p);
  struct hdr_ip* iphdr = hdr_ip::access(p);


  //printHeaders(p);

  // set all the necessary things for the common header
  cmnhdr->next_hop_ = to;
  cmnhdr->prev_hop_ = this->addr();
  //cmnhdr->next_hop_ = 1;
  //cmnhdr->iface() = iface_literal::ANY_IFACE;    // any interface
  cmnhdr->direction() = hdr_cmn::DOWN;    // hopefully send it out
  //cmnhdr->direction() = hdr_cmn::UP;

  // now the ip header stuff
  iphdr->saddr() = this->addr();
  iphdr->sport() = 254;
  iphdr->daddr() = to;
  //iphdr->daddr() = 1;
  iphdr->dport() = 254;
  //iphdr->dport() = 255;         // router port
  iphdr->ttl() = 32;

  //cout << endl << "After Modification" << endl;
  //printHeaders(p);

  // Send the packet
  recordSentPacket(cmnhdr->uid_);
  //cout << "Gamer/Geocast:  Sending a packet..." << endl;

  // The next three lines should all be equivilant ways to send a packet,
  // I'm just trying them all to get the broadcast to go.
  //send(p, 0);
  //send(p, ll);
  //target_->recv(p, (Handler*) 0);
  //Scheduler::instance().schedule(target_,p,0);

  if (useJitteronSend)
  {
    // this one is different and was taken from dsragent::sendOutBCastPkt
    // I had to add a little jitter because it turned out that neighboring nodes
    // Where re-broadcasting in the simulator at "exactly" the same time and
    // killing each other's transmissions.
    double jitter = ((double)rand()/(double)RAND_MAX) / gamerJitter;
    if (verbosity > 0)
    {
    	cout << "Scheduling the packet for delivery:  " << cmnhdr->uid_ 
         << " with jitter=" << jitter << endl;
    	cout << "Really sending at:  " << Scheduler::instance().clock() + jitter << endl;
    }
    Scheduler::instance().schedule(ll, p, jitter);
  } else {
    if (verbosity > 0) 
    {
    	cout << "Scheduling the packet for delivery:  " << cmnhdr->uid_ << endl;
    	cout << "Sending at:  " << Scheduler::instance().clock() << endl;
    	cout << "Common header size:  " << cmnhdr->size_ << endl;
	cout << "Common header size:  " << cmnhdr->size_ << endl;
    }	
    cmnhdr->size_ = geohdr->size();
    Scheduler::instance().schedule(ll, p, 0.0);
  }


  if (!ll)
  {
    cerr << "Crap, the link layer is NULL!!!!!" << endl;
  }

//cout << "Gamer:  Done sending." << endl;

  return;
}

// ***********************************************************************
//                            broadcastPacket
// ***********************************************************************
void GamerAgent::broadcastPacket(Packet *p)
{
  /*
  // Access the common header for the new packet:
  hdr_cmn* cmnhdr = (hdr_cmn*)p->access(off_cmn_);

  // Access the IP header for the new packet:
  hdr_ip* iphdr = (hdr_ip*)p->access(off_ip_);

  // Access the gamer header for the new packet:
  hdr_gamer* geohdr = (hdr_gamer*)p->access(off_gamer_);*/
  
  // Access the headers for the received packet:

  struct hdr_gamer* geohdr = hdr_gamer::access(p);
  struct hdr_cmn* cmnhdr = hdr_cmn::access(p);
  struct hdr_ip* iphdr = hdr_ip::access(p);


  //printHeaders(p);

  // set all the necessary things for the common header
  cmnhdr->next_hop_ = IP_BROADCAST;  // broadcast
  cmnhdr->prev_hop_ = this->addr();
  //cmnhdr->next_hop_ = 1;
  //cmnhdr->iface() = iface_literal::ANY_IFACE;    // any interface
  cmnhdr->direction() = hdr_cmn::DOWN;    // hopefully send it out
  //cmnhdr->direction() = hdr_cmn::UP;

  // now the ip header stuff
  iphdr->saddr() = this->addr();
  iphdr->sport() = 254;
  iphdr->daddr() = IP_BROADCAST;
  //iphdr->daddr() = 1;
  iphdr->dport() = 254;
  //iphdr->dport() = 255;         // router port
  iphdr->ttl() = 32;

  //cout << endl << "After Modification" << endl;
  //printHeaders(p);

  // Send the packet
  recordSentPacket(cmnhdr->uid_);
  //cout << "Gamer/Geocast:  Sending a packet..." << endl;

  // The next three lines should all be equivilant ways to send a packet,
  // I'm just trying them all to get the broadcast to go.
  //send(p, 0);
  //send(p, ll);
  //target_->recv(p, (Handler*) 0);
  //Scheduler::instance().schedule(target_,p,0);

  // this one is different and was taken from dsragent::sendOutBCastPkt
  // I had to add a little jitter because it turned out that neighboring nodes
  // where re-broadcasting in the simulator at "exactly" the same time and
  // killing each other's transmissions.
  double jitter = ((double)rand()/(double)RAND_MAX) / gamerJitter;
  
  if (verbosity > 0)
  {
	cout << "Scheduling the packet for delivery:  " << cmnhdr->uid_ 
     		<< " with jitter=" << jitter << endl;
	cout << "Really sending at:  " << Scheduler::instance().clock() + jitter << endl;
	//cout << "Common header size:  " << cmnhdr->size_ << endl;
	cout << "Common header size:  " << cmnhdr->size_ << endl;
  }

  cmnhdr->size_ = geohdr->size();
  Scheduler::instance().schedule(ll, p, jitter);

  // ############# statistic ##############
  totalTransmitPkts++;
  totalTransmitBytes += geohdr->size();
  if (geohdr->gamerCode() == 'D')
  {
  	totalTransmitDataPkts++;
  	totalTransmitDataBytes += geohdr->size();
  }
  else if (geohdr->gamerCode() == 'T' || geohdr->gamerCode() == 'J')
  {
  	totalTransmitCtrlPkts++;
  	totalTransmitCtrlBytes += geohdr->size();
  }

  if (!ll)
  {
    cerr << "Crap, the link layer is NULL!!!!!" << endl;
  }

//cout << "Gamer:  Done sending." << endl;

  return;
}

// ***********************************************************************
//                            rebroadcastPacket
// ***********************************************************************
// same as broadcastPacket, but 
//      - we don't change the ip header source address,
//      - we have to set previous hop,
//      - we need to decrement the ttl
//      - and we have to set the direction to down

void GamerAgent::rebroadcastPacket(Packet *p)
{
  /*
  // Access the common header for the new packet:
  hdr_cmn* cmnhdr = (hdr_cmn*)p->access(off_cmn_);

  // Access the IP header for the new packet:
  hdr_ip* iphdr = (hdr_ip*)p->access(off_ip_);

  // Access the gamer header for the new packet:
  hdr_gamer* geohdr = (hdr_gamer*)p->access(off_gamer_);*/
  
  // Access the headers for the received packet:

  struct hdr_gamer* geohdr = hdr_gamer::access(p);
  struct hdr_cmn* cmnhdr = hdr_cmn::access(p);
  struct hdr_ip* iphdr = hdr_ip::access(p);


  //printHeaders(p);

  // set all the necessary things for the common header
  cmnhdr->next_hop_ = IP_BROADCAST;  // broadcast
  cmnhdr->prev_hop_ = this->addr();
  //cmnhdr->next_hop_ = 1;
  //cmnhdr->iface() = iface_literal::ANY_IFACE;    // any interface
  cmnhdr->direction() = hdr_cmn::DOWN;    // hopefully send it out
  //cmnhdr->direction() = hdr_cmn::UP;

  // now the ip header stuff
  // don't reset this on a rebroadcast
  //iphdr->saddr() = this->addr();

  // this doesn't have to be done, (it should be ok from the original)
  // but just in case...
  iphdr->sport() = 254;
  iphdr->daddr() = IP_BROADCAST;
  //iphdr->daddr() = 1;
  iphdr->dport() = 254;
  //iphdr->dport() = 255;         // router port

  // this is a difference from the regular broadcast
  // it appears that this needs to be done
  // since this packet doesn't go through the routing agent, this is never
  // done anywhere but here
//cout << "ttl before decrement:  " << iphdr->ttl_ << endl;
  iphdr->ttl()--;

  // to calculate approx. number of hops from source to geocast region
  geohdr->numHops++;
  
  //cout << endl << "After Modification" << endl;
  //printHeaders(p);

  // Send the packet
  recordSentPacket(cmnhdr->uid_);
  //cout << "Geocast:  Sending a packet..." << endl;

  // The next three lines should all be equivilant ways to send a packet,
  // I'm just trying them all to get the broadcast to go.
  //send(p, 0);
  //send(p, ll);
  //target_->recv(p, (Handler*) 0);
  //Scheduler::instance().schedule(target_,p,0);

  // this one is different and was taken from dsragent::sendOutBCastPkt
  // I had to add a little jitter because it turned out that neighboring nodes
  // where re-broadcasting in the simulator at "exactly" the same time and
  // killing each other's transmissions.
  double jitter = ((double)rand()/(double)RAND_MAX) / gamerJitter;
  
  if (verbosity > 0)
  {
	cout << "Scheduling the packet for delivery:  " << cmnhdr->uid_ 
     		<< " with jitter=" << jitter << endl;
	cout << "Really sending at:  " << Scheduler::instance().clock() + jitter << endl;
	//cout << "Common header size:  " << cmnhdr->size_ << endl;
	cout << "Common header size:  " << cmnhdr->size_ << endl;
  }	
  cmnhdr->size_ = geohdr->size();
  Scheduler::instance().schedule(ll, p, jitter);

  // ############# statistic ##############
  totalTransmitPkts++;
  totalTransmitBytes += geohdr->size();
  if (geohdr->gamerCode() == 'D')
  {
  	totalTransmitDataPkts++;
  	totalTransmitDataBytes += geohdr->size();
  }
  else if (geohdr->gamerCode() == 'T' || geohdr->gamerCode() == 'J')
  {
  	totalTransmitCtrlPkts++;
  	totalTransmitCtrlBytes += geohdr->size();
  }

  if (!ll)
  {
    cerr << "Crap, the link layer is NULL!!!!!" << endl;
  }

//cout << "Geocast:  Done sending." << endl;

  return;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// From here down should really be in a numerical utilities library
// and be made available to all of NS.  For now though, it only has
// file scope and should be OK.
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//                         determinant3x3
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
inline double determinant3x3(double A[3][3])
{
  double det;

  det = A[0][0]*(A[1][1]*A[2][2]-A[2][1]*A[1][2]);
  det -= A[0][1]*(A[1][0]*A[2][2]-A[2][0]*A[1][2]);
  det += A[0][2]*(A[1][0]*A[2][1]-A[2][0]*A[1][1]);
  return det;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//                         leftTurn
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
bool leftTurn(const Pointxy &p1, const Pointxy &p2, const Pointxy &p3)
{
  double  mat[3][3];
  mat[0][0] = p1.getX();
  mat[0][1] = p1.getY();
  mat[0][2] = 1.0;
  mat[1][0] = p2.getX();
  mat[1][1] = p2.getY();
  mat[1][2] = 1.0;
  mat[2][0] = p3.getX();
  mat[2][1] = p3.getY();
  mat[2][2] = 1.0;

  // this means that the cycle p1, p2, p3 is ccw
  // note:  this is >= zero because if they are co-linear then
  // this determinant is 0.  (I hope this is the only case)
  // if they are colinear, then the center lies on the convex
  // hull, not in it really...
  return (determinant3x3(mat) >= 0);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//                         rightTurn
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
bool rightTurn(const Pointxy &p1, const Pointxy &p2, const Pointxy &p3)
{
  return (!leftTurn(p1, p2, p3));
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//                            randomInterior
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Pointxy randomInterior(const vector<Pointxy> &hull)
{
//cout << "Inside randomInterior and the point is:  " << flush;
  vector<Pointxy>::const_iterator m;
  double avgX=0.0, avgY=0.0;
  int count = hull.size();

  // pick an interior point at random
  for (m=hull.begin(); m != hull.end(); m++)
  {
    avgX += m->getX() / (double)count;
    avgY += m->getY() / (double)count;
  }

  Pointxy q(avgX, avgY);

//  if ((feedback[0]=='V') && (feedback[1]=='V'))
//  {
//    cout << "Picked " << q << " as the random interior point" << endl;
//  }

  return q;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//                         inHull
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
bool inHull(const Pointxy &p, const vector<Pointxy> &hull)
{
  // algorithm based on that given on page 43 of "Computational
  // Geometry" by Preparata and Shamos
  // NOTE:  only works for convex hulls
  //  I think the points in the hull must be numbered counterclockwise
  bool insideHull = false;

  // pick an interior point at random
  Pointxy p_i, p_i_plus_1;
  Pointxy q = randomInterior(hull);

  vector<Pointxy>::const_iterator m, next;

  for (m=hull.begin(); m!=hull.end(); m++)
  {
    next = m;
    next++;
    p_i = *m;
    if (next == hull.end())
    {
      p_i_plus_1 = *(hull.begin());
    } else {
      p_i_plus_1 = *next;
    }

    // does the random interior point lie in the wedge?
    if (rightTurn(p, q, p_i_plus_1) && leftTurn(p, q, p_i))
    {
      // is it internal?
      if (leftTurn(p_i, p_i_plus_1, p))
      {
        insideHull = true;
      }
      break;
    }
  }
  return insideHull;
}

//utility function to find the cross point of two line
Pointxy crossPoint(const GamerLine& line1, const GamerLine& line2)
{
 double a1, a2, b1, b2,x=0.0,y=0.0;
 a1 = line1.getA();
 b1 = line1.getB();
 a2 = line2.getA();
 b2 = line2.getB();

 x = (b2-b1)/(a1-a2);
 y = a1 * (b2-b1)/(a1-a2) + b1;

 Pointxy p(x,y);

 return p;
}

//utility function to find the line function across two points
GamerLine getLineTwoP(const Pointxy& p1, const Pointxy& p2)
{
 double a, b, x1, y1, x2, y2 ;
 x1 = p1.getX();
 y1 = p1.getY();
 x2 = p2.getX();
 y2 = p2.getY();

 a = (y1-y2) / (x1-x2);
 b = y1 - x1 * (y1-y2)/(x1-x2);
 GamerLine line(a,b);

 return line;
}

//utility function to find the line function with one point and rate
GamerLine getLineOneP(const Pointxy& p, const double& cut_rate)
{
 double a, b ,x, y;
 a = cut_rate;
 x = p.getX();
 y = p.getY();
 b = y - x * a;
 
 GamerLine line(a, b);

 return line;
}

