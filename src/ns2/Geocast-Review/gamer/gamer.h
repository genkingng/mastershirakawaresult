/******************************************************************************
*   Copyright (C) 2004  Toilers Research Group -- Colorado School of Mines
*
*   Please see COPYRIGHT.TXT and LICENSE.TXT for copyright and license
*   details.
*******************************************************************************/
#ifndef ns_gamer_h
#define ns_gamer_h

#include "object.h"
#include "agent.h"
#include "tclcl.h"
#include "packet.h"
#include "address.h"
#include "ip.h"
#include "mac.h"
#include "mobilenode.h"

#include <set>
#include <map>

using namespace std;

// This means that the jitter ranges from 0.0 to 0.0001 with
// a mean of 0.00005, so the average number of bits that could
// be transmitted without jitter is 50 per transmission.  This seems
// acceptable.
// Note:  this is just the divisor in the random number range calculation.
#define  gamerJitter 10000.0

#define Gamer_min(x,y) (((x) < (y)) ? (x) : (y))
#define Gamer_max(x,y) (((x) > (y)) ? (x) : (y))

//define the timeout source as the forwarding timer 
//choose according to the longeset round trip time for a set of simulation
//now it is (300*600) 51 nodes speed from 1 to 20 m/s
//may change for different simulation setting
//#define GEO_TIMER_FORWARD 1.0 
//#define GEO_TIMER_FORWARD 0.5
//#define GEO_TIMER_FORWARD 0.2
//#define GEO_TIMER_FORWARD 1.5

// prototypes for utilitiy functions
void printHeaders(Packet *p);

// size of the fixed portion of the simulated header
#define GEO_HDR_SIZE_FIXED sizeof(hdr_gamer)

struct hdr_gamer {

  // 'J' = Join demand
  // 'T' = join Table
  // 'D' = Data packet
  // 'U' = unset
  char gamerCode_;

  // 'F' = flood
  // 'B' = box
  // 'D' = corridor
  // 'C' = cone
  // 'U' = unset
  char forwardCode_;

  // fields always needed
  double      sourceX_;
  double      sourceY_;
  double      lowerLeftX_;
  double      lowerLeftY_;
  double      upperRightX_;
  double      upperRightY_;
  double      sendTime_;

  // fields needed by join demands
  bool        repliedTo_;  // indicates that a table has already been
                           // sent for this demand.  Receivers of this
                           // demand should note the ID and not send
                           // tables as a reply.

/*~~~~~~~~~~~~~~~~~~change source routing to local routing~~~~~~~~~~~~~
  // fields needed by join tables
  nsaddr_t  route_[32];  // currently nsaddr_t is a 32 bit int defined in
                         // config.h
  int       hops_;
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
  
  nsaddr_t  demandID_;  // this is the packet ID of the join demand that
                        // prompted this table packet
  double    demandSendTime_;  // used to calculate the total delay

  // fields needed by data packets
  int       dataLength_;   // length of the data in the packet in bytes
                          // does not include the header length

  // additional field for calculate 
  // approx. number of hops from source to geocast region
  int numHops;

  // things I get for "free" from hdr_ip in packet.h
  // int saddr();      // IP addr of source sender
  // int sport();      // port number of the source sender
  // int daddr();      // IP addr of the destination
  // int dport();      // port of the destination

  // things I get for "free" from hdr_cmn in packet.h
  // double   ts_;            // timestamp
  // int      size_;          // simulated packet size
  // int      uid_;           // unique packet id
  // nsaddr_t prev_hop_;      // IP addr of forwarding hop
  // int      num_forwards_;  // number of forwards

  //static int offset_;
  //inline static int& offset() { return offset_; }
  //inline static hdr_gamer* access(Packet *p)
  //{
  //  return (hdr_gamer*) p->access(offset_);
  //}


  // Now I need to provide access functions for my structure members
  inline char     &gamerCode() { return gamerCode_; }
  inline char     &forwardCode() { return forwardCode_; }
  inline double   &sourceX() { return sourceX_; }
  inline double   &sourceY() { return sourceY_; }
  inline double   &lowerLeftX() { return lowerLeftX_; }
  inline double   &lowerLeftY() { return lowerLeftY_; }
  inline double   &upperRightX() { return upperRightX_; }
  inline double   &upperRightY() { return upperRightY_; }
  inline double   &sendTime() { return sendTime_; }
  
/*~~~~~~~~~~~~~~~~~~change source routing to local routing~~~~~~~~~~~~~
  inline int      &hops() { return hops_; }
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

  inline bool     &repliedTo() { return repliedTo_; }
  inline nsaddr_t &demandID() { return demandID_; }
  inline double   &demandSendTime() { return demandSendTime_; }
  inline int      &dataLength() { return dataLength_; }

/*~~~~~~~~~~~~~~~~~~change source routing to local routing~~~~~~~~~~~~~  
  inline int size()
  { 
    return GEO_HDR_SIZE_FIXED + 4 * hops_ + dataLength_;
  }
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
   
  inline int size()
  { 
    return GEO_HDR_SIZE_FIXED + dataLength_;
  }
    
  static int offset_;
  inline static int& offset() {return offset_;}
  inline static hdr_gamer* access(const Packet* p) {
    return (hdr_gamer*) p->access(offset_);
  }
  
};

class GamerAgent;

//timer class defined to set for the forwarding code switch
   class GeoRtxTimer : public TimerHandler {
    public:
      GeoRtxTimer(GamerAgent* a) : TimerHandler() {a_ = a;}
    protected:
      virtual void expire(Event *e);
      GamerAgent *a_;
   };                       
// Agent inheritance is because this is a specialized Agent
class GamerAgent : public Agent
{
  public:
    GamerAgent();
    ~GamerAgent();
    int command(int argc, const char*const* argv);
    void recv(Packet*, Handler*);
    void timeout();

  protected:
    int off_gamer_;
    //Timer for switch forwarding code
    GeoRtxTimer rtx_timer_;
    bool inForwardingRegion(char fwdCode, double X, double Y,
                            double llx, double lly, 
                            double urx, double ury);
    bool dupRcvdPacket(int id);
    bool dupSentPacket(int id);
    bool alreadyRepliedTo(int id);
    bool alreadyForwardedTable(int id);
    //method to repeat sending JOIN_DEMAND while changing forwarding code
    int sendJoinDemand(); 
    void recordSentPacket(int id);
    void recordRcvdPacket(int id);
    bool inGeocastRegion(double llx, double lly, double urx, double ury);
    void sendPacket(Packet *p, nsaddr_t to);
    void broadcastPacket(Packet *p);
    void rebroadcastPacket(Packet *p);
    void set_rtx_timer();
    void updateFwdCode(bool success);

  private:
    typedef struct
    {
      double llx;
      double lly;
      double urx;
      double ury;
      char method;
    } geocastRegionType;
    
/*~~~~~~~~~~~~~~~~~~change source routing to local routing~~~~~~~~~~~~~	*/
typedef struct
   {  
	nsaddr_t  demandID_;
	// this is the packet ID of the join demand that
        // prompted this table packet
   	
	nsaddr_t  lastHop_;
	// this is the hop the agent received JD from
   } localRoute;
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/    


    bool meshMember;  // true if I am in the forwarding zone and
                      // I have received a Join Table recently

    bool sparseMeshMode;  // used to limit the fan out of the mesh
                          // on the last hop

    double timeOfLastJoinTable;

    char current;

    char next;
 

    // this is just a set of packets I've sent
    set<int, less<int> > sentPackets;
    // this is just a set of packets I've received
    set<int, less<int> > rcvdPackets;

    // this set is used to support the sparse mesh mode
    set<int, less<int> > repliedToDemands;

    // this set is used to avoid flooding too many join tables back down the mesh
    set<int, less<int> > tablesForwardedByDemand;

/*~~~~~~~~~~~~~~~~~~change source routing to local routing~~~~~~~~~~~~~
More meaning for this set, each agent store the last hop of the join 
demand it received, then we don't need the hops and route_[] in the
geocast header, when agent receive a join table, just search the set
and get the hop before it in the JD route, unicast the join table to
that hop
    // this is a set of packets I've originated with a join demand
    // when I get back a join table referencing this join demand, I need to
    // output some route results
    set<int, less<int> > originator;
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

/*~~~~~~~~~~~~~~~~~~change source routing to local routing~~~~~~~~~~~~~	*/
map<int, localRoute, less<int> > lastHopOfJD;
/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

    map<int, geocastRegionType, less<int> > geocastRegions;
    map<int, int, less<int> > demandsToRegions;

    NsObject *ll;  // link layer output in order to avoid using a routing
                   // agent
    MobileNode *node;  // the node that this agent is attached to
};

#endif
