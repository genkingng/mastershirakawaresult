Code for Geocast Review. This code is related to the following paper:

P. Yao, E. Krohne, and T. Camp, Performance Comparison of Geocast Routing 
Protocols for a MANET, Proceedings of the 13th IEEE International 
Conference on Computer Communications and Networks (IC3N), pp. 213-220, 2004.

Copyright (C) 2004  Toilers Research Group -- Colorado School of Mines

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions 
are met:
1. Redistributions of source code must retain the above copyright 
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
      This product includes software developed by the Toilers Group at 
      Colorado School of Mines (http://toilers.mines.edu).
4. Neither the name of the University nor of the Group may be used
   to endorse or promote products derived from this software without
   specific prior written permission.

For commercial purposes, for newer versions, for questions 
or comments, or if you received this program from 
someone not in the Toilers research group, please contact:
	Tracy Camp <tcamp@mines.edu>
	Department of Math. and Computer Science
	Colorado School of Mines
	Golden, CO 80401
	U.S.A.
