// -*-	Mode:C++; c-basic-offset:4; tab-width:4; indent-tabs-mode:t -*-
/******************************************************************************
*   Copyright (C) 2004  Toilers Research Group -- Colorado School of Mines
*
*   Please see COPYRIGHT.TXT and LICENSE.TXT for copyright and license
*   details.
*******************************************************************************/
///////////////////////////////////////////////////////////////////////////////////////////////
//   
//   geogrid.h
//
//   Header for the Implementation of the GeoGRID geocast protocol, specified in:
//   W.-H. Liao, Y.-C. Tseng, K.-L. Lo, J.-P. Sheu. GeoGRID: A Geocasting Protocol for Mobile
//   Ad Hoc Networks Based on GRID.  In Journal of Internet Technology, 1(2):23-32, 2000
//
//   Implemented by Ed Krohne June 2003
//

#ifndef ns_geogrid_h
#define ns_geogrid_h

#include "object.h"
#include "agent.h"
#include "tclcl.h"
#include "packet.h"
#include "address.h"
#include "ip.h"
#include "mac.h"
#include "mobilenode.h"
#include "timer-handler.h"
#include "math.h"

#ifdef min            // These are defined by something-or-other.  They're useless to me, and pollute the namespace, preventing
#undef min            //  ranvar.h from being included.
#endif
#ifdef max
#undef max
#endif

#include <ranvar.h>

#include <set>
#include <map>
#include <vector>
#include <list>
#include <iostream>

#include "loc.h"

// size of the fixed portion of the simulated header????????
#define GEOGRID_HDR_SIZE_FIXED sizeof(hdr_geogrid)

struct grid_t { 
	grid_t ()               : x(0),  y(0)  {};
	grid_t (int _x, int _y) : x(_x), y(_y) {};
	int x;
	int y;
};
inline bool operator == (grid_t lhs, grid_t rhs) { return ( lhs.x == rhs.x && lhs.y == rhs.y ); };
inline bool operator != (grid_t lhs, grid_t rhs) { return ( lhs.x != rhs.x || lhs.y != rhs.y ); };
inline std::ostream& operator << (std::ostream& out, grid_t grid) { return out << grid.x << ", " << grid.y; };

inline grid_t operator + (grid_t lhs, grid_t rhs) { return grid_t(lhs.x + rhs.x, lhs.y + rhs.y); };

class GeoGRIDAgent;
class GeoGRIDAnnounceTimer;
class GeoGRIDExpireTimer;
class GeoGRIDElectTimer;
class GeoGRIDLeaveGridTimer;
class GeoGRIDRegion;

//////////////////////////////////////////////////////////////////////////////////////////////
//
//  GeoGRID Timers
//
//  GeoGRIDAnnounceTimer   - Timer for gateways to transmit GATE messages
//  GeoGRIDExpireTimer     - Delay before non-gateways start bidding
//  GeoGRIDElectTimer      - Delay before a contestant gateway is elected.
//  GeoGRIDLeaveGridTimer  - A node constantly checks to see if it has left is grid
//
//  Timers used to inform GeoGRID agents when they are supposed to send BID and GATE messages
//   etc.
//

class GeoGRIDAnnounceTimer : public TimerHandler {
public:
	GeoGRIDAnnounceTimer(GeoGRIDAgent *_agent) : TimerHandler() { agent = _agent; };
	virtual void expire(Event* event);
protected:
	GeoGRIDAgent *agent;
};

class GeoGRIDExpireTimer : public TimerHandler {
public:
	GeoGRIDExpireTimer(GeoGRIDAgent *_agent) : TimerHandler() { agent = _agent; };
	virtual void expire(Event* event);
protected:
	GeoGRIDAgent *agent;
};

class GeoGRIDElectTimer : public TimerHandler {
public:
	GeoGRIDElectTimer(GeoGRIDAgent *_agent) : TimerHandler() { agent = _agent; };
	virtual void expire(Event* event);
protected:
	GeoGRIDAgent *agent;
};


class GeoGRIDLeaveGridTimer : public TimerHandler {
public:
	GeoGRIDLeaveGridTimer(GeoGRIDAgent *_agent) : TimerHandler() { agent = _agent; };
	virtual void expire(Event* event);
protected:
	GeoGRIDAgent *agent;
};

//////////////////////////////////////////////////////////////////////////////////////////////
//
//  GeoGRIDRegion
//
//  Class implementing regions, which are used to represent any geocast-ready region.
//  The following implementation replaces original code which stores individual x and y
//   coordinates of a bounding box.  This is to facilitate future addition of different
//   region types: bars, fans, etc.
//

class GeoGRIDRegion {
public:
	enum regionType {
		GEOGRID_BOX
	};

	// Note on constructors.  Number of arguments alone can easily decide which forwarding type to use.  However,
	//  this is a flaky way of doing things.  If arguments change, or there are more than one region type with the
	//  same number of arguments, it would wreak havoc.  Therefore, both type and arg number below are required to match.
	GeoGRIDRegion(GeoGRIDRegion::regionType type, double xMinExt, double yMinExt, double xMaxExt, double yMaxExt); // BOX region constructor

	// Returns true if the region contains the given grid.
	bool contains(loc_t loc);
	
	// Causes this object to become the smallest region that inclues both its former self and the given grid
	// Useful for calculating forwarding regions.
	void growToInclude(loc_t loc);

	// Causes this object to expand so that it contains its former self and is aligend to grid edges.
	void growToGrids(); 

	friend GeoGRIDRegion intersect ( const GeoGRIDRegion& region1, const GeoGRIDRegion& region2 );

	// Coordinate accessors
	double getX1() const { return lowerLeft.x; };
	double getY1() const { return lowerLeft.y; };
	double getX2() const { return upperRight.x; };
	double getY2() const { return upperRight.y; };

	// Center -- for ticket distribution
	loc_t getCenterLoc() const;

	void format(char * buffer, char * prefixString);    // NOTE: This function is defined in cmu-trace.cc and NOT in geogrid.cc
private:
	loc_t lowerLeft;
	loc_t upperRight;
};

inline std::ostream& operator << (std::ostream& out, const GeoGRIDRegion& rgn) { return out << rgn.getX1() << ", " << rgn.getY1() << ", " << rgn.getX2() << ", " << rgn.getY2(); };

//////////////////////////////////////////////////////////////////////////////////////////////
//
//  hdr_geogrid
//
//  Structure simulating the GeoGRID header attached to any packet sent using the GeoGRID
//   geocasting protocol.  Contains all geogrid-relevant information to make forwarding possible.
//

struct hdr_geogrid {
	// Identification of packet type.  See paper for definitions of the various packets.
	//  UNKNOWN is self-explanatory.
	enum packetType {
		GEOGRID_DATA,
		GEOGRID_BID,
		GEOGRID_GATE,
		GEOGRID_RETIRE,
		GEOGRID_UNKNOWN
	} type;

	// Whether to use flood-based forwarding or ticket-based forwarding
	enum methodType {
		GEOGRID_FLOOD,
		GEOGRID_TICKET
	} forwardMethod;
  
	//loc_t  lastHopLoc;    // The location of the last hop node
	//grid_t lastHopGrid;   // The grid of the last hop node (totally redundant given loc, but called for in the paper so I won't argue)
	loc_t  sourceLoc;       // The location of the sending node 
	grid_t sourceGrid;      // The grid of the sending node (totally redundant given loc, but called for in the paper so I won't argue)
	double sendTime_;

	GeoGRIDRegion destRegion;
	// GeoGRIDRegion forwardRegion;

	// Fields needed by ticket-based packets
	grid_t  ticketGrids[3];
	int     ticketNums[3];

	// fields needed by data packets
	int       dataLength;   // length of the data in the packet in bytes
	// does not include the header length 
													   
	// Retrieval of the header from the packet.
	static int offset_;									
	inline int size() { 	
		int size = sizeof(hdr_geogrid) - sizeof(sendTime_);
		if ( type == GEOGRID_DATA ) {
			// Data packets only use the source grid for forwarding (on-demand caclulation of forwarding region)
			size += dataLength - sizeof(sourceLoc); 
			if ( forwardMethod == GEOGRID_FLOOD ) {
				size -= sizeof(ticketGrids) + sizeof(ticketNums);
			}
		} else {
			// Note: sourceGrid is redundant with sourceLoc in anything but a retire packet.
			// Yes, I DO have both in the header, but this is just for simplicity.  In a
			// true implementation, the packets would be optimized and sourceGrid would be
            // calculated on demand every time.  Simply storing it in the packet and not
            // counting it as part of the size accomplishes the same thing for simulation purposes.
			if ( type == GEOGRID_RETIRE ) {
				size -= sizeof(sourceLoc);  // sourceLoc is not used in a RETIRE packet but is in a BID or GATE packet
			} else {
				size -= sizeof(sourceGrid); // sourceGrid is not used in a BID or GATE packet but is used in a RETIRE packet
			}
			size -= sizeof(ticketGrids) + sizeof(ticketNums) + sizeof(forwardMethod) + sizeof(destRegion);
		}
		return size;
	}													
	inline static int& offset() {return offset_;}		
	inline static hdr_geogrid* access(const Packet* p) 	
	{													
		return (hdr_geogrid*) p->access(offset_);
	}

	// things I get for "free" from hdr_ip in packet.h
	// int saddr();      // IP addr of source sender
	// int sport();      // port number of the source sender
	// int daddr();      // IP addr of the destination
	// int dport();      // port of the destination

	// things I get for "free" from hdr_cmn in packet.h
	// double   ts_;            // timestamp
	// int      size_;          // simulated packet size
	// int      uid_;           // unique packet id
	// nsaddr_t prev_hop_;      // IP addr of forwarding hop
	// int      num_forwards_;  // number of forwards

};

//////////////////////////////////////////////////////////////////////////////////////////////
//
//  GeoGRIDAgent
//
//  Class for an Agent (an object that exists as part of a node and which the node uses to
//   utilize a protocol) which implements the GeoGRID geocasting protocol.
//
class GeoGRIDAgent : public Agent
{
public:
	GeoGRIDAgent();
	~GeoGRIDAgent();
	int command(int argc, const char*const* argv);
	void recv(Packet*, Handler*);
    
	void gatewayAnnounceTimeout(); // Periodic timer used by the gateway to determine when to announce its continued presence.  
	void gatewayExpireTimeout();   // Timer used by non-gateways to determine when the gateway should be considered dead due to prolonged silence.
	void gatewayElectTimeout();    // Timer used by gateway condidates to determine when they should silently make themselves the gateway.
	void checkLeaveGridTimeout();  // Timer used to periodically check to make sure the node hasn't left it's grid.


	// Mathematical and helper functions
	bool isCloserToCenter(loc_t argLoc);                   // Returns true if the argLoc is closer
	static bool isCloserToCenter(loc_t loc1, loc_t loc2);  // Returns true if loc1 is closer than loc2
	static loc_t getTopRightLoc(loc_t argLoc);
	static loc_t getTopRightLoc(grid_t argLoc);
	static loc_t getBottomLeftLoc(loc_t argLoc);
	static loc_t getCenterLoc(loc_t argLoc);
	static loc_t getCenterLoc(grid_t argGrid);
	loc_t  getLoc();
	grid_t getGrid();

	static grid_t getGrid(loc_t argLoc);

private:
	class neighborGrids_t {
	public:
		neighborGrids_t();
		void move(grid_t newCenter);
		void setGateway(grid_t grid, nsaddr_t gateAddr);
		nsaddr_t getGateway(grid_t grid);
	private:
		grid_t translateGridIndex(grid_t grid);
		grid_t translateGridIndex(grid_t grid, grid_t refGrid);
		nsaddr_t neighborGateways[3][3];
		grid_t center;
	} neighborGrids;

	// I want to only send bid packets once per election.
	// Elections end when a gate packet is received, grids are changed, and the election timer times out.
	// Elections start when the node enters an unknown grid, at the beginning of the simulation, when
	//  the gateway expire timer times out, and when a retire packet is received

	class electionHandler_t {
	public:
		electionHandler_t();
		nsaddr_t endElection();
		void startElection(loc_t thisLoc, nsaddr_t thisAddr);
		void recvBIDpkt(loc_t senderLoc, nsaddr_t senderAddr);
		bool isBIDsendOk();
		void sendBIDpkt();
		int *getOptimizePtr();
		bool isBidding();
	private:
		int optimizeBidFlag;  // Whether or not optimized bidding or by-the-paper bidding is used.
		bool biddingFlag;      // Whether or not this node is bidding
		bool hasSentBIDpkt;    // Used to prevent more than one BID packet being sent any one election
		loc_t bestLoc;         // Used to return the id of the best gateway at the end of the election
		nsaddr_t bestAddr;     // Used to return the id of the best gateway at the end of the election
	} electionHandler;

	// Print out all the headers of the packet if the verbosity is high enough.
	void printHeaders(Packet* p);

	// Used to jitter transmit times to prevent collisions
	double jitterFunc();

	// MPN: Maximum Packet Number -- a generic array size boundary
	enum {MPN = 4000};
  
	int off_geogrid_;

	// The set of packets this object has sent
	std::set<int, std::less<int> > sentPackets;
	// The set of packets this object has received
	std::set<int, std::less<int> > rcvdPackets;

	// Functions used to make sure no packet is resent twice.
	bool dupRcvdPacket(int id);
	bool dupSentPacket(int id);
	void recordSentPacket(int id);
	void recordRcvdPacket(int id);
	void broadcastPacket(Packet *p);
	void rebroadcastPacket(Packet *p);

	// Timers, used to implement GeoGRID's hello packets getting sent at timeouts
	void setGatewayAnnounceTimer();
	void setGatewayExpireTimer();      
	void setGatewayElectTimer();
	void setCheckLeaveGridTimer();

	void killGatewayAnnounceTimer();
	void killGatewayExpireTimer();      
	void killGatewayElectTimer();
	void killCheckLeaveGridTimer();

	GeoGRIDAnnounceTimer   gatewayAnnounceTimer;
	GeoGRIDElectTimer      gatewayElectTimer;
	GeoGRIDExpireTimer     gatewayExpireTimer;
	GeoGRIDLeaveGridTimer  checkLeaveGridTimer;

	UniformRandomVariable  ranVar;        // For random numbers


	// Packet thru-put
	void sendGATEpkt();
	void sendBIDpkt();
	void sendRETIREpkt();
	void sendDATApkt(const GeoGRIDRegion& destRegion,
					 int dataLength,
					 hdr_geogrid::methodType forwardMethod
					 );

	void recvGATEpkt  (Packet *pkt);
	void recvBIDpkt   (Packet *pkt);
	void recvRETIREpkt(Packet *pkt);

	void recvDATApkt        (Packet *pkt);
	void recvDATA_FLOODpkt  (Packet *pkt);
	void recvDATA_TICKETpkt (Packet *pkt);
	void recvDATApktSuccess (Packet *pkt);

	void doTicketMethodStuff(hdr_geogrid *geogridhdr, int availableTickets);

	void setGateway(nsaddr_t _gateway);
	bool isGateway();
	bool gatewayUnknown();

	// Constants
	const static int GATEWAY_UNKNOWN = -1;
	const static grid_t NO_GRID;

	bool isVerboseEnough();     // Returns true if whatever relevant conditions there are dictate that an output should be made.

	// Parameters (bound to tcl variables)
	int verbosity;              // Used to determine exactly how much miscellaneous information this agent writes to stdout.
	int verboseNode;            // To filter debug output for sanity reasons, only examine one node for verbose output
	static double gridSize;     // The length on a side of one grid
	double gatewayAnnounceTime; // Time interval between GATE gateway announcement packets
	double gatewayElectTime;    // Time after unchallenged bid before a node considers itself the gateway.
	double gatewayExpireTime;   // Maximum period between GATE announcement packets before the gateway is considered dead.
	double checkLeaveGridTime;  // Interval between which the agent checks to see if it has moved.
	double timeJitter;          // Jitter on time of transmits to prevent collisions

	// Environment, associated objects, etc.
	NsObject   *ll;            // link layer output in order to avoid using a routing agent
	MobileNode *node;          // the node that this agent is attached to
	nsaddr_t    gateway;       // The address of the gateway for this grid
	loc_t       gatewayLoc;    // The last known location of the gateway
	grid_t      lastGrid;      // Last known grid that the node was in.  Used solely for comparison purposes.
  
	// Statics for statistics
	static long        dataPktsOrig;    // the number of new, originated packets
	static long long   controlPkts;	    // the number of control packets transmitted
	static long long   controlBytes;    // total byte overhead of control packets
	static long long   dataPkts;	    // the number of data packets transmitted
	static long long   dataBytes;       // total byte overhead of data packets
	static long long   oneSuccess;      // total number of times a packet reaches at least one of the nodes in the destination.
	static double      totalDelay;      // total delay for all packets.
	static long long   rcvdDataPkts;    // total number of packets received in teh destination region.
	static long long   totalPktHops;    // Total number of hops for each of the one successes.
	static std::vector<int> successID;
    
	// static used for reporting the number of nodes in the multicast region 
	static int reportNodes;
   
	// Statistics reporting
	// int geogridPkts; // the number of geogrid packets sent in the simulation
	// int totalTransmitPkts; // the total number of packets transmitted in the network by adding one for each hop in the network a packet takes
	// int deliveredPktID[30000]; // deliveredPktID[i] indicate how many nodes in the multicast region receive the packet with packet ID i
};

#endif
