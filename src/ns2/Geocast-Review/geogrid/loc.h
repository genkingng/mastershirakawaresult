///////////////////////////////////////////////////////////////////////////////////////////////
//   
//  loc.h
//
//  Header defining a single location struct for GeoED and GeoGRID.  
//  Placed in its own file so that it can be shared common.
//
//   Implemented by Ed Krohne July 2004
//
#include <iostream>
#ifndef ns_loc_h
#define ns_loc_h
struct loc_t {
	loc_t ()                     : x(0),  y(0)  {};
	loc_t (double _x, double _y) : x(_x), y(_y) {};
	double x;
	double y;
};
	
inline double dist(loc_t loc1, loc_t loc2)
{
	double dist = (loc2.x-loc1.x)*(loc2.x-loc1.x) + (loc2.y-loc1.y)*(loc2.y-loc1.y);
	dist = sqrt(dist);
	return dist;
};
inline bool operator == (loc_t lhs, loc_t rhs) { return ( lhs.x == rhs.x && lhs.y == rhs.y ); };
inline bool operator != (loc_t lhs, loc_t rhs) { return ( lhs.x != rhs.x || lhs.y != rhs.y ); };
inline std::ostream& operator << (std::ostream& out, loc_t loc) { return out << loc.x << ", " << loc.y; };
inline loc_t operator + (loc_t lhs, loc_t rhs) { return loc_t(lhs.x + rhs.x, lhs.y + rhs.y); };
inline loc_t operator - (loc_t lhs, loc_t rhs) { return loc_t(lhs.x - rhs.x, lhs.y - rhs.y); };
inline loc_t operator * (loc_t lhs, double rhs) { return loc_t(lhs.x * rhs, lhs.y * rhs); };
inline loc_t operator / (loc_t lhs, double rhs) { return loc_t(lhs.x / rhs, lhs.y / rhs); };
inline loc_t operator -=(loc_t& lhs, loc_t rhs) { return lhs = lhs - rhs; };
inline loc_t operator +=(loc_t& lhs, loc_t rhs) { return lhs = lhs + rhs; };
#endif
