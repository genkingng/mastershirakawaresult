/* -*-	Mode:C++; c-basic-offset:4; tab-width:4; indent-tabs-mode:t -*- */
/******************************************************************************
*   Copyright (C) 2004  Toilers Research Group -- Colorado School of Mines
*
*   Please see COPYRIGHT.TXT and LICENSE.TXT for copyright and license
*   details.
*******************************************************************************/
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <ctype.h>
#include <cmath>
#include "geogrid.h"
using namespace std;

///////////////////////////////////////////////////////////////////////////
//
//  Declaration of static variables from GeoGRIDAgent, others
//

int hdr_geogrid::offset_;
const grid_t GeoGRIDAgent::NO_GRID(-1,-1);

const int VECTOR_INCREMENT = 1000;      // The number that successID and numNodes are incremented by each time they overflow

static grid_t directionGrids[8];        // Used for deciding where to distribute how many tickets.


// variables for statistics
long        GeoGRIDAgent::dataPktsOrig    = 0;   // the number of new, originated packets
long long   GeoGRIDAgent::controlPkts     = 0;   // the number of control packets transmitted
long long   GeoGRIDAgent::controlBytes    = 0;   // total byte overhead of control packets
long long   GeoGRIDAgent::dataPkts        = 0;   // the number of data packets transmitted
long long   GeoGRIDAgent::dataBytes       = 0;   // total byte overhead of data packets
long long   GeoGRIDAgent::oneSuccess      = 0;   // total number of times a packet reaches at least one of the nodes in the destination.
double      GeoGRIDAgent::totalDelay      = 0;   // total delay for all packets.
long long   GeoGRIDAgent::rcvdDataPkts    = 0;   // total number of packets received in the destination region.
long long   GeoGRIDAgent::totalPktHops    = 0;   // total number of hops for each of the one successes.
vector<int> GeoGRIDAgent::successID(VECTOR_INCREMENT, 0);

// used for reporting the number of nodes in the multicast region 
int GeoGRIDAgent::reportNodes = 0;

// if true, this will add jitter to multicast packets being sent
const bool GeoGRIDuseJitteronBroadcast = true;

// Static option
double GeoGRIDAgent::gridSize = 0; // The length on a side of one grid


///////////////////////////////////////////////////////////////////////////////////////
//
//  Actually create otcl classes, bind our c++ classes (header, agent) to those otcl classes
//
//   pretty much standard boilerplate, you'll see this in just about any agent cc file.
//
static class GeoGRIDHeaderClass: public PacketHeaderClass {
public:
	GeoGRIDHeaderClass():  PacketHeaderClass("PacketHeader/GeoGRID",
											 sizeof(hdr_geogrid))
	{
		bind_offset(&hdr_geogrid::offset_);
	}
} class_geogridhdr;

static class GeoGRIDClass: public TclClass {
public:
	GeoGRIDClass() : TclClass("Agent/GeoGRID") {
		directionGrids[0] = grid_t(-1, -1);
		directionGrids[1] = grid_t(-1,  0);
		directionGrids[2] = grid_t(-1,  1);
		directionGrids[3] = grid_t( 0, -1);
		directionGrids[4] = grid_t( 0,  1);
		directionGrids[5] = grid_t( 1, -1);
		directionGrids[6] = grid_t( 1,  0);
		directionGrids[7] = grid_t( 1,  1);
	}
	~GeoGRIDClass() {
	}
	TclObject* create(int, const char*const*)
	{
		return (new GeoGRIDAgent());
	}
} class_geogrid;

///////////////////////////////////////////////////////////////////////////////////////
//
//  Constructor/Destructor, GeoGRIDAgent
//
GeoGRIDAgent::GeoGRIDAgent() : 
	Agent(PT_GEOGRID),
	gatewayAnnounceTimer(this),
	gatewayElectTimer(this),
	gatewayExpireTimer(this),
	checkLeaveGridTimer(this),
	ranVar(0, 0)
{
	
	// Make sure instantiate all objects
	ll = NULL;
	node = NULL;

	setGateway(GATEWAY_UNKNOWN);
		
	// Bind C++ variables to tcl variables.  Permits lines in tcl such as "Agent/GeoGRID set gridSize_ 10" or whatever.
	bind("gridSize_", &gridSize);
	bind("verbosity_", &verbosity);
	bind("verboseNode_", &verboseNode);
	bind_time("gatewayAnnounceTime_", &gatewayAnnounceTime);
	bind_time("gatewayElectTime_", &gatewayElectTime);
	bind_time("gatewayExpireTime_", &gatewayExpireTime);
	bind_time("checkLeaveGridTime_", &checkLeaveGridTime);
	bind_time("timeJitter_", ranVar.maxp());  // Bind it directly to the random variable's max.
	bind("optimizeBidFlag_", electionHandler.getOptimizePtr()); // Bind directly to the subobject's internal variable
			
	return;
}

GeoGRIDAgent::~GeoGRIDAgent() {
	rcvdPackets.erase(rcvdPackets.begin(), rcvdPackets.end());
	sentPackets.erase(sentPackets.begin(), sentPackets.end());
	return;
}

//////////////////////////////////////////////////////////////////////////
//
//  misc
//
bool GeoGRIDAgent::isGateway() {
	if ( node ) {
		return addr() == gateway;
	} else {
		cerr << "Error: isGateway called at C-level before node has been initialized." << endl;
		return 0;
	}
}
bool GeoGRIDAgent::gatewayUnknown() {
	if ( node ) {
		return gateway == GATEWAY_UNKNOWN;
	} else {
		cerr << "Error: isGateway called at C-level before node has been initialized." << endl;
		return 0;
	}
}

// Note: look at this as if it had infix notation.  "loc1 isCloserToCenter than loc2", not the other way around.
bool GeoGRIDAgent::isCloserToCenter(loc_t argLoc) {
	if ( node ) {
		return dist(getLoc(), getCenterLoc(getLoc())) < dist(argLoc, getCenterLoc(argLoc));
	} else {
		cerr << "Error: isCloserToCenter called at C-level before node has been initialized." << endl;
		return 0;
	}
}

// Note: look at this as if it had infix notation.  "loc1 isCloserToCenter than loc2", not the other way around.
bool GeoGRIDAgent::isCloserToCenter(loc_t loc1, loc_t loc2) {
	return dist(loc1, getCenterLoc(loc1)) < dist(loc2, getCenterLoc(loc2));
}


loc_t GeoGRIDAgent::getLoc() {
	if ( node ) {
		loc_t retLoc(node->X(), node->Y());
// 		if ( retLoc.y > 500 ) {
// 			cout << "retLoc is outside rectangle. " << retLoc << endl;
// 			cout << "getGrid returns" << getGrid() << endl;
// 			cout << "Gridsize is " << gridSize << endl;
// 		}
		return retLoc;
	} else {
		cerr << "Node not set when getLoc called. Returning 0, 0 location.  This would"
			 << "have been a segmentation fault." << endl;
	}
	return loc_t(0, 0);
}

grid_t GeoGRIDAgent::getGrid() {
	if ( node ) {
		grid_t retGrid(int(node->X()/gridSize), int(node->Y()/gridSize));
		return retGrid;
	} else {
		cerr << "Node not set when getLoc called. Returning 0, 0 location.  This would"
			 << "have been a segmentation fault." << endl;
		return grid_t(0, 0);
	}
}

grid_t GeoGRIDAgent::getGrid(loc_t argLoc) {
	grid_t retGrid(int(argLoc.x/gridSize), int(argLoc.y/gridSize));
	return retGrid;
}


loc_t GeoGRIDAgent::getCenterLoc(loc_t referenceLoc) {
	return loc_t(
				 int(referenceLoc.x/gridSize) * gridSize + gridSize/2,
				 int(referenceLoc.y/gridSize) * gridSize + gridSize/2
				 );
}

loc_t GeoGRIDAgent::getCenterLoc(grid_t referenceGrid) {
	return loc_t(
				 referenceGrid.x * gridSize + gridSize/2,
				 referenceGrid.y * gridSize + gridSize/2
				 );
}

loc_t GeoGRIDAgent::getTopRightLoc(loc_t referenceLoc) {
	return loc_t(
				 int(referenceLoc.x/gridSize) * gridSize,
				 int(referenceLoc.y/gridSize) * gridSize
				 );
}


loc_t GeoGRIDAgent::getTopRightLoc(grid_t referenceGrid) {
	return loc_t(
				 referenceGrid.x * gridSize,
				 referenceGrid.y * gridSize
				 );
}

loc_t GeoGRIDAgent::getBottomLeftLoc(loc_t referenceLoc) {
	return loc_t(
				 int(referenceLoc.x/gridSize) * gridSize + gridSize,
				 int(referenceLoc.y/gridSize) * gridSize + gridSize
				 );
}


void GeoGRIDAgent::setGateway(nsaddr_t _gateway) {
	gateway = _gateway;
}

bool GeoGRIDAgent::isVerboseEnough() {
	return 0;
}

//////////////////////////////////////////////////////////////////////////
//
//  GeoGRIDAgent::neighborGrids_t -- member class for the neighborGrids object
//
//  Used to keep track of the gateway for each neighboring grid.
//
GeoGRIDAgent::neighborGrids_t::neighborGrids_t() : 
	center(0, 0)
{
	grid_t grid;       // in grid coordinates, each successive neighbor of newCenter.

	// Now copy the new array back onto the old
	for ( grid.x = 0; grid.x < 3; grid.x++ ) {
		for ( grid.y = 0; grid.y < 3; grid.y++ ) {
			neighborGateways[grid.x][grid.y] = GATEWAY_UNKNOWN;
		}
	}
}
	
void GeoGRIDAgent::neighborGrids_t::move(grid_t newCenter) {
	// For purposes of copying and translation, make a new array and fill it first.
	nsaddr_t newNeighborGateways[3][3];
	grid_t destGrid;       // in grid coordinates, each successive neighbor of newCenter.
	for ( destGrid.x = newCenter.x-1; destGrid.x <= newCenter.x+1 ; destGrid.x++ ) {
		for ( destGrid.y = newCenter.y-1; destGrid.y <= newCenter.y+1; destGrid.y++ ) {
			grid_t destIndex = translateGridIndex(destGrid, newCenter);          // In array coordinates, the destination index
			grid_t sourceIndex = translateGridIndex(destGrid, center);           // In array coordinates, the source index
			if ( sourceIndex.x < 0 || sourceIndex.y < 0 || sourceIndex.x >= 3 || sourceIndex.y >= 3 ) {
				newNeighborGateways[destIndex.x][destIndex.y] = GATEWAY_UNKNOWN;   // source array does not contain information about this grid, so unknown.
				continue;                                                         
			}
			newNeighborGateways[destIndex.x][destIndex.y] = neighborGateways[sourceIndex.x][sourceIndex.y];
		}
	}

	grid_t destIndex;

	// Now copy the new array back onto the old
	for ( destIndex.x = 0; destIndex.x < 3; destIndex.x++ ) {
		for ( destIndex.y = 0; destIndex.y < 3; destIndex.y++ ) {
			neighborGateways[destIndex.x][destIndex.y] = newNeighborGateways[destIndex.x][destIndex.y];
		}
	}

	// Finally reset the center
	center = newCenter;
}
void GeoGRIDAgent::neighborGrids_t::setGateway(grid_t grid, nsaddr_t gateAddr) {
	grid_t gridIndex = translateGridIndex(grid);
	if ( gridIndex.x < 0 || gridIndex.y < 0 || gridIndex.x >= 3 || gridIndex.y >= 3 ) {
		return;     // Ignore.  We just don't keep track of non-neighbors.
	}
  	neighborGateways[gridIndex.x][gridIndex.y] = gateAddr;
}
nsaddr_t GeoGRIDAgent::neighborGrids_t::getGateway(grid_t grid) {
	grid_t gridIndex = translateGridIndex(grid);
	if ( gridIndex.x < 0 || gridIndex.y < 0 || gridIndex.x >= 3 || gridIndex.y >= 3 ) {
		return GATEWAY_UNKNOWN;
	}

	return neighborGateways[gridIndex.x][gridIndex.y];	
}

grid_t GeoGRIDAgent::neighborGrids_t::translateGridIndex(grid_t grid) { return translateGridIndex(grid, center); }
grid_t GeoGRIDAgent::neighborGrids_t::translateGridIndex(grid_t grid, grid_t refGrid) {
	grid_t retGrid(grid.x - refGrid.x + 1, grid.y - refGrid.y + 1);
	return retGrid;
}


//////////////////////////////////////////////////////////////////////////
//
//  GeoGRIDAgent::electionHandler_t -- member class for the neighborGrids object
//
//  Used to keep track of the gateway for each neighboring grid.
//
GeoGRIDAgent::electionHandler_t::electionHandler_t() {
	optimizeBidFlag = 0;
	biddingFlag = 0;
	hasSentBIDpkt = 0;
}

nsaddr_t GeoGRIDAgent::electionHandler_t::endElection() {
	biddingFlag = 0;
	hasSentBIDpkt = 0;
	return bestAddr;
}
	
void GeoGRIDAgent::electionHandler_t::startElection(loc_t thisLoc, nsaddr_t thisAddr) {
	biddingFlag = 1;
	// hasSentBIDpkt = 0       // Do not use this in case I accidentally send a BID packet BEFORE I start the election.
	bestLoc = thisLoc;
	bestAddr = thisAddr;
}
void GeoGRIDAgent::electionHandler_t::recvBIDpkt(loc_t senderLoc, nsaddr_t senderAddr) {
	if ( isCloserToCenter(senderLoc, bestLoc) ) {
		bestLoc = senderLoc;
		bestAddr = senderAddr;
	}
}

bool GeoGRIDAgent::electionHandler_t::isBIDsendOk() {
	return !optimizeBidFlag || !hasSentBIDpkt;
}

void GeoGRIDAgent::electionHandler_t::sendBIDpkt() {
	hasSentBIDpkt = 1;
}

int *GeoGRIDAgent::electionHandler_t::getOptimizePtr() {
	return &optimizeBidFlag;
}

bool GeoGRIDAgent::electionHandler_t::isBidding() {
	return biddingFlag;
}

//////////////////////////////////////////////////////////////////////////
//
//  jitterFunc
//
//  using whatever random-number generation is available, and the jitter
//  parameter, produces a positive or negative value to be added to various
//  time intervals to prevent collisions.
//
double GeoGRIDAgent::jitterFunc() {
	return ranVar.value();
}

//////////////////////////////////////////////////////////////////////////
//
//  setTimers/killtimers
//
//  Set and kill different kinds of timers for the purpose of gateway election.
//

// Periodic timer used by the gateway to determine when to announce its continued presence.
void GeoGRIDAgent::setGatewayAnnounceTimer() {
	gatewayAnnounceTimer.resched(gatewayAnnounceTime);
}
// Timer used by non-gateways to determine when the gateway should be considered dead due to prolonged silence.
void GeoGRIDAgent::setGatewayExpireTimer() {
	gatewayExpireTimer.resched(gatewayExpireTime);
}
// Timer used by gateway condidates to determine when they should silently make themselves the gateway.
void GeoGRIDAgent::setGatewayElectTimer() {
	gatewayElectTimer.resched(gatewayElectTime);
	//	cout << "Beginning gateway election timer." << endl;
	//  cout << (gatewayElectTimer.status() == TIMER_PENDING) << endl;
}
// Timer used by gateway condidates to determine when they should silently make themselves the gateway.
void GeoGRIDAgent::setCheckLeaveGridTimer() {
	checkLeaveGridTimer.resched(checkLeaveGridTime);
}


// Periodic timer used by the gateway to determine when to announce its continued presence.
void GeoGRIDAgent::killGatewayAnnounceTimer() {
	if ( gatewayAnnounceTimer.status() == TIMER_PENDING ) {
		gatewayAnnounceTimer.cancel();
	}
}
// Timer used by non-gateways to determine when the gateway should be considered dead due to prolonged silence.
void GeoGRIDAgent::killGatewayExpireTimer() {
	if ( gatewayExpireTimer.status() == TIMER_PENDING ) {
		gatewayExpireTimer.cancel();
	}
}
// Timer used by gateway condidates to determine when they should silently make themselves the gateway.
void GeoGRIDAgent::killGatewayElectTimer() {
	if ( gatewayElectTimer.status() == TIMER_PENDING ) {
		gatewayElectTimer.cancel();
	}
}
/// Timer used by gateway condidates to determine when they should silently make themselves the gateway.
void GeoGRIDAgent::killCheckLeaveGridTimer() {
	if ( checkLeaveGridTimer.status() == TIMER_PENDING ) {
		checkLeaveGridTimer.cancel();
	}
}

///////////////////////////////////////////////////////////////////////////////////////
//
//  expire
//   The point of the timer classes, the function that is called when the timers expire
//
void GeoGRIDAnnounceTimer::expire(Event* event) {
	agent->gatewayAnnounceTimeout();
}
void GeoGRIDElectTimer::expire(Event* event) {
	agent->gatewayElectTimeout();
}
void GeoGRIDExpireTimer::expire(Event* event) {
	agent->gatewayExpireTimeout();
}
void GeoGRIDLeaveGridTimer::expire(Event* event) {
	agent->checkLeaveGridTimeout();
}

//////////////////////////////////////////////////////////////////////////
//
//  timeouts
//
//  Respond to different kinds of timers for the purpose of gateway election.
//

// Periodic timer used by the gateway to determine when to announce its continued presence.
void GeoGRIDAgent::gatewayAnnounceTimeout() {
	if ( isVerboseEnough() ) {
		cout << addr() << " at " << Scheduler::instance().clock() << ": I am the gateway.  It is time for me to announce this." << endl;
	}

	node->update_position();
	sendGATEpkt();
	setGatewayAnnounceTimer();  // Retransmit again, and again, and again...
}
// Timer used by non-gateways to determine when the gateway should be considered dead due to prolonged silence.
void GeoGRIDAgent::gatewayExpireTimeout() {
	if ( isVerboseEnough() ) {
		cout << addr() << " at " << Scheduler::instance().clock() << ": The gateway has been too silent, and is now considered dead.  I bid." << endl;
	}

	node->update_position();
	electionHandler.startElection(getLoc(), addr());
	sendBIDpkt();
	setGatewayElectTimer();   // Wait to see if the bid is challenged.
}
// Timer used by gateway condidates to determine when they should silently make themselves the gateway.
void GeoGRIDAgent::gatewayElectTimeout() {
	//	cout << addr() << " at " << Scheduler::instance().clock() << ": Election ended." << endl;

	node->update_position();
	setGateway(electionHandler.endElection());
	if ( isGateway() ) {
		setGatewayAnnounceTimer();
		killGatewayExpireTimer();
	} else {
		setGatewayExpireTimer();
	}

	//	cout << "        My gateway, ( grid " << getGrid() << "), is " << gateway << endl;
}

// Timer used to periodically check to make sure the node hasn't left it's grid.
void GeoGRIDAgent::checkLeaveGridTimeout() {
	if ( getGrid() != lastGrid ) {
		// Debug output
		if ( isVerboseEnough() ) {
			cout << addr() << " at " << Scheduler::instance().clock() << ": changing grids from " << lastGrid << " to " << getGrid() << endl;
		}

		// Make sure to send a retire packet if we're resigning as gateway due to leaving grid.
		if ( isGateway() ) {
			sendRETIREpkt();
			killGatewayAnnounceTimer();
		}

		// Update variables
		neighborGrids.move(getGrid());
		setGateway(neighborGrids.getGateway(getGrid()));
		killGatewayElectTimer();
		killGatewayExpireTimer();
		electionHandler.endElection(); // Reset any previous election that may have been happening.

		// Check to see if we should bid to become gateway of the new grid.
		if ( gatewayUnknown() ) {
			electionHandler.startElection(getLoc(), addr());
			sendBIDpkt();
			setGatewayElectTimer();
		} else {
			setGatewayExpireTimer();
		}
		lastGrid = getGrid();
	}

	// Always, no matter what, check to see if we left the grid.
	setCheckLeaveGridTimer();
}

////////////////////////////////////////////////////////////////////////////
//
//  Packet thru-put
//
//  Packet I/O for gateway election and data transmission.

void GeoGRIDAgent::recvGATEpkt(Packet *pkt) {
	struct hdr_ip*      iphdr      = hdr_ip::access(pkt);
	struct hdr_geogrid* geogridhdr = hdr_geogrid::access(pkt);

	// Keep track of neighboring grids.  Any GATE packet that comes, tell the neighborgrids member object, but let it
	//  decide whether to record the packet.
	// Print debug information
	if ( isVerboseEnough() ) {
		cout << addr() << " at " << Scheduler::instance().clock() << ": GATE packet received from " << iphdr->saddr() << " in grid " << geogridhdr->sourceGrid << endl;
		cout << "      before other gateway is " << neighborGrids.getGateway(geogridhdr->sourceGrid) << endl;
	}
	neighborGrids.setGateway(geogridhdr->sourceGrid, iphdr->saddr());
	if ( isVerboseEnough() ) {
		cout << "      after other gateway is " << neighborGrids.getGateway(geogridhdr->sourceGrid) << endl;
		cout << "      I am at " << getGrid() << " and used to be at " << lastGrid << endl;
	}

	// Only look at packets that come from the receiving agent's own grid.
	if ( geogridhdr->sourceGrid == getGrid() ) {                

		// If we think we're the gateway (double-gateway situation)
		if ( isGateway() ) {
			// If the sender is closer, silently aknowledge the other gateway.
			if ( isCloserToCenter(geogridhdr->sourceLoc) ) {
				if ( isVerboseEnough() ) cout << "      based on packet from " << iphdr->saddr() << " silently acknowledging other gateway" << endl;
				setGateway(iphdr->saddr());
				setGatewayExpireTimer();
				killGatewayAnnounceTimer();
				Packet::free(pkt);
				return;
			}
			
			// Refute the other gateway's gatewayness
			sendGATEpkt();
			setGatewayAnnounceTimer();
			Packet::free(pkt);
			return;
		}

        // Normal case -- non-gateway receiving a GATE packet.
		setGateway(iphdr->saddr());      // Silently acknowledge
		setGatewayExpireTimer();         // Reset normal expiration timer -- the gateway has checked in.

		electionHandler.endElection();   // Totally ignore any existing election
		killGatewayElectTimer();
		Packet::free(pkt);
		return;
	}
}
void GeoGRIDAgent::recvBIDpkt(Packet *pkt) { 
	struct hdr_ip*      iphdr      = hdr_ip::access(pkt);
	struct hdr_geogrid* geogridhdr = hdr_geogrid::access(pkt);

	// Print debug information
	if ( isVerboseEnough() ) {
		cout << addr() << " at " << Scheduler::instance().clock() << ": BID packet received from " << iphdr->saddr() << " in grid " << geogridhdr->sourceGrid << endl;
	}
	
	if ( geogridhdr->sourceGrid == getGrid() ) {      // Is this packet coming from our grid?
		// Are we the gateway?  If so, refute the bid
		if ( isGateway() ) {                       
			if ( isVerboseEnough() ) cout << "      Refuting bid.  I'm the gateway" << endl;
			sendGATEpkt();                           // Refute the bid
			Packet::free(pkt);
			return;
		}
		
		// Begin an election -- or perhaps continue one.
		if ( !electionHandler.isBidding() ) {
			electionHandler.startElection(getLoc(), addr());
		}
		electionHandler.recvBIDpkt(geogridhdr->sourceLoc, iphdr->saddr());
		setGatewayElectTimer();
		
		// If we are a better candidate?  If so, challenge the bid
		if ( isCloserToCenter(geogridhdr->sourceLoc) ) {  
			if ( isVerboseEnough() ) cout << "      Challenging bid.  I'm closer." << endl;
			sendBIDpkt();
			Packet::free(pkt);
			return;
		}
		
		if ( isVerboseEnough() ) cout << "     Accepting bid." << endl;
	}
	Packet::free(pkt);
	return;
}

void GeoGRIDAgent::recvRETIREpkt(Packet *pkt) {
	struct hdr_ip*      iphdr      = hdr_ip::access(pkt);
	struct hdr_geogrid* geogridhdr = hdr_geogrid::access(pkt);

	// Print debug information
	if ( isVerboseEnough() ) {
		cout << addr() << Scheduler::instance().clock() << ": RETIRE packet received from " << iphdr->saddr() << " in grid " << geogridhdr->sourceGrid << endl;
	}

   	// Keep track of neighboring grids.  Any RETIRE packet that comes, tell the neighborgrids member object, but let it
	//  decide whether to record the packet.
	neighborGrids.setGateway(geogridhdr->sourceGrid, GATEWAY_UNKNOWN);
	
	if ( geogridhdr->sourceGrid == getGrid() ) {      // Is this packet coming from our grid?
		// On the off chance that we're the gateway receiving a retire packet
		if ( isGateway() ) {
			sendGATEpkt();
			setGatewayAnnounceTimer();
			Packet::free(pkt);
			return;
		}
		
		// Need to determine who is the new gateway
		sendBIDpkt();
		electionHandler.startElection(getLoc(), addr());
		setGatewayElectTimer();
		Packet::free(pkt);
		return;
	}
		
}

////////////////////////////////////////////////////////////////////////////
//
//  recvDATApk
//
//   Handles all DATA packet receptions.  This function will simply decide
//    whether to pass the packet to one of four other functions, which exist
//    to avoid an unweildy if tree.
//
void GeoGRIDAgent::recvDATApkt(Packet *pkt) {
	// Access the GeoGRID header for the new packet:
	struct hdr_geogrid* geogridhdr = hdr_geogrid::access(pkt);

	// cout << "Packet received in grid: " << getGrid() << endl;

	// Non-gateways never resend
	if ( ! isGateway() ) {
		Packet::free(pkt);
		return;
	}

	// Is it in the destination region?  (Successful transmission)
	if (geogridhdr->destRegion.contains(getLoc())) {	
		recvDATApktSuccess(pkt);
   		return;
	}
	
	GeoGRIDRegion forwardRegion = geogridhdr->destRegion;
	forwardRegion.growToInclude(getCenterLoc(geogridhdr->sourceGrid));
	forwardRegion.growToGrids();

	// Is it in the forwarding region?
	if (forwardRegion.contains(getLoc())) {
		// Ticket-based forwarding?
		if ( geogridhdr->forwardMethod == hdr_geogrid::GEOGRID_TICKET ) {
			recvDATA_TICKETpkt(pkt);
			return;
		}
		
		// Flood-based forwarding?
		if ( geogridhdr->forwardMethod == hdr_geogrid::GEOGRID_FLOOD ) {
			recvDATA_FLOODpkt(pkt);
			return;
		}
		
		// Drop it -- unknown type
		cerr << "Unknown packet forward method type " << geogridhdr->forwardMethod << endl;
		cerr << "Dropping packet." << endl;
		Packet::free(pkt);
		return;
	}

	// I am not in multicast region and forwarding region.
	// I just discard the packet.
	//	cout << "Node from grid " << getGrid() << ", previous grid " << geogridhdr->lastHopGrid << " location " << getLoc() << ", dropping packet, outside of multicast region" << endl;
	Packet::free(pkt);
	return;
}

////////////////////////////////////////////////////////////////////////////
//
//  recvDATA_FLOODpkt
//
//   Handles a FLOOD-method packet sent within the forwarding region.
//    Such packets are subject to a packet-duplication check to prevent
//    endless rebroadcasting.
//
void GeoGRIDAgent::recvDATA_FLOODpkt(Packet *pkt) {
	// Access the GeoGRID header for the new packet:
	struct hdr_cmn*     cmnhdr     = hdr_cmn::access(pkt);

	// already seen it, or I sent it, just free it and drop it
	if ((this->dupRcvdPacket(cmnhdr->uid_) || this->dupSentPacket(cmnhdr->uid_)) ) {
		Packet::free(pkt);
		return;
	}

	// This is redundant for now because dupRcvdPacket records the packet
	// as recieved, but I don't like that side effect and may change it
	// later.
	recordRcvdPacket(cmnhdr->uid_);

	// Update last location and grid
	// geogridhdr->lastHopLoc = getLoc();
	// geogridhdr->lastHopGrid = getGrid();

	// Send it
	this->rebroadcastPacket(pkt);
	return;
		

}

////////////////////////////////////////////////////////////////////////////
//
//  recvDATA_TICKETpkt
//
//   Handles a TICKET-method packet sent within the forwarding region.
//    Such packets are NOT subject to a packet-duplication check to prevent
//    endless rebroadcasting, because they only rebroadcast if the sender
//    told them to.
//
void GeoGRIDAgent::recvDATA_TICKETpkt(Packet *pkt) {
	// Access the GeoGRID header for the new packet:
	struct hdr_geogrid* geogridhdr = hdr_geogrid::access(pkt);

	int availableTickets = 0;
	// Loop through the packet's data fields and find the one that corresponds with this one -- if one does at all.
	for ( int count = 0; count < 3; count++ ) {
		if ( geogridhdr->ticketGrids[count] == getGrid() ) {
			availableTickets = geogridhdr->ticketNums[count];
			break;
		}
	}
	
	// If no matching grids found, the sender did not see fit to send this gateway any tickets.  Disregard.
	if ( availableTickets == 0 ) {
		//cout << "Node from grid " << getGrid() << ", previous grid " << geogridhdr->lastHopGrid << ", location " << getLoc() << " dropping packet: not enough tickets" << endl;
		Packet::free(pkt);
		return;
	}	

	doTicketMethodStuff(geogridhdr, availableTickets);

	// Update last location and grid
	// geogridhdr->lastHopLoc = getLoc();
	// geogridhdr->lastHopGrid = getGrid();

	// Send it
	this->rebroadcastPacket(pkt);
	return;
}

////////////////////////////////////////////////////////////////////////////
//
//  recvDATApktSuccess
//
//   Handles the condition where a packet is received within the destination region
//
//   This case is the same for ticket-based packets and flood-based packets.
//
void GeoGRIDAgent::recvDATApktSuccess(Packet *pkt) {
	// Access the GeoGRID header for the new packet:
	struct hdr_cmn*     cmnhdr     = hdr_cmn::access(pkt);
	struct hdr_geogrid* geogridhdr = hdr_geogrid::access(pkt);

	
	// already seen it, or I sent it, just free it and drop it
	if ((this->dupRcvdPacket(cmnhdr->uid_) || this->dupSentPacket(cmnhdr->uid_)) ) {
		Packet::free(pkt);
		return;
	}

	// This is redundant for now because dupRcvdPacket records the packet
	// as recieved, but I don't like that side effect and may change it
	// later.
	recordRcvdPacket(cmnhdr->uid_);

	// Update last location and grid
	// geogridhdr->lastHopLoc = getLoc();
	// geogridhdr->lastHopGrid = getGrid();
		
	// Update timing (increase total time and total receptions to calculate an average later)
	rcvdDataPkts++;

	// Update one success rate.  The successID array is there to make
	// sure each packet received is only counted once.  It may need to
	// be resized.
	if ( cmnhdr->uid_ >= (signed)successID.size() ) {
		successID.resize(successID.size() + VECTOR_INCREMENT, 0);
	}
	if ( successID[cmnhdr->uid_] == 0 ) {
		oneSuccess++;
		successID[cmnhdr->uid_]++;
		totalDelay += Scheduler::instance().clock() - geogridhdr->sendTime_;
		totalPktHops += cmnhdr->num_forwards_;
	}
	
	// I am in multicast region, I must rebroadcast the packet.
	this->rebroadcastPacket(pkt);
	return;
}

////////////////////////////////////////////////////////////////////////////
//
//  GATE
//
void GeoGRIDAgent::sendGATEpkt() {
	// Make sure link layer and node have been initialized.
	if ((ll == NULL) || (node == NULL)) {
		cerr << "Link layer and node must be set on an geogrid agent" << endl
			 << "before anything can be sent." <<endl;
		return;
	}
	node->update_position();
  
	// Create a new packet
	Packet* gatePkt = allocpkt();

	// Access the GeoGRID header for the new packet:
	struct hdr_geogrid* geogridhdr = hdr_geogrid::access(gatePkt);
  	
	// Fill out the header
	geogridhdr->type        = hdr_geogrid::GEOGRID_GATE;
	geogridhdr->sourceGrid  = getGrid();   // Does us no good to send a retire packet to the grid we're entering.  So use the old grid.
	geogridhdr->sourceLoc   = getLoc();
	geogridhdr->dataLength  = 0;         // Does not contain data.
  
	// Store the current time in the 'send_time' field
	geogridhdr->sendTime_   = Scheduler::instance().clock();
  
	if ( isVerboseEnough() ) {
		cout << "      sending GATE packet from " << getLoc() << " and grid " << getGrid() << endl;
	}
	broadcastPacket(gatePkt);
}

////////////////////////////////////////////////////////////////////////////
//
//  BID
//
void GeoGRIDAgent::sendBIDpkt() {
	// Make sure link layer and node have been initialized.
	if ((ll == NULL) || (node == NULL)) {
		cerr << "Link layer and node must be set on an geogrid agent" << endl
			 << "before anything can be sent." <<endl;
		return;
	}

	// In case optimized bidding is used, sometimes it's best not to send a bid packet.
	//  Just hacking in a function here keeps the decisions out of the sendBIDpkt function.
	if ( electionHandler.isBIDsendOk() ) {
		electionHandler.sendBIDpkt();
	} else {
		return;
	}

	node->update_position();
  
	// Create a new packet
	Packet* bidPkt = allocpkt();

	// Access the GeoGRID header for the new packet:
	struct hdr_geogrid* geogridhdr = hdr_geogrid::access(bidPkt);
  
	// Fill out the header
	geogridhdr->type        = hdr_geogrid::GEOGRID_BID;
	geogridhdr->sourceGrid  = getGrid();   // Does us no good to send a retire packet to the grid we're entering.  So use the old grid.
	geogridhdr->sourceLoc   = getLoc();
	// geogridhdr->lastHopGrid = getGrid();
	// geogridhdr->lastHopLoc  = getLoc();
	geogridhdr->dataLength  = 0;         // Does not contain data.
  
	// Store the current time in the 'send_time' field
	geogridhdr->sendTime_   = Scheduler::instance().clock();

	if ( isVerboseEnough() ) {
		cout << "      sending BID packet from " << getLoc() << " and grid " << getGrid() << endl;
	}
  
	broadcastPacket(bidPkt);
}

////////////////////////////////////////////////////////////////////////////
//
//  RETIRE
//
void GeoGRIDAgent::sendRETIREpkt() {
	// Make sure link layer and node have been initialized.
	if ((ll == NULL) || (node == NULL)) {
		cerr << "Link layer and node must be set on an geogrid agent" << endl
			 << "before anything can be sent." <<endl;
		return;
	}
	node->update_position();
  
	// Create a new packet
	Packet* retirePkt = allocpkt();

	// Access the GeoGRID header for the new packet:
	struct hdr_geogrid* geogridhdr = hdr_geogrid::access(retirePkt);
  
	// Fill out the header
	geogridhdr->type        = hdr_geogrid::GEOGRID_RETIRE;
	geogridhdr->sourceGrid  = lastGrid;   // Does us no good to send a retire packet to the grid we're entering.  So use the old grid.
	geogridhdr->sourceLoc   = getLoc();
	// geogridhdr->lastHopGrid = lastGrid;
	// geogridhdr->lastHopLoc  = getLoc();
	geogridhdr->dataLength  = 0;         // Does not contain data.
  	geogridhdr->sendTime_   = Scheduler::instance().clock();
  
	if ( isVerboseEnough() ) {
		cout << addr() << "      sending RETIRE packet from " << getLoc() << " and grid " << getGrid() << endl;
	}
	broadcastPacket(retirePkt);
}

////////////////////////////////////////////////////////////////////////////
//
//  DATA
//
void GeoGRIDAgent::sendDATApkt(
							   const GeoGRIDRegion& destRegion,
							   int dataLength,
							   hdr_geogrid::methodType forwardMethod
							   ) {
	// Make sure link layer and node have been initialized.
	if ((ll == NULL) || (node == NULL)) {
		cerr << "Link layer and node must be set on an geogrid agent" << endl
			 << "before anything can be sent." <<endl;
		return;
	}
	node->update_position();
	
	// Create a new packet
	Packet* dataPkt = allocpkt();

	// Access the GeoGRID header for the new packet:
	struct hdr_geogrid* geogridhdr = hdr_geogrid::access(dataPkt);
	struct hdr_cmn*     cmnhdr     = hdr_cmn::access(dataPkt);
	
	// Fill out the header information
	geogridhdr->type          = hdr_geogrid::GEOGRID_DATA;
	geogridhdr->forwardMethod = forwardMethod;
	geogridhdr->destRegion    = destRegion;
	geogridhdr->sourceLoc     = getLoc();
	geogridhdr->sourceGrid    = getGrid();
	geogridhdr->dataLength    = dataLength;
	geogridhdr->sendTime_     = Scheduler::instance().clock();

	// Do ticket specific things if necessary
	if ( forwardMethod == hdr_geogrid::GEOGRID_TICKET ) {
		// Expand the destination region (for the pruposes of ticket allocation) to grid boundaries.
		GeoGRIDRegion expandedDestRegion = destRegion;
		expandedDestRegion.growToGrids();
		cout << "Expanded Destination Region is: " << expandedDestRegion << endl;

		int availableTickets = int(abs(expandedDestRegion.getX1() - expandedDestRegion.getX2())/gridSize + abs(expandedDestRegion.getY1() - expandedDestRegion.getY2())/gridSize + 2);
		doTicketMethodStuff(geogridhdr, availableTickets);
	}
		
	broadcastPacket(dataPkt);
	
	// Add 1 in the dataPktsOrig by originating one data packet
	dataPktsOrig++;

	// This is quite hacky -- I'll do it better if I ever need to know more at the tcl level about packets being sent
	// Causes the tcl-level procedure ultimately calling this c-level function to return the id of the packet sent.
	// This code assumes that sendDATApkt is only ever called from within sendMulticastData. - EK
	Tcl::instance().resultf("%d", cmnhdr->uid_);
}

void GeoGRIDAgent::doTicketMethodStuff(hdr_geogrid *geogridhdr, int availableTickets) {
	// Get the region for ease of access
	// const GeoGRIDRegion& destRegion = geogridhdr->destRegion;

	int gridCount = 0;
	double distances[3];
	// Set initial ticket-based grids to zero
	for ( gridCount = 0; gridCount < 3; gridCount++ ) {
		geogridhdr->ticketGrids[gridCount] = NO_GRID;
	    geogridhdr->ticketNums[gridCount] = 0;
		distances[gridCount] = 10*gridSize;  // Effectively set to infinity -- used for the loop below
	}

	loc_t  dirVector;                       // A vector compared to centers of neighboring grids to determine ticket dist
	dirVector = geogridhdr->destRegion.getCenterLoc() - getLoc();	
	dirVector = dirVector / dist(geogridhdr->destRegion.getCenterLoc(), getLoc()) * gridSize;
	//cout << "dirVector: " << dirVector << endl;

	// Assign relative grids (based on the directionGrids array) to receive packets.
	for ( gridCount = 0; gridCount < 8; gridCount++ ) {
		grid_t referenceGrid = directionGrids[gridCount];
		//cout << "Grid: " << referenceGrid << endl;

		loc_t referenceLoc = getTopRightLoc(referenceGrid);
		//cout << "ReferenceLoc: " << referenceLoc << endl;
		
		double distance = dist(dirVector, referenceLoc);
		//cout << "Distance: " << distance << endl;

		grid_t tempGrid;
		double tempDistance;
		if ( distance < distances[2] ) {
			tempGrid = geogridhdr->ticketGrids[2];
			tempDistance = distances[2];

			geogridhdr->ticketGrids[2] = referenceGrid;
			distances[2] = distance;

			referenceGrid = tempGrid;
			distance = tempDistance;
		}

		if ( distance < distances[1] ) {
			tempGrid = geogridhdr->ticketGrids[1];
			tempDistance = distances[1];

			geogridhdr->ticketGrids[1] = referenceGrid;
			distances[1] = distance;

			referenceGrid = tempGrid;
			distance = tempDistance;
		}

		if ( distance < distances[0] ) {
			geogridhdr->ticketGrids[0] = referenceGrid;
			distances[0] = distance;
		}
	}

	// Distribute tickets, make grids non-relative
	for (gridCount = 0; gridCount < 3; gridCount++) {
		// Give the grid a fair portion of the available tickets.
        // 1/3 of all of the total, 1/2 of the rest, or all of the remaining after that.
		// Distributes tickets evenly and nicely even if the number of tickets is not a multiple of three.
		geogridhdr->ticketGrids[gridCount] = geogridhdr->ticketGrids[gridCount] + getGrid();
		geogridhdr->ticketNums[gridCount] = availableTickets/(3-gridCount);
		availableTickets -= geogridhdr->ticketNums[gridCount];
	}

 	cout << "Transmitting from grid: " << getGrid() << " at " << Scheduler::instance().clock() << endl;
	// cout << "    this packet came from: " << geogridhdr->lastHopGrid << endl;
 	cout << "  Grid: " << geogridhdr->ticketGrids[0] << " has " << geogridhdr->ticketNums[0] << endl;
 	cout << "  Grid: " << geogridhdr->ticketGrids[1] << " has " << geogridhdr->ticketNums[1] << endl;
 	cout << "  Grid: " << geogridhdr->ticketGrids[2] << " has " << geogridhdr->ticketNums[2] << endl;
}


////////////////////////////////////////////////////////////////////////////
//
//  command
//
//  All tcl commands used on this object other than default ones are passed
//   to this function, where they can be intercepted and interpreted.
//
//  argv[0] is usually "cmd"
//  argv[1] is command name
//  argv[2..(argc-1)] are the arguments
//
//  If argument is unrecognized, pass to the Agent subclass.  In many cases,
//   this is because of a typo of some sort, and the command is illegal.  In
//   such a case, ns2 will simply abort and display the call stack, but will
//   not output that the command was unrecognized.  If that happens, check to
//   make sure that the command that caused the problem was typed correctly,
//   among other things.
//
int GeoGRIDAgent::command(int argc, const char*const* argv)
{
	TclObject *obj;

	switch (argc) {
	case 2:
		// begin
		//  Call at beginning of simulation to begin elections, etc.
		if (strcmp(argv[1], "begin") == 0) {	
			if ( isVerboseEnough() ) cout << addr() << " at " << Scheduler::instance().clock() << ": beginning simulation " << endl;
			if ((ll == NULL) || (node == NULL)) {
				Tcl::instance().resultf("Link layer and node must be set on an geogrid agent before begin can be called.\n");
				return (TCL_ERROR);
			}

			// Output debug info
			//			cout << addr() << " at " << Scheduler::instance().clock() << ": Beginning simulation " << addr() << " in grid " << getGrid() << endl;
			  
			// Bid -- begin initial gateway election
			setGatewayElectTimer();
			electionHandler.startElection(getLoc(), addr());
			sendBIDpkt();

			// Begin check-to-leave-grid loop.
			setCheckLeaveGridTimer();

			node->update_position();
			lastGrid = getGrid();
			neighborGrids.move(getGrid());  // I could write a separate init routine, but move will get the job done.
			
			return (TCL_OK);
		} // begin

		// geogridDone
		// Call at the end of the simulation to give the agent a chance to clean up and calls for error reporting.
		if (strcmp(argv[1], "geogridDone") == 0) {
			if ( this->addr() == 0 ) {
				if ( reportNodes == 0 ) {
					cout << "oneSuccessRate     " << (float)oneSuccess/dataPktsOrig << endl;

					// Make sure not to print this information if it will cause a division by zero.  The data-compilation scripts will handle the absences.
					if ( oneSuccess != 0 ) {
						cout << "pktsPerOne         " << (float)(controlPkts  + dataPkts)  / oneSuccess << endl;
						cout << "bytesPerOne        " << (float)(controlBytes + dataBytes) / oneSuccess << endl;
						cout << "controlPktsPerOne  " << (float)controlPkts                / oneSuccess << endl;
						cout << "controlBytesPerOne " << (float)controlBytes               / oneSuccess << endl;
						cout << "dataPktsPerOne     " << (float)dataPkts                   / oneSuccess << endl;
						cout << "dataBytesPerOne    " << (float)dataBytes                  / oneSuccess << endl;
						cout << "eeDelay            " << (float)totalDelay                 / oneSuccess << endl;
						cout << "numHops            " << (float)totalPktHops               / oneSuccess << endl;
					}
				}			
			}
			return (TCL_OK);
		} // geogridDone

		// testFunc
		// A useful function for prototyping things and testing them while I get this to work.  EK
		if (strcmp(argv[1], "testFunc") == 0) {
			Packet* pkt = allocpkt();
			struct hdr_geogrid* geogridhdr = hdr_geogrid::access(pkt);
			geogridhdr->forwardMethod      = hdr_geogrid::GEOGRID_FLOOD;
			geogridhdr->type               = hdr_geogrid::GEOGRID_DATA;
			cout << "Data " << geogridhdr->size() << endl;

			geogridhdr->type               = hdr_geogrid::GEOGRID_GATE;
			cout << "Gate " << geogridhdr->size() << endl;

			geogridhdr->type               = hdr_geogrid::GEOGRID_BID;
			cout << "Bid " << geogridhdr->size() << endl;

			geogridhdr->type               = hdr_geogrid::GEOGRID_RETIRE;
			cout << "Retire " << geogridhdr->size() << endl;

			geogridhdr->forwardMethod      = hdr_geogrid::GEOGRID_TICKET;
			geogridhdr->type               = hdr_geogrid::GEOGRID_DATA;
			cout << "Data " << geogridhdr->size() << endl;

			geogridhdr->type               = hdr_geogrid::GEOGRID_GATE;
			cout << "Gate " << geogridhdr->size() << endl;

			geogridhdr->type               = hdr_geogrid::GEOGRID_BID;
			cout << "Bid " << geogridhdr->size() << endl;

			geogridhdr->type               = hdr_geogrid::GEOGRID_RETIRE;
			cout << "Retire " << geogridhdr->size() << endl;
			
			delete pkt;
			return (TCL_OK);
		} // testFunc

		// isGateway
		//  Returns true if this node thinks its a gateway.
		//  This funciton is intended for debug purposes.
		if (strcmp(argv[1], "isGateway") == 0) {
			int retVal = isGateway();
			Tcl::instance().resultf("%d", retVal);
			return (TCL_OK);
		} // isGateway

		// isBidding
		//  Returns true if this node is in the middle of the bidding process
		//  This funciton is intended for debug purposes.
		if (strcmp(argv[1], "isBidding") == 0) {
			int retVal = electionHandler.isBidding();
			Tcl::instance().resultf("%d", retVal);
			return (TCL_OK);
		} // isBidding

		// expireTimerPending
		//  Returns true if this node's expire timer is set
		//  This funciton is intended for debug purposes.
		if (strcmp(argv[1], "expireTimerPending") == 0) {
			int retVal = gatewayExpireTimer.status() == TIMER_PENDING;
			Tcl::instance().resultf("%d", retVal);
			return (TCL_OK);
		} // expireTimerPending

		// electTimerPending
		//  Returns true if this node's elect timer is set
		//  This funciton is intended for debug purposes.
		if (strcmp(argv[1], "electTimerPending") == 0) {
			int retVal = gatewayElectTimer.status() == TIMER_PENDING;
			Tcl::instance().resultf("%d", retVal);
			return (TCL_OK);
		} // electTimerPending

		// announceTimerPending
		//  Returns true if this node's announce timer is set
		//  This funciton is intended for debug purposes.
		if (strcmp(argv[1], "announceTimerPending") == 0) {
			int retVal = gatewayAnnounceTimer.status() == TIMER_PENDING;
			Tcl::instance().resultf("%d", retVal);
			return (TCL_OK);
		} // announceTimerPending

		break;
	case 3:
		// set-ll
	    // Boilerplate command for agents of mobile nodes, sets link layer pointer
		if (strcmp(argv[1], "set-ll") == 0) {
			if( (obj = TclObject::lookup(argv[2])) == 0) {
			    Tcl::instance().resultf("lookup of \"%s\" failed\n", argv[2]);
				return (TCL_ERROR);
			}
			ll = (NsObject*) obj;
			return (TCL_OK);
		} // set-ll

		// set-node
		// Boilerplate command for agents of mobile nodes, sets node that this object is an agent for
		if (strcmp(argv[1], "set-node") == 0) {  
			if( (obj = TclObject::lookup(argv[2])) == 0) {
				Tcl::instance().resultf("lookup of \"%s\" failed\n", argv[2]);
				return (TCL_ERROR);
			}

			node = dynamic_cast< MobileNode * >(obj);
			if (node) {      // dynamic cast was successful and didn't return NULL
				return (TCL_OK);
			}

			Tcl::instance().resultf("Unable to dynamically cast \"%s\" to a MobileNode\n", argv[2]);
			return (TCL_ERROR);
		} // set-node
		break;
	case 6:
		// isWithinRegion
		// usage: "GeoGRIDAgent isWithinRegion llx lly urx ury"
		// Returns true if the agent is within the specified region
		// Intended for debug purposes
		if (strcmp(argv[1], "isWithinRegion") == 0) {
			node->update_position();
			GeoGRIDRegion testRegion(GeoGRIDRegion::GEOGRID_BOX, atof(argv[2]), atof(argv[3]), atof(argv[4]), atof(argv[5]));

			int retVal = testRegion.contains(getLoc());

			Tcl::instance().resultf("%d", retVal);
			return (TCL_OK);
		} // isWithinRegion

		break;
	case 8:
		// sendMulticastData
		// usage: "GeoGRIDAgent sendMulticastData xMinExt yMinExt xMaxExt yMaxExt size method"
		// Sends data using the given method to the multicast region, specified in grid coordinates by xMinExt etc.
		if (strcmp(argv[1], "sendMulticastData") == 0) {
			GeoGRIDRegion            destRegion(GeoGRIDRegion::GEOGRID_BOX, atof(argv[2]), atof(argv[3]), atof(argv[4]), atof(argv[5]));
			int                      size          = atoi(argv[6]);
			hdr_geogrid::methodType  forwardMethod = hdr_geogrid::methodType(atoi(argv[7]));

			sendDATApkt(destRegion, size, forwardMethod);
			
			return (TCL_OK);
		} // sendMulticastData
		break;
	}

	// If the command hasn't been processed by GeocastAgent()::command,
	// call the command() function for the base class
	return (Agent::command(argc, argv));
}

//////////////////////////////////////////////////////////////////////////
// 
//  recv
//  Function called when any packet is received.  Decides whether it's a
//   DATA, GATE, BID, or RETIRE packet.
void GeoGRIDAgent::recv(Packet* pkt, Handler*)
{
	// Access the common header for the received packet:
	// new way to do this (2.1b7 and later)
	struct hdr_geogrid* geogridhdr = hdr_geogrid::access(pkt);

	node->update_position();

	// What kind of packet is it?
	switch( geogridhdr->type ) {
	case hdr_geogrid::GEOGRID_DATA:
		recvDATApkt(pkt);
		return;
	case hdr_geogrid::GEOGRID_GATE:
		recvGATEpkt(pkt);
		return;
	case hdr_geogrid::GEOGRID_BID:
		recvBIDpkt(pkt);
		return;
	case hdr_geogrid::GEOGRID_RETIRE:
		recvRETIREpkt(pkt);
		return;
	case hdr_geogrid::GEOGRID_UNKNOWN:
	default:
		// there is an error if this happens, but I'm not sure how to
		// respond, so it is commented out for now
		cerr << "The geogrid code is unset, so I'm freeing the packet." << endl;
		Packet::free(pkt);
		return;
	}

	return;
}

// ***********************************************************************
//                            dupRcvdPacket
// ***********************************************************************
bool GeoGRIDAgent::dupRcvdPacket(int id)
{
	// I may need to limit the size of this in the future.
	if (rcvdPackets.find(id) != rcvdPackets.end())
		{
			return true;
		} else {
			rcvdPackets.insert(id);
			return false;
		}
}

// ***********************************************************************
//                            dupSentPacket
// ***********************************************************************
bool GeoGRIDAgent::dupSentPacket(int id)
{
	// I may need to limit the size of this in the future.
	if (sentPackets.find(id) != sentPackets.end())
		{
			return true;
		} else {
			sentPackets.insert(id);
			return false;
		}
}	 

// ***********************************************************************
//                            broadcastPacket
// ***********************************************************************
void GeoGRIDAgent::broadcastPacket(Packet *p)
{
	// Access the common header for the new packet:
	// new way to do this (2.1b7 and later)
	struct hdr_cmn*     cmnhdr     = hdr_cmn::access(p);
	struct hdr_ip*      iphdr      = hdr_ip::access(p);
	struct hdr_geogrid* geogridhdr = hdr_geogrid::access(p);

	// set all the necessary things for the common header
	cmnhdr->next_hop_   = IP_BROADCAST;       // broadcast
	cmnhdr->prev_hop_   = this->addr();
	cmnhdr->direction() = hdr_cmn::DOWN;      // hopefully send it out
	cmnhdr->size()      = geogridhdr->size(); // set the size

	// record the sending of the correct number of bytes/packet
	if ( geogridhdr->type == hdr_geogrid::GEOGRID_DATA ) {
		dataBytes += cmnhdr->size();
		dataPkts++;
	} else {
		controlBytes += cmnhdr->size();
		controlPkts++;
	}

	// now the ip header stuff
	iphdr->saddr() = this->addr();
	iphdr->sport() = 220;
	iphdr->daddr() = IP_BROADCAST;
	iphdr->dport() = 220;
	iphdr->ttl()   = 32;

	// Send the packet
	recordSentPacket(cmnhdr->uid_);

	double jitter = 0.0;
	if (GeoGRIDuseJitteronBroadcast) {
		// this one is different and was taken from dsragent::sendOutBCastPkt
		// I had to add a little jitter because it turned out that neighboring nodes
		// where re-broadcasting in the simulator at "exactly" the same time and
		// killing each other's transmissions.
		jitter = jitterFunc();
	} else {
		jitter = 0.0;
	}

	if (verbosity > 1) {
		cout << "Broadcast---------------" << endl;
		cout << "Scheduling the packet for delivery:  " << cmnhdr->uid_ 
			 << " with jitter=" << jitter << endl;
		cout << "Really sending at:  " << Scheduler::instance().clock() + jitter << endl;
	}
 
	Scheduler::instance().schedule(ll, p, jitter);

	if (!ll) {
		cerr << "Crap, the link layer is NULL!!!!!" << endl;
	}

	return;
}


// ***********************************************************************
//                            rebroadcastPacket
// ***********************************************************************
// same as broadcastPacket, but 
//      - we don't change the ip header source address,
//      - we have to set previous hop,
//      - we need to decrement the ttl
//      - and we have to set the direction to down
//      - if ticket-based forwarding is used, DO re-send already received packets.

void GeoGRIDAgent::rebroadcastPacket(Packet *p) {
	// Access the common header for the new packet:
	// new way to do this (2.1b7 and later)
	struct hdr_cmn*     cmnhdr     = hdr_cmn::access(p);
	struct hdr_ip*      iphdr      = hdr_ip::access(p);
	struct hdr_geogrid* geogridhdr = hdr_geogrid::access(p);

	// set all the necessary things for the common header
	cmnhdr->next_hop_ = IP_BROADCAST;  // broadcast
	cmnhdr->prev_hop_ = this->addr();
	cmnhdr->direction() = hdr_cmn::DOWN;    // hopefully send it out
	cmnhdr->size()      = geogridhdr->size();

	// record the sending of the correct number of bytes/packet
	if ( geogridhdr->type == hdr_geogrid::GEOGRID_DATA ) {
		dataBytes += cmnhdr->size();
		dataPkts++;
	} else {
		controlBytes += cmnhdr->size();
		controlPkts++;
	}

	// this doesn't have to be done, (it should be ok from the original)
	// but just in case...
	iphdr->sport() = 220;
	iphdr->daddr() = IP_BROADCAST;
	iphdr->dport() = 220;

	// this is a difference from the regular broadcast
	// it appears that this needs to be done
	// since this packet doesn't go through the routing agent, this is never
	// done anywhere but here
	iphdr->ttl()--;

	// Send the packet
	recordSentPacket(cmnhdr->uid_);

	double jitter = 0.0;
	if (GeoGRIDuseJitteronBroadcast) {
		// this one is different and was taken from dsragent::sendOutBCastPkt
		// I had to add a little jitter because it turned out that neighboring nodes
		// where re-broadcasting in the simulator at "exactly" the same time and
		// killing each other's transmissions.
		jitter = jitterFunc();
	} else {
		jitter = 0.0;
	}

	if (verbosity > 1) {
		cout << "Rebroadcast---------------" << endl;
		cout << "Scheduling the packet for delivery:  " << cmnhdr->uid_ 
			 << " with jitter=" << jitter << endl;
		cout << "Really sending at:  " << Scheduler::instance().clock() + jitter << endl;
	}
	
	Scheduler::instance().schedule(ll, p, jitter);
	
	if (!ll) {
		cerr << "Crap, the link layer is NULL!!!!!" << endl;
	}

	return;
}

// ***********************************************************************
//                            recordSentPacket
// ***********************************************************************
void GeoGRIDAgent::recordSentPacket(int id)
{
	sentPackets.insert(id);
	return;
}

// ***********************************************************************
//                            recordRcvdPacket
// ***********************************************************************
void GeoGRIDAgent::recordRcvdPacket(int id)
{
	rcvdPackets.insert(id);
	return;
}

// ***********************************************************************
//                            printHeaders
// Print out all the headers of the packet if the verbosity is high enough.
// ***********************************************************************
void GeoGRIDAgent::printHeaders(Packet *p)
{
	struct hdr_mac* machdr = hdr_mac::access(p);
	struct hdr_cmn* cmnhdr = hdr_cmn::access(p);
	struct hdr_ip* iphdr = hdr_ip::access(p);


	// Access the GeoGRID header for the new packet:
	//hdr_geogrid* geogridhdr = (hdr_geogrid*)p->access(off_geogrid_);

	if (verbosity > 1) {
		cout << "IP Header Information:" << endl;
		cout << "\tSrc Address:   " << iphdr->saddr() << endl;
		cout << "\tSrc Port:      " << iphdr->sport() << endl;
		cout << "\tDest Address:  " << iphdr->daddr() << endl;
		cout << "\tDest Port:     " << iphdr->dport() << endl;
		cout << "\tTTL:           " << iphdr->ttl() << endl;
		
		cout << endl;
		cout << "Common Header Information:" << endl;
		cout << "\tPacket Type:   " << p_info().name(cmnhdr->ptype()) << endl;
		cout << "\tSize:          " << cmnhdr->size() << endl;
		cout << "\tUID:           " << cmnhdr->uid() << endl;
		cout << "\terror:         " << cmnhdr->error() << endl;
		cout << "\ttimestamp:     " << cmnhdr->timestamp() << endl;
		cout << "\tinterface:     " << cmnhdr->iface() << endl;
		cout << "\tDirection:     " << cmnhdr->direction() << endl;
		cout << "\tpkt ref count: " << cmnhdr->ref_count() << endl;
		cout << "\tprev hop:      " << cmnhdr->prev_hop_ << endl;
		cout << "\tnext hop:      " << cmnhdr->next_hop() << endl;
		cout << "\taddress type:  " << cmnhdr->addr_type() << endl;
		cout << "\tlast hop:      " << cmnhdr->last_hop_ << endl;
		cout << "\tnum forwards:  " << cmnhdr->num_forwards() << endl;
		cout << "\topt forwards:  " << cmnhdr->opt_num_forwards() << endl;
		
		cout << endl;
		cout << "MAC Header Information:" << endl;
		cout << "\tSrc Address:   " << machdr->macSA() << endl;
		cout << "\tDest Address:  " << machdr->macDA() << endl;
	}
	
	return;
}

////////////////////////////////////////////////////////////////////////////
//
//  GeoGRIDRegion Constructors
//
//  In order to facilitate different regions, I'm using different constructors.  I do not use different classes
//   for each of the regions and do polymorphism (trust me I'd like to) because I don't want to be dynamically
//   creating objects and handing around packets with pointers in them.  In a packet done with a struct,
//   that could get messy fast.  So, I'm using a one-class-fits-all approach, which means that the region type
//   goes in as the first argument and I've got switch statements inside my member functions.
//
//  Note:  Number of arguments alone can easily decide which forwarding type to use.  However,
//   this is a flaky way of doing things.  If arguments change, or there are more than one region type with the
//   same number of arguments, it would wreak havoc.  Therefore, both type and arg number below are required to match.
// BOX region constructor
GeoGRIDRegion::GeoGRIDRegion(GeoGRIDRegion::regionType type, double xMinExt, double yMinExt, double xMaxExt, double yMaxExt) {
	if ( type != GEOGRID_BOX ) {
		cout << "Unknown region type " << type << ".  Assuming BOX (" << int(GEOGRID_BOX) << ")." << endl;
	}

	// Note: no reason to force the first pair to be lower left or second to be upper right.  Just handle it.
	lowerLeft.x = min(xMinExt, xMaxExt);
	lowerLeft.y = min(yMinExt, yMaxExt);
	upperRight.x = max(xMinExt, xMaxExt);
	upperRight.y = max(yMinExt, yMaxExt);
}

////////////////////////////////////////////////////////////////////////////
//
// contains
//
//  Returns true if the region contains the given grid.
bool GeoGRIDRegion::contains(loc_t loc) {
	return loc.x >= lowerLeft.x && loc.x <= upperRight.x &&
	       loc.y >= lowerLeft.y && loc.y <= upperRight.y;
}
	
/////////////////////////////////////////////////////////////////////////////
//
//  growToInclude
//
//  Causes this object to become the smallest region that inclues both its former self and the given loc
//  Useful for calculating forwarding regions.
void GeoGRIDRegion::growToInclude(loc_t loc) {
	lowerLeft.x  = min(loc.x, lowerLeft.x);
	lowerLeft.y  = min(loc.y, lowerLeft.y);
	upperRight.x = max(loc.x, upperRight.x);
	upperRight.y = max(loc.y, upperRight.y);
}

/////////////////////////////////////////////////////////////////////////////
//
//  growToGrids
//
//  Causes this object to become the smallest region that inclues both its former self is aligned to grid
//   edges
void GeoGRIDRegion::growToGrids() {
	growToInclude(GeoGRIDAgent::getBottomLeftLoc  (upperRight));
	growToInclude(GeoGRIDAgent::getTopRightLoc    (lowerLeft) );
}

/////////////////////////////////////////////////////////////////////////////
//
//  getCenterLoc
//
//  Returns the object at the center of this region
loc_t GeoGRIDRegion::getCenterLoc() const {
	return (lowerLeft + upperRight) / 2;
}


	// Calculate the next three closer.  This is done by looking at all nine neighboring grids to the sender, and adding their distances from
	//  the destination region coordinates.  Only three of the neighboring grids (possibly two) can be closer, using this metric, than the sending
	//  node.
// 	grid_t lookAtGrid(0,0);
// 	gridCount = 0;
// 	int referenceSum = 
// 		abs(getGrid().x - destRegion.getX1()) +
// 		abs(getGrid().x - destRegion.getX2()) +
// 		abs(getGrid().y - destRegion.getY1()) +
// 		abs(getGrid().y - destRegion.getY2());
// 	for ( lookAtGrid.x = getGrid().x - 1; lookAtGrid.x <= getGrid().x + 1; lookAtGrid.x++ ) {
// 		for ( lookAtGrid.y = getGrid().y - 1; lookAtGrid.y <= getGrid().y + 1; lookAtGrid.y++ ) {
// 			int sum =
// 				abs(lookAtGrid.x - destRegion.getX1()) +
// 				abs(lookAtGrid.x - destRegion.getX2()) +
// 				abs(lookAtGrid.y - destRegion.getY1()) +
// 				abs(lookAtGrid.y - destRegion.getY2());

// 			// Check to see if the grid is deserving of tickets.
// 			if ( sum < referenceSum ) {
// 				if ( gridCount >= 3 ) {
// 					cerr << "Warning: situation encountered that requires greater than three ticket-receiving grids.  Ignoring the exra grids." << endl;
// 					break;  // Won't break all the way out of the 2d loop, but that doesn't matter
// 					        // since this also prevents anything from actually happening in an iteration.
// 				}
// 				// Set grid
// 				geogridhdr->ticketGrids[gridCount] = lookAtGrid;
// 				gridCount++;
// 			}
// 		}
// 	}

// 	if ( getGrid().x > destRegion.getX2() ) {
// 		if ( getGrid().y > destRegion.getY2() ) {
// 			geogridhdr->ticketGrids[0] = getGrid() + grid_t(-1, 0);
// 			geogridhdr->ticketGrids[1] = getGrid() + grid_t(-1,-1);
// 			geogridhdr->ticketGrids[2] = getGrid() + grid_t( 0,-1);
// 		} else if (getGrid().y > destRegion.getY1()) {
// 			geogridhdr->ticketGrids[0] = getGrid() + grid_t(-1, 1);
// 			geogridhdr->ticketGrids[1] = getGrid() + grid_t(-1, 0);
// 			geogridhdr->ticketGrids[2] = getGrid() + grid_t(-1,-1);
// 		} else {
// 			geogridhdr->ticketGrids[0] = getGrid() + grid_t( 0, 1);
// 			geogridhdr->ticketGrids[1] = getGrid() + grid_t(-1, 1);
// 			geogridhdr->ticketGrids[2] = getGrid() + grid_t(-1, 0);
// 		}
// 	} else if ( getGrid().x > destRegion.getX1() ) {
// 		if ( getGrid().y > destRegion.getY2() ) {
// 			geogridhdr->ticketGrids[0] = getGrid() + grid_t(-1,-1);
// 			geogridhdr->ticketGrids[1] = getGrid() + grid_t( 0,-1);
// 			geogridhdr->ticketGrids[2] = getGrid() + grid_t( 1,-1);
// 		} else if (getGrid().y > destRegion.getY1()) {
// 			// cerr << "Warning: transmitting from within the destination region." << endl;
// 		} else {
// 			geogridhdr->ticketGrids[0] = getGrid() + grid_t(-1, 1);
// 			geogridhdr->ticketGrids[1] = getGrid() + grid_t( 0, 1);
// 			geogridhdr->ticketGrids[2] = getGrid() + grid_t( 1, 1);
// 		}
// 	} else {
// 		if ( getGrid().y > destRegion.getY2() ) {
// 			geogridhdr->ticketGrids[0] = getGrid() + grid_t( 1, 0);
// 			geogridhdr->ticketGrids[1] = getGrid() + grid_t( 1,-1);
// 			geogridhdr->ticketGrids[2] = getGrid() + grid_t( 0,-1);
// 		} else if (getGrid().y > destRegion.getY1()) {
// 			geogridhdr->ticketGrids[0] = getGrid() + grid_t( 1, 1);
// 			geogridhdr->ticketGrids[1] = getGrid() + grid_t( 1, 0);
// 			geogridhdr->ticketGrids[2] = getGrid() + grid_t( 1,-1);
// 		} else {
// 			geogridhdr->ticketGrids[0] = getGrid() + grid_t( 0, 1);
// 			geogridhdr->ticketGrids[1] = getGrid() + grid_t( 1, 1);
// 			geogridhdr->ticketGrids[2] = getGrid() + grid_t( 1, 0);
// 		}
// 	}
