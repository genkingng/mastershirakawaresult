#####################################################################
#   Copyright (C) 2004  Toilers Research Group -- Colorado School of Mines
#
#   Please see COPYRIGHT.TXT and LICENSE.TXT for copyright and license
#   details.
#####################################################################
######################################################################
#
#  sendmulticastdata.tcl
#     Tcl routine isntalled on the agents to make tcl-level interface
#      easier.
#
#  Ed Krohne June 2003
#
######################################################################

if { [file exists debug.tcl] } {
    source debug.tcl
}

#########################################################################################################
#
# Special arguments for sendMulticastData
#
# To simplify code slightly, I interpret the last argument of sendMultiCastData in an instproc that shadows
# the C-level command.  Interpretation of "FLOOD" versus "TICKET" forwarding is done here, rather than at the
# C-level, to make it simpler to use variable-binding type syntax to declare a default forwarding method like
# so:
# Agent/GeoGRID set forwardMethod_     FLOOD
#
# Due to sendmulticastdata.tcl, it is possible to use all of the following commands
# 
# $agent sendMulticastData 1 1 5 5 10 
# $agent sendMulticastData 1 1 5 5 10 FLOOD
# $agent sendMulticastData 1 1 5 5 10 TICKET
#
#########################################################################################################
Agent/GeoGRID instproc sendMulticastData {minExtX minExtY maxExtX maxExtY size {forwardMethod ""}} {
    set ns [Simulator instance]    ;# Figure out what the simulator is
    
    # If unspecified above, use default forward method defined like a bound variable on the class
    if { $forwardMethod == "" } {
	set forwardMethod [$class set forwardMethod_]
    }

    # Convert strings to numbers -- these numbers correspond to the enumeration declared in geogrid.h
    #  FLOOD is 0, TICKET is 1.
    switch $forwardMethod {
	FLOOD { set forwardMethod 0 }
	TICKET { set forwardMethod 1 }
	default { error "Unrecognized forward method $forwardMethod found." }
    }

    # Evaluate the prepared command.  Note that sendMulticastData actually returns the id of its packet
    # This is accomplished with a rather hacky single line inserted into sendDATApkt at the C level.
    set pktID [eval "$self cmd sendMulticastData $minExtX $minExtY $maxExtX $maxExtY $size $forwardMethod"]
}

