set val(chan)       Channel/WirelessChannel
set val(prop)       Propagation/Nakagami
set val(netif)      Phy/WirelessPhyExt
set val(mac)        Mac/802_11Ext
set val(ifq)        Queue/DropTail/PriQueue
set val(ll)         LL
set val(ant)        Antenna/OmniAntenna
set val(x)          2655
set val(y)          1841
set val(ifqlen)     60   ;#インタフェースキューの最大値
set val(rp)         AODV
set val(nn)         3033
set val(sc)         "kyoto_mobility.tcl"

set val(dataStart)  200.0
set val(dataStop)   300.0
set val(signalStop) 305.0
set val(finish)     310.0

# =====================================================================
# Other default settings

puts "setting other default settings..."

Phy/WirelessPhyExt set CSThresh_                3.162e-12   ;#-85 dBm Wireless interface sensitivity
Phy/WirelessPhyExt set Pt_                      0.001   
Phy/WirelessPhyExt set freq_                    5.9e+9
Phy/WirelessPhyExt set noise_floor_             1.26e-13    ;#-99 dBm for 10MHz bandwidth
Phy/WirelessPhyExt set L_                       1.0         ;#default radio circuit gain/loss
Phy/WirelessPhyExt set PowerMonitorThresh_      3.981071705534985e-18   ;#-174dBm power monitor  sensitivity
Phy/WirelessPhyExt set HeaderDuration_          0.000040    ;#40 us
Phy/WirelessPhyExt set BasicModulationScheme_   0
Phy/WirelessPhyExt set PreambleCaptureSwitch_   1
Phy/WirelessPhyExt set DataCaptureSwitch_       0
Phy/WirelessPhyExt set SINR_PreambleCapture_    2.5118;     ;# 4 dB
Phy/WirelessPhyExt set SINR_DataCapture_        100.0;      ;# 10 dB
Phy/WirelessPhyExt set trace_dist_              1e6         ;# PHY trace until distance of 1 Mio. km ("infinty")
Phy/WirelessPhyExt set PHY_DBG_                 0

Mac/802_11Ext set CWMin_                        15
Mac/802_11Ext set CWMax_                        1023
Mac/802_11Ext set SlotTime_                     0.000013
Mac/802_11Ext set SIFS_                         0.000032
Mac/802_11Ext set ShortRetryLimit_              7
Mac/802_11Ext set LongRetryLimit_               4
Mac/802_11Ext set HeaderDuration_               0.000040
Mac/802_11Ext set SymbolDuration_               0.000008
Mac/802_11Ext set BasicModulationScheme_        0
Mac/802_11Ext set use_802_11a_flag_             true
Mac/802_11Ext set RTSThreshold_                 2346
Mac/802_11Ext set MAC_DBG                       0

# ======================================================================
# Urban propagation model: Nakagami
# ======================================================================
Propagation/Nakagami set use_nakagami_dist_ true
Propagation/Nakagami set gamma0_ 1.8 
Propagation/Nakagami set gamma1_ 2.5 
Propagation/Nakagami set gamma2_ 3.0 
Propagation/Nakagami set d0_gamma_ 20 
Propagation/Nakagami set d1_gamma_ 50 
Propagation/Nakagami set m0_  1.6 
Propagation/Nakagami set m1_  1.0 
Propagation/Nakagami set m2_  0.5 
Propagation/Nakagami set d0_m_ 20 
Propagation/Nakagami set d1_m_ 50 
# ======================================================================

# ======================================================================
# unity gain, omni-directional antennas
# set up the antennas to be centered in the node and 1.5 meters above it
# ======================================================================
Antenna/OmniAntenna set X_ 0
Antenna/OmniAntenna set Y_ 0
Antenna/OmniAntenna set Z_ 1.5
Antenna/OmniAntenna set Gt_ 1.0
Antenna/OmniAntenna set Gr_ 1.0
# ======================================================================


# =====================================================================
# This puts in only the headers that we need.
# =====================================================================
puts "removing unnecessary packet headers..."
remove-all-packet-headers
add-packet-header IP
add-packet-header Common
add-packet-header LBM
add-packet-header LL
add-packet-header Mac
add-packet-header ARP
add-packet-header Flags
add-packet-header UDP 
add-packet-header AODV 


#Create a simulator object
set ns_ [new Simulator]
$ns_ use-scheduler Heap

#Open a trace file

#トレースファイルの指定
set nt [open kyoto_mobility.tr w]
$ns_ trace-all $nt

set nf [open kyoto_mobility.nam w]
$ns_ namtrace-all-wireless $nf $val(x) $val(y)

set val(ni) 1
set topo [new Topography]
$topo load_flatgrid $val(x) $val(y)

set god_ [create-god $val(nn)]

# New API to config node:
# 1. Create channel (or multiple-channels);
# 2. Specify channel in node-config (instead of channelType);
# 3. Create nodes for simulations.

# Create channel #1
puts "creating channel..."
set chan_1_ [new $val(chan)]

#
# define how node should be created
#

#global node setting
puts "setting global node values..."
$ns_ node-config -adhocRouting $val(rp) \
                 -llType $val(ll) \
                 -macType $val(mac) \
                 -ifqType $val(ifq) \
                 -ifqLen $val(ifqlen) \
                 -antType $val(ant) \
                 -propType $val(prop) \
                 -phyType $val(netif) \
                 -channel $chan_1_ \
                 -topoInstance $topo \
                 -agentTrace ON \
                 -routerTrace OFF \
                 -macTrace OFF \
					  -phyTrace ON \
                 -movementTrace ON \

# create the nodes
puts "creating the nodes..."

# create the nodes			 
for {set i 0} {$i < $val(nn) } {incr i} {
  set node_($i) [$ns_ node]	
  $node_($i) random-motion 0           ;# disable random motion
}

#
#Define a 'finish' procedure
proc finish {} {
        global ns_ nt
        $ns_ flush-trace
        close $nt
        exit 0
}

# Load the movement file
puts "Loading the mobility file..."
source $val(sc)

#Create lar agents and attach them to the nodes
puts "creating lbm agents and attaching them to nodes..."
for {set i 0} {$i < $val(nn)} {incr i} {
  set g($i) [new Agent/LBM]
  $node_($i) attach $g($i) 220


  # need to tell the lbm agents about their link layers
  set ll($i) [$node_($i) set ll_(0)]
  $ns_ at 0.0 "$g($i) set-ll $ll($i)"

  # need to tell the lbm agents which nodes they're on also
  $ns_ at 0.0 "$g($i) set-node $node_($i)"
}

# the format now for the lar send is
#
# "$nodeId sendData <dest ID> <size> <method>"
#
# this will be used to test in a static configuration, and will
# change once the mobility portion is figured out.

#Schedule events

puts "Scheduling the send events"
for {set k $val(dataStart)} {$k < $val(dataStop)} {set k [expr $k + 1.0] } \
{

  $ns_ at $k "$g(1384) sendMulticastData 2118 1015 2168 1065 936 B"    ;#(x1,y1,x2,y2) 範囲(x2-x1)×(y2-y1)
}


# this is done to make the simulator continue running and "settle" things out
for {set i 0} {$i < $val(nn)} {incr i} {
  $ns_ at $val(signalStop) "$g($i) lbmDone"
}

$ns_ at $val(finish) "finish"
$ns_ at [expr $val(finish) + 0.1] "puts \"NS Exiting...\" ; $ns_ halt"


# Create some feedback for hov far we are into the simulation
for {set i 0} {$i < 100} {incr i} {
	$ns_ at [expr $i * $val(finish) / 100] "puts \" ... $i % into sim ....\""
}


#Run the simulation
puts ""
puts ""
puts "***********************************************"
puts "***********************************************"
puts "***********************************************"
puts ""
puts "Running the simulation"
puts ""
puts "***********************************************"
puts "***********************************************"
puts "***********************************************"
puts ""
puts ""
$ns_ run
