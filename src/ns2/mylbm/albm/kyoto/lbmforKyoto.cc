#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <ctype.h>
#include <cmath>
#include "lbm.h"

using namespace std;

int hdr_lbm::offset_;

// variables for statistics
long LBMsendpkts = 0; // the number of packets in the area, add one per hop
long long LBMtotalTransmitPkts = 0; // the total number of packets transmitted in the sim area, add one at broadcast and rebroadcast
long long LBMtotalTransmitBytes = 0; // the total bytes transmitted in the sim area, add at broadcast and rebroadcast
long LBMoneSuccess = 0; // the number of one success packets
double LBMtotalEndToEndDelay = 0; // The end_to_end delay is the delay from the time when data packet sent to the time when the data packet reached the geocast region (the first node received)
vector<int> successID;	// the number of received nodes for each packet ID

// to calculate approx. number of hops from source to geocast region
int LBMtotalnumHops = 0;

// used for reporting status, higher numbers imply more verbose output
int lbmVerbosity = 2;

/*********************************/
int count = 0; //ForwardingTransmission
int S_count = 0; //成功回数
int TotalHop = 0; //MluticastRegion到達成功時のホップ数の合計
double AverageHop = 0.0;
double TotalDelay = 0.0; //MluticastRegion到達成功時の遅延時間の合計
double AverageDelay = 0.0;

int dist0_5 = 0;
int dist6_10 = 0;
int dist11_20 = 0;
int dist21_30 = 0;
int dist31_40 = 0;
int dist41_50 = 0;
int dist51_60 = 0;
int dist61_70 = 0;
int dist71_80 = 0;
int dist81_90 = 0;
int dist91_100 = 0;
/*********************************/

typedef struct {
	double IRx, IRy;
	//int z;
} Point;

Point aaa(double IRx, double IRy) {
	Point p;
	p.IRx = IRx;
	p.IRy = IRy;
	return p;
}

// The next two class declarations bind our C++ classes to the OTcl class.
static class LBMHeaderClass: public PacketHeaderClass
{
	public:
	LBMHeaderClass():  PacketHeaderClass("PacketHeader/LBM", sizeof(hdr_lbm))
	{
		bind_offset(&hdr_lbm::offset_);
	}

} class_lbmhdr;

static class LBMClass: public TclClass
{
	public:
	LBMClass() : TclClass("Agent/LBM") {}
	TclObject* create(int, const char*const*)
	{
		return (new LBMAgent());
	}

} class_lbm;

// if true, this will add jitter to multicast packets being sent
bool LBMuseJitteronBroadcast = true;

// ***********************************************************************
//                            Creator
// ***********************************************************************
LBMAgent::LBMAgent() : Agent(PT_LBM)
{

	ll = NULL;
	node = NULL;

	srand(clock());

	return;
}

// ***********************************************************************
//                            Destructor
// ***********************************************************************
LBMAgent::~LBMAgent()
{
	rcvdPackets.erase(rcvdPackets.begin(), rcvdPackets.end());
	sentPackets.erase(sentPackets.begin(), sentPackets.end());
	return;
}

// ***********************************************************************
//                            command
// ***********************************************************************
int LBMAgent::command(int argc, const char*const* argv)
{
	TclObject *obj;  

	if (argc == 2)
	{
		if (strcmp(argv[1], "lbmDone") == 0)
		{
			if ( this->addr() == 0 )
			{
				cout << "\t LBMsendpkts = " << LBMsendpkts << endl;
				cout << "\t LBMoneSuccess = " << LBMoneSuccess << endl;
				cout << "\t LBMtotalTransmitPkts = " << LBMtotalTransmitPkts << endl;
				cout << "\t LBMtotalTransmitPkts/LBMoneSuccess = " << 
				LBMtotalTransmitPkts/LBMoneSuccess << endl;
				cout << "\t LBMtotalTransmitBytes/LBMoneSuccess = " <<
				LBMtotalTransmitBytes/LBMoneSuccess << endl;
				cout << "\t LBMtotalEndToEndDelay/LBMoneSuccess = " <<
				LBMtotalEndToEndDelay/LBMoneSuccess << endl;
				// to calculate approx. number of hops 
				// from source to geocast region
				cout << "\t LBMtotalnumHops = " << LBMtotalnumHops << endl;
				cout << "\t LBMtotalnumHops/LBMoneSuccess = " << ((float)LBMtotalnumHops)/LBMoneSuccess << endl;
				/*********************************/
				cout << "---------" << endl;
				cout << "" << (float)LBMoneSuccess/LBMsendpkts << endl;
				cout << "" << TotalHop/(double)S_count << endl;
				cout << "" << TotalDelay/(double)S_count << endl;
				cout << "" << count << endl;
				cout << "\t TotalSuccessCount = " << S_count << endl;
				cout << "\t TotalSuccessHop = " << TotalHop << endl;
				cout << "\t TotalSuccessDelay = " << TotalDelay << endl;
				cout << "---------" << endl;
				cout << "\t count(0~5) = " << dist0_5 << endl;
				cout << "\t count(5~10) = " << dist6_10 << endl;
				cout << "\t count(10~20) = " << dist11_20 << endl;
				cout << "\t count(20~30) = " << dist21_30 << endl;
				cout << "\t count(30~40) = " << dist31_40 << endl;
				cout << "\t count(40~50) = " << dist41_50 << endl;
				cout << "\t count(50~60) = " << dist51_60 << endl;
				cout << "\t count(60~70) = " << dist61_70 << endl;
				cout << "\t count(70~80) = " << dist71_80 << endl;
				cout << "\t count(80~90) = " << dist81_90 << endl;
				cout << "\t count(90~100) = " << dist91_100 << endl;
				/*********************************/
				cout << "---------" << endl;
			}
			return (TCL_OK);
		}
	}
	else if (argc == 3) 
	{
		if (strcmp(argv[1], "set-ll") == 0) 
		{
			if( (obj = TclObject::lookup(argv[2])) == 0)
			{
				fprintf(stderr, "LBMAgent(set-ll): %s lookup of %s failed\n", argv[1], argv[2]);
				return (TCL_ERROR);
			}
			ll = (NsObject*) obj;
			return (TCL_OK);
		} 
		else if (strcmp(argv[1], "set-node") == 0) 
		{
			if( (obj = TclObject::lookup(argv[2])) == 0)
			{
				fprintf(stderr, "LBMAgent(set-node): %s lookup of %s failed\n", argv[1], argv[2]);
				return (TCL_ERROR);
			}
			node = dynamic_cast< MobileNode * >(obj);
			if (node)       // dynamic cast was successful and didn't return NULL
			{
				return (TCL_OK);
			} 
			else 
			{
				fprintf(stderr, "Unable to dynamically cast %s to a MobileNode\n", argv[2]);
				return (TCL_ERROR);
			}
		}
	}

	// usage: "LBMAgent sendMulticastData llx lly urx ury size method"
	else if (argc == 8)
	{
		if (strcmp(argv[1], "sendMulticastData") == 0)
		{
			if ((ll == NULL) || (node == NULL))
			{
				fprintf(stderr, "Link layer and node must be set on a lbm agent\n");
				fprintf(stderr, "before anything can be sent.\n");
				return (TCL_ERROR);
			}
			node->update_position();

			// Create a new packet
			Packet* dataPkt = allocpkt();
			// Access the LAR header for the new packet:
			struct hdr_lbm* lbmhdr = hdr_lbm::access(dataPkt);
			// struct hdr_cmn* hdrcmn = hdr_cmn::access(dataPkt);
			//struct hdr_cmn* hdrcmn = hdr_cmn::access(dataPkt);	

			lbmhdr->lbmCode_ = 'D';
			lbmhdr->forwardCode_ = toupper(argv[7][0]);
			// cout << "forwardCode = " << lbmhdr->forwardCode() << endl;
			lbmhdr->sourceX_ = node->X();
			lbmhdr->sourceY_ = node->Y();
			lbmhdr->lastHopX_ = node->X();
			lbmhdr->lastHopY_ = node->Y();
			lbmhdr->lowerLeftX_ = atof(argv[2]);
			lbmhdr->lowerLeftY_ = atof(argv[3]);
			lbmhdr->upperRightX_ = atof(argv[4]);
			lbmhdr->upperRightY_ = atof(argv[5]);
			lbmhdr->dataLength_ = atoi(argv[6]);

			// to calculate approx. number of hops from source to geocast region
			lbmhdr->numHops = 1;

			// Store the current time in the 'send_time' field
			lbmhdr->sendTime_ = Scheduler::instance().clock();

			broadcastPacket(dataPkt);

			// ############# statistic ##############	  
			// add 1 in the LBMsendpkts by sending one data packet
			LBMsendpkts++;
			// add an entry for the vectors
			successID.push_back(0);

			// for statistics convenience, if the node which broadcasts 
			// the multicast packet is in the multicast region, 
			// then we think it receives its owm broadcasted packet, 
			// add 1 on corresponding deliveredPktID
			if ( this->inMulticastRegion(lbmhdr->lowerLeftX(), lbmhdr->lowerLeftY(), lbmhdr->upperRightX(), lbmhdr->upperRightY()))
			{
				successID[LBMsendpkts-1]++;
				if (successID[LBMsendpkts-1] == 1)
				{
					LBMoneSuccess++;
					LBMtotalEndToEndDelay += Scheduler::instance().clock()-lbmhdr->sendTime(); 
				}
			}

			// return TCL_OK, so the calling function knows that the
			// command has been processed
			return (TCL_OK);
		}
	}
	// If the command hasn't been processed by GeocastAgent()::command,
	// call the command() function for the base class
	return (Agent::command(argc, argv));
}


// ***********************************************************************
//                            recv
// ***********************************************************************
void LBMAgent::recv(Packet* pkt, Handler*)
{
	// Access the common header for the received packet:
	// new way to do this (2.1b7 and later)
	struct hdr_lbm* lbmhdr = hdr_lbm::access(pkt);
	struct hdr_cmn* hdrcmn = hdr_cmn::access(pkt);
	struct hdr_ip* hdrip = hdr_ip::access(pkt);

	node->update_position();


	// Is it a duplicate packet?
	if (this->dupRcvdPacket(hdrcmn->uid_) || this->dupSentPacket(hdrcmn->uid_))
	{
		// already seen it, or I sent it, just free it and drop it
		//if (lbmVerbosity == 2)
		if (lbmVerbosity > 2)
		{
			cout << "****************************************************" << endl;
			cout << "In LBM recv at node:  " << this->addr() << " at [" << node->X() << "," << node->Y() << "]" << endl;
			cout << "Time:  " << Scheduler::instance().clock() << endl;
			cout << "Packet ID:  " << hdrcmn->uid_ << "     Type:  " << lbmhdr->lbmCode() << endl;
			cout << "Duplicate packet seen from " << hdrcmn->prev_hop_ << ", freeing it." << endl;
			cout << "Returning from recv early" << endl;
			cout << "Time:  " << Scheduler::instance().clock() << endl;
			cout << "****************************************************" << endl << endl;
		}
		Packet::free(pkt);
		return;
	}
	else 
	{	
		// This is redundant for now because dupRcvdPacket records the packet
		// as recieved, but I don't like that side effect and may change it
		// later.
		recordRcvdPacket(hdrcmn->uid_);
	}

	if (lbmVerbosity > 0)
	{
		cout << "****************************************************" << endl;
		cout << "In LBM recv at node:  " << this->addr() << " at [" << node->X() << "," << node->Y() << "]" << endl;
		cout << "Time:  " << Scheduler::instance().clock() << endl;
		//cout << "hdrcmn->size() = " << hdrcmn->size() << endl;
		//cout << "lbmhdr->size() = " << lbmhdr->size() << endl;
		cout << endl;
	}

	// What kind of packet is it?
	// ***********************************************************************
	//                            Data Packet
	// ***********************************************************************
	if (lbmhdr->lbmCode() == 'D')
	{

	// if((lbmhdr->lastHopX() == lbmhdr->sourceX()) && (lbmhdr->lastHopY() == lbmhdr->sourceY()))
	// {
	// 	forwarding_count = 0;
	// }

		if (lbmVerbosity > 0)
		{
			//cout << "LBM data packet seen at node" << this->addr() << endl;
			cout << "From:       " << hdrip->src_.addr_ << " at [" << lbmhdr->sourceX() << "," << lbmhdr->sourceY() << "]" << endl;
			cout << "Last hop:   " << hdrcmn->prev_hop_ << endl;
			cout << "Packet ID:  " << hdrcmn->uid_ << endl;
		}

		if (this->inMulticastRegion(lbmhdr->lowerLeftX(), lbmhdr->lowerLeftY(),
		lbmhdr->upperRightX(), lbmhdr->upperRightY()))
		{
			/*************************************************/
			double dist_MR;
			double MR_x = node->X() - lbmhdr->lastHopX();	// x distance
			double MR_y = node->Y() - lbmhdr->lastHopY();   // y distance
			
			dist_MR = sqrt(MR_x * MR_x + MR_y * MR_y);
			/*************************************************/

			if((1 < dist_MR) && (dist_MR <= 100))
			{
				/*************************************************/
				S_count++;
				TotalHop += lbmhdr->numHops;
				TotalDelay += Scheduler::instance().clock()-lbmhdr->sendTime(); 
				/*************************************************/

				// print some nice informational message
				if (lbmVerbosity > 0)
				{
					cout << "Successfully!!!" << endl;
					cout << "LastHopPoint: " << lbmhdr->lastHopX() << " , " << lbmhdr->lastHopY() << endl;
					cout << "distance: " << dist_MR << endl;
					cout << "hop count:            " << lbmhdr->numHops << endl;
					cout << "Send scheduled at:    " << lbmhdr->sendTime() << endl;
					cout << "Received at:          " << Scheduler::instance().clock() << endl;
					cout << "Total Delay:          " << Scheduler::instance().clock()-lbmhdr->sendTime() << endl;
					cout << "Rebroadcast the packet" << endl;
					cout << "****************************************************" << endl << endl;
				}

				// if the data packet is first arrival in the geocast region,
				// count it to the received data packets, LBMoneSuccess++
				// if no, other node has already count it
				// I receive the packet successfully, add 1 on corresponding deliveredPktID
				successID[hdrcmn->uid_]++;

				if (successID[hdrcmn->uid_] == 1)
				{
					LBMoneSuccess++;
					LBMtotalEndToEndDelay += Scheduler::instance().clock()-lbmhdr->sendTime(); 

					// to calculate approx. number of hops from source to geocast region
					LBMtotalnumHops += lbmhdr->numHops;

					// cout << "Packet ID:  " << hdrcmn->uid_  << endl;
					// cout << "Delay for this packet :" <<
					// Scheduler::instance().clock()-lbmhdr->sendTime() << endl;
					// cout << "TotalDelay : " << LBMtotalEndToEndDelay << endl;
				}

				// I am in multicast region, I must rebroadcast the packet.
				this->rebroadcastPacket(pkt);

				// Discard the packet
				//  Packet::free(pkt);

				return;
			}
		}
		else if (this->inForwardingRegion(lbmhdr->forwardCode_, lbmhdr->lastHopX(), lbmhdr->lastHopY(), 
		lbmhdr->lowerLeftX(),lbmhdr->lowerLeftY(), lbmhdr->upperRightX(), lbmhdr->upperRightY(), 
		lbmhdr->sourceX(), lbmhdr->sourceY()))
		{
			/*************************************************/
			double dist_FR;
			double FR_x = node->X() - lbmhdr->lastHopX();	// distance of x
			double FR_y = node->Y() - lbmhdr->lastHopY();   // distance of y

			dist_FR = sqrt(FR_x * FR_x + FR_y * FR_y);

			if((1 < dist_FR) && (dist_FR <= 5))
			{
				dist0_5++;
			}
			else if((5 < dist_FR) && (dist_FR <= 10))
			{
				dist6_10++;
			}
			else if((10 < dist_FR) && (dist_FR <= 20))
			{
				dist11_20++;
			}
			else if((20 < dist_FR) && (dist_FR <= 30))
			{
				dist21_30++;
			}
			else if((30 < dist_FR) && (dist_FR <= 40))
			{
				dist31_40++;
			}
			else if((40 < dist_FR) && (dist_FR <= 50))
			{
				dist41_50++;
			}
			else if((50 < dist_FR) && (dist_FR <= 60))
			{
				dist51_60++;
			}
			else if((60 < dist_FR) && (dist_FR <= 70))
			{
				dist61_70++;
			}
			else if((70 < dist_FR) && (dist_FR <= 80))
			{
				dist71_80++;
			}
			else if((80 < dist_FR) && (dist_FR <= 90))
			{
				dist81_90++;
			}
			else if((90 < dist_FR) && (dist_FR <= 100))
			{
				dist91_100++;
			}
			/*************************************************/

			if((1 < dist_FR) && (dist_FR <= 100))
			{
				count++;
				if (lbmVerbosity > 0)
				{
					cout << "LastHopPoint: " << lbmhdr->lastHopX() << " , " << lbmhdr->lastHopY() << endl;
					cout << "distance: " << dist_FR << endl;
					//cout << "I am in the forwarding region" << endl;
					cout << "Success!! at [" << node->X() << "," << node->Y() << "]" << endl;
					//cout << "Rebroadcast the packet" << endl;
					cout << "hop count = " << lbmhdr->numHops << endl;//
					cout << "****************************************************" << endl << endl;
				}

				// I am not in multicast region, but I am in forwarding region. 
				// I must rebroadcast the packet.
				this->rebroadcastPacket(pkt);

				// Discard the packet
				// Packet::free(pkt);

				return;
			}
		}
		else
		{
			// I am not in multicast region and forwarding region.
			// I just discard the packet.
			Packet::free(pkt);

			if (lbmVerbosity > 0)
			{
				//cout << "I am not in multicast region and forwarding region" << endl;
				cout << "Failure!!" << endl;
				//cout << "Discard the packet" << endl;
				cout << "****************************************************" << endl << endl;
			}

			return;
		}
	}
	else if (lbmhdr->lbmCode() == 'U') 
	{
		// there is an error if this happens, but I'm not sure how to
		// respond, so it is commented out for now
		cerr << "The lbm code is unset, so I'm freeing the packet." << endl;
		Packet::free(pkt);
		return;
	}

	if (lbmVerbosity > 0)
	{
		cout << "Returning from recv at the bottom" << endl;
		cout << "Time:  " << Scheduler::instance().clock() << endl;
		cout << "****************************************************" << endl << endl;
	}

	return;
}

// ***********************************************************************
//                            dupRcvdPacket
// ***********************************************************************
bool LBMAgent::dupRcvdPacket(int id)
{
	// I may need to limit the size of this in the future.
	if (rcvdPackets.find(id) != rcvdPackets.end())
	{
		return true;
	} else {
		rcvdPackets.insert(id);
		return false;
	}
}

// ***********************************************************************
//                            dupSentPacket
// ***********************************************************************
bool LBMAgent::dupSentPacket(int id)
{
	// I may need to limit the size of this in the future.
	if (sentPackets.find(id) != sentPackets.end())
	{
		return true;
	} 
	else 
	{
		sentPackets.insert(id);
		return false;
	}
}

// ***********************************************************************
//                            inMulticastRegion
// ***********************************************************************
bool LBMAgent::inMulticastRegion(double llx, double lly, double urx, double ury)
{
	node->update_position();

	if ((node->X() >= llx) && (node->X() <= urx) && (node->Y() >= lly) && (node->Y() <= ury))
	{
		if ( lbmVerbosity > 0 )
			cout << "LBM agent on node " << this->addr() << " is in multicast region" << endl;
			return true;
	} 
	else 
	{
		return false;
	}
}

// ***********************************************************************
//                            inForwardingRegion
// ***********************************************************************
bool LBMAgent::inForwardingRegion(char fwdCode, double srcX, double srcY,
double llx, double lly, double urx, double ury,
double sourceX, double sourceY)
{
	node->update_position();

	if (fwdCode == 'B')
	{	
		double fwdllx, fwdlly, fwdurx, fwdury;

		fwdllx = lbm_min(srcX, llx);
		fwdlly = lbm_min(srcY, lly);
		fwdurx = lbm_max(srcX, urx);
		fwdury = lbm_max(srcY, ury);

		double roadMargin = 5.0; 

		Point p[4];

		p[0] = aaa(1878,1037);
		p[1] = aaa(2005,1038);
		p[2] = aaa(2141,1037);
		p[3] = aaa(2140,1185);


			if((fwdllx <= node->X()) && (node->X() <= p[2].IRx) && ((sourceY - roadMargin) <= node->Y()) && (node->Y() <= (p[1].IRy + roadMargin)))
			{
				return true;
			}
			else if(((p[2].IRx - roadMargin) <= node->X()) && (node->X() <= (p[2].IRx + roadMargin)) && ((p[2].IRy - roadMargin) <= node->Y()) && (fwdlly <= node->Y()) && (node->Y() <= ury))
			{
				return true;
			}
			else
			{
				return false;
			}

		
	}

	if (fwdCode == 'S')
	{  
		double centerX = ( llx + urx )/2;
		double centerY = ( lly + ury )/2;

		double lastHopDist = DIST(srcX,srcY,centerX,centerY);
		double myDist = DIST(node->X(),node->Y(),centerX,centerY);

		if ( lbmVerbosity > 0 )
		{
			cout << "Forwarding method is step." << endl;
			cout << "lastHopDist = " << lastHopDist << endl;
			cout << "myDist = " << myDist << endl;
			cout << "LBMstepDelta = " << LBMstepDelta << endl;
		}

		return (myDist <= (lastHopDist + LBMstepDelta));
	}

	if (fwdCode == 'F')
	{
		if ( lbmVerbosity > 0 )
		{
			cout << "Forwarding method is flood." << endl;
		}
		return true;
	}
	else
	{
		cerr << "inForwardingRegion and the forward code is not set!" << endl;
		return false;
	}
}

// ***********************************************************************
//                            broadcastPacket
// ***********************************************************************
void LBMAgent::broadcastPacket(Packet *p)
{
	// Access the common header for the new packet:
	// new way to do this (2.1b7 and later)
	struct hdr_cmn* cmnhdr = hdr_cmn::access(p);
	struct hdr_ip*  iphdr  = hdr_ip::access(p);
	struct hdr_lbm* lbmhdr = hdr_lbm::access(p);

	// set all the necessary things for the common header
	cmnhdr->next_hop_ = IP_BROADCAST;  // broadcast
	cmnhdr->prev_hop_ = this->addr();
	//cmnhdr->next_hop_ = 1;
	//cmnhdr->iface() = iface_literal::ANY_IFACE;    // any interface
	cmnhdr->direction() = hdr_cmn::DOWN;    // hopefully send it out
	//cmnhdr->direction() = hdr_cmn::UP;

	// now the ip header stuff
	iphdr->saddr() = this->addr();
	iphdr->sport() = 220;
	iphdr->daddr() = IP_BROADCAST;
	//iphdr->daddr() = 1;
	iphdr->dport() = 220;
	//iphdr->dport() = 255;         // router port
	iphdr->ttl() = 32;

	// cause the end-to-end delay and the success ratios 
	// to take into account the LBM part of the header
	cmnhdr->size() = lbmhdr->size();

	// cout << endl << "After Modification" << endl;
	// LBMprintHeaders(p);

	// Send the packet
	recordSentPacket(cmnhdr->uid_);
	// cout << "LBM:  Sending a packet..." << endl;

	double jitter = 0.0;
	if (LBMuseJitteronBroadcast)
	{
		// this one is different and was taken from dsragent::sendOutBCastPkt
		// I had to add a little jitter because it turned out that neighboring nodes
		// where re-broadcasting in the simulator at "exactly" the same time and
		// killing each other's transmissions.
		jitter = ((double)rand()/(double)RAND_MAX) / lbmJitter;
	} else {
		jitter = 0.0;
	}

	if (lbmVerbosity > 0)
	{
		cout << "Broadcast---------------" << endl;
		cout << "Scheduling the packet for delivery:  " << cmnhdr->uid_ << " with jitter=" << jitter << endl;
		cout << "Really sending at:  " << Scheduler::instance().clock() + jitter << endl;
	}

	Scheduler::instance().schedule(ll, p, jitter);
	// cout << "----------after broadcast" << endl;

	// ############# statistic ##############
	LBMtotalTransmitPkts++;
	LBMtotalTransmitBytes += lbmhdr->size();

	if (!ll)
	{
		cerr << "Crap, the link layer is NULL!!!!!" << endl;
	}

	return;
}


// ***********************************************************************
//                            rebroadcastPacket
// ***********************************************************************
// same as broadcastPacket, but 
//      - we don't change the ip header source address,
//      - we have to set previous hop,
//      - we need to decrement the ttl
//      - and we have to set the direction to down

void LBMAgent::rebroadcastPacket(Packet *p)
{
	// Access the common header for the new packet:
	// new way to do this (2.1b7 and later)
	struct hdr_cmn* cmnhdr = hdr_cmn::access(p);
	struct hdr_ip*  iphdr  = hdr_ip::access(p);
	struct hdr_lbm* lbmhdr = hdr_lbm::access(p);

	// LBMprintHeaders(p);

	lbmhdr->lastHopX_ = node->X();
	lbmhdr->lastHopY_ = node->Y();

	// to calculate approx. number of hops from source to geocast region
	lbmhdr->numHops++;

	// set all the necessary things for the common header
	cmnhdr->next_hop_ = IP_BROADCAST;  // broadcast
	cmnhdr->prev_hop_ = this->addr();
	//cmnhdr->next_hop_ = 1;
	//cmnhdr->iface() = iface_literal::ANY_IFACE;    // any interface
	cmnhdr->direction() = hdr_cmn::DOWN;    // hopefully send it out
	//cmnhdr->direction() = hdr_cmn::UP;

	// now the ip header stuff
	// don't reset this on a rebroadcast  
	//iphdr->saddr() = this->addr();

	// this doesn't have to be done, (it should be ok from the original)
	// but just in case...
	iphdr->sport() = 220;
	iphdr->daddr() = IP_BROADCAST;
	//iphdr->daddr() = 1;
	iphdr->dport() = 220;
	//iphdr->dport() = 255;         // router port

	// this is a difference from the regular broadcast
	// it appears that this needs to be done
	// since this packet doesn't go through the routing agent, this is never
	// done anywhere but here
	//cout << "ttl before decrement:  " << iphdr->ttl_ << endl;
	iphdr->ttl()--;

	// cause the end-to-end delay and the success ratios 
	// to take into account the LBM part of the header
	cmnhdr->size() = lbmhdr->size();

	// cout << endl << "After Modification" << endl;
	// LBMprintHeaders(p);

	// Send the packet
	recordSentPacket(cmnhdr->uid_);
	//cout << "LBM:  Sending a packet..." << endl;

	double jitter = 0.0;
	if (LBMuseJitteronBroadcast)
	{
		// this one is different and was taken from dsragent::sendOutBCastPkt
		// I had to add a little jitter because it turned out that neighboring nodes
		// where re-broadcasting in the simulator at "exactly" the same time and
		// killing each other's transmissions.
		jitter = ((double)rand()/(double)RAND_MAX) / lbmJitter;
	} else {
		jitter = 0.0;
	}

	if (lbmVerbosity > 0)
	{
		cout << "Rebroadcast---------------" << endl;
		cout << "Scheduling the packet for delivery:  " << cmnhdr->uid_ << " with jitter=" << jitter << endl;
		cout << "Really sending at:  " << Scheduler::instance().clock() + jitter << endl;
	}

	Scheduler::instance().schedule(ll, p, jitter);
	// cout << "----------after rebroadcast" << endl;

	// ############# statistic ##############
	LBMtotalTransmitPkts++;
	LBMtotalTransmitBytes += lbmhdr->size();

	if (!ll)
	{
		cerr << "Crap, the link layer is NULL!!!!!" << endl;
	}

// cout << "LBM:  Done sending." << endl;

	return;
}

// ***********************************************************************
//                            recordSentPacket
// ***********************************************************************
void LBMAgent::recordSentPacket(int id)
{
	sentPackets.insert(id);
	return;
}

// ***********************************************************************
//                            recordRcvdPacket
// ***********************************************************************
void LBMAgent::recordRcvdPacket(int id)
{
	rcvdPackets.insert(id);
	return;	
}

// ***********************************************************************
//                            LBMprintHeaders
// ***********************************************************************
void LBMprintHeaders(Packet *p)
{
	struct hdr_mac* machdr = hdr_mac::access(p);
	struct hdr_cmn* cmnhdr = hdr_cmn::access(p);
	struct hdr_ip* iphdr = hdr_ip::access(p);


	// Access the LBM header for the new packet:
	//hdr_lbm* lbmhdr = (hdr_lbm*)p->access(off_lbm_);

	if (lbmVerbosity > 0)
	{
		cout << "IP Header Information:" << endl;
		cout << "\tSrc Address:   " << iphdr->saddr() << endl;
		cout << "\tSrc Port:      " << iphdr->sport() << endl;
		cout << "\tDest Address:  " << iphdr->daddr() << endl;
		cout << "\tDest Port:     " << iphdr->dport() << endl;
		cout << "\tTTL:           " << iphdr->ttl() << endl;

		cout << endl << "Common Header Information:" << endl;
		cout << "\tPacket Type:   " << p_info().name(cmnhdr->ptype()) << endl;
		//  cout << "\tPacket Type:   " << cmnhdr->ptype() << endl;
		cout << "\tSize:          " << cmnhdr->size() << endl;
		cout << "\tUID:           " << cmnhdr->uid() << endl;
		cout << "\terror:         " << cmnhdr->error() << endl;
		cout << "\ttimestamp:     " << cmnhdr->timestamp() << endl;
		cout << "\tinterface:     " << cmnhdr->iface() << endl;
		cout << "\tDirection:     " << cmnhdr->direction() << endl;
		//cout << "\tref count:     " << cmnhdr->ref_count() << endl;
		cout << "\tprev hop:      " << cmnhdr->prev_hop_ << endl;
		cout << "\tnext hop:      " << cmnhdr->next_hop() << endl;
		cout << "\taddress type:  " << cmnhdr->addr_type() << endl;
		cout << "\tlast hop:      " << cmnhdr->last_hop_ << endl;
		cout << "\tnum forwards:  " << cmnhdr->num_forwards() << endl;
		cout << "\topt forwards:  " << cmnhdr->opt_num_forwards() << endl;

		cout << endl << "MAC Header Information:" << endl;
		cout << "\tSrc Address:   " << machdr->macSA() << endl;
		cout << "\tDest Address:  " << machdr->macDA() << endl;
	}

	return;
}
